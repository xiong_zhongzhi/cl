package com.cl.awt.js.tree.quadtree;

import com.cl.util.AllPowerful;

public class GameObject{
    private int x;          // 游戏对象的 x 坐标
    private int y;          // 游戏对象的 y 坐标
    private float rotation; // 游戏对象的旋转角度
    private float scale;    // 游戏对象的缩放比例

    public GameObject(int x, int y) {
        this.x = x;
        this.y = y;
        this.rotation = 0.0f;
        this.scale = 1.0f;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    private AllPowerful<GameObject,Object> updateAllPowerful;

    private AllPowerful<GameObject,Object> renderAllPowerful;

    public AllPowerful<GameObject, Object> getUpdateAllPowerful() {
        return updateAllPowerful;
    }

    public void setUpdateAllPowerful(AllPowerful<GameObject, Object> updateAllPowerful) {
        this.updateAllPowerful = updateAllPowerful;
    }

    public AllPowerful<GameObject, Object> getRenderAllPowerful() {
        return renderAllPowerful;
    }

    public void setRenderAllPowerful(AllPowerful<GameObject, Object> renderAllPowerful) {
        this.renderAllPowerful = renderAllPowerful;
    }

    // 游戏逻辑更新方法，用于处理游戏对象的逻辑
    public void update() {
        // 在这里实现游戏对象的逻辑
        updateAllPowerful.hook(this);
    }

    // 渲染方法，用于将游戏对象渲染到屏幕上
    public void render() {
        // 在这里实现游戏对象的渲染
        renderAllPowerful.hook(this);
    }
}
