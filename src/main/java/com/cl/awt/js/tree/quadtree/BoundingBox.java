package com.cl.awt.js.tree.quadtree;

import com.cl.util.AllPowerful;

public class BoundingBox {
    private int x;      // 矩形左上角的 x 坐标
    private int y;      // 矩形左上角的 y 坐标
    private int width;  // 矩形的宽度
    private int height; // 矩形的高度

    public BoundingBox(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    // 判断矩形是否包含某个点
    public boolean contains(int pointX, int pointY) {
        return pointX >= x && pointX <= x + width && pointY >= y && pointY <= y + height;
    }

    // 判断矩形是否与另一个矩形相交
    public boolean intersects(BoundingBox other) {
        return x < other.x + other.width && x + width > other.x && y < other.y + other.height && y + height > other.y;
    }

    private AllPowerful<BoundingBox,Object> updateAllPowerful;

    private AllPowerful<BoundingBox,Object> renderAllPowerful;

    public AllPowerful<BoundingBox, Object> getUpdateAllPowerful() {
        return updateAllPowerful;
    }

    public void setUpdateAllPowerful(AllPowerful<BoundingBox, Object> updateAllPowerful) {
        this.updateAllPowerful = updateAllPowerful;
    }

    public AllPowerful<BoundingBox, Object> getRenderAllPowerful() {
        return renderAllPowerful;
    }

    public void setRenderAllPowerful(AllPowerful<BoundingBox, Object> renderAllPowerful) {
        this.renderAllPowerful = renderAllPowerful;
    }

    // 游戏逻辑更新方法，用于处理游戏对象的逻辑
    public void update() {
        // 在这里实现游戏对象的逻辑
        updateAllPowerful.hook(this);
    }

    // 渲染方法，用于将游戏对象渲染到屏幕上
    public void render() {
        // 在这里实现游戏对象的渲染
        renderAllPowerful.hook(this);
    }

}
