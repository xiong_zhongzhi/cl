package com.cl.awt.js.tree.quadtree;

import java.util.ArrayList;
import java.util.List;

public class Quadtree {
    private final int MAX_OBJECTS = 10; // 每个节点最多存储的对象数量
    private final int MAX_LEVELS = 5; // 四叉树的最大层数

    private int level;
    private List<BoundingBox> blocks;
    private BoundingBox bounds;
    private Quadtree[] children;

    public Quadtree(int level, BoundingBox bounds) {
        this.level = level;
        this.blocks = new ArrayList<>();
        this.bounds = bounds;
        this.children = new Quadtree[4];
    }

    public void clear() {
        blocks.clear();

        for (int i = 0; i < children.length; i++) {
            if (children[i] != null) {
                children[i].clear();
                children[i] = null;
            }
        }
    }

    private void split() {
        int subWidth = bounds.getWidth() / 2;
        int subHeight = bounds.getHeight() / 2;
        int x = bounds.getX();
        int y = bounds.getY();

        children[0] = new Quadtree(level + 1, new BoundingBox(x + subWidth, y, subWidth, subHeight));
        children[1] = new Quadtree(level + 1, new BoundingBox(x, y, subWidth, subHeight));
        children[2] = new Quadtree(level + 1, new BoundingBox(x, y + subHeight, subWidth, subHeight));
        children[3] = new Quadtree(level + 1, new BoundingBox(x + subWidth, y + subHeight, subWidth, subHeight));
    }

    private int getIndex(BoundingBox blockBounds) {
        int index = -1;
        double verticalMidpoint = bounds.getX() + (bounds.getWidth() / 2);
        double horizontalMidpoint = bounds.getY() + (bounds.getHeight() / 2);

        boolean topQuadrant = blockBounds.getY() < horizontalMidpoint && blockBounds.getY() + blockBounds.getHeight() < horizontalMidpoint;
        boolean bottomQuadrant = blockBounds.getY() > horizontalMidpoint;

        if (blockBounds.getX() < verticalMidpoint && blockBounds.getX() + blockBounds.getWidth() < verticalMidpoint) {
            if (topQuadrant) {
                index = 1;
            } else if (bottomQuadrant) {
                index = 2;
            }
        } else if (blockBounds.getX() > verticalMidpoint) {
            if (topQuadrant) {
                index = 0;
            } else if (bottomQuadrant) {
                index = 3;
            }
        }

        return index;
    }

    public void insert(BoundingBox block) {
        if (children[0] != null) {
            int index = getIndex(block);

            if (index != -1) {
                children[index].insert(block);
                return;
            }
        }

        blocks.add(block);

        if (blocks.size() > MAX_OBJECTS && level < MAX_LEVELS) {
            if (children[0] == null) {
                split();
            }

            int i = 0;
            while (i < blocks.size()) {
                int index = getIndex(blocks.get(i));
                if (index != -1) {
                    children[index].insert(blocks.remove(i));
                } else {
                    i++;
                }
            }
        }
    }

    public List<BoundingBox> retrieve(List<BoundingBox> result, BoundingBox blockBounds) {
        if (!bounds.intersects(blockBounds)) {
            return result;
        }

        for (BoundingBox block : blocks) {
            if (block.intersects(blockBounds)) {
                result.add(block);
            }
        }

        if (children[0] != null) {
            int index = getIndex(blockBounds);
            if (index != -1) {
                children[index].retrieve(result, blockBounds);
            } else {
                for (Quadtree child : children) {
                    child.retrieve(result, blockBounds);
                }
            }
        }
        return result;
    }

    public void remove(BoundingBox block) {
        if (children[0] != null) {
            int index = getIndex(block);
            if (index != -1) {
                children[index].remove(block);
            }
        }

        blocks.remove(block);

        // 合并子节点
        if (isMergeable()) {
            merge();
        }
    }

    private boolean isMergeable() {
        return children[0] != null && blocks.size() <= MAX_OBJECTS;
    }

    private void merge() {
        for (Quadtree child : children) {
            if (child != null) {
                blocks.addAll(child.blocks);
                child.clear();
            }
        }
        children = new Quadtree[4];
    }
}

