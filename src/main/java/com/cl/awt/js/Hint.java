package com.cl.awt.js;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;

/**
 * 消息弹出工具
 */
public class Hint extends Js{

	//步长
	private Integer step = 1;

	//播放间隔毫秒
	private Integer interval = 50;

	//启动延时毫秒
	private Integer Delay = 0;

	public void run(Integer state, String msg) {
		new Thread(() -> keepalive(state, msg,step,interval,Delay)).start();
	}

	public void run(Integer state, String msg,Integer step) {
		new Thread(() -> keepalive(state, msg,step,interval,Delay)).start();
	}

	public void run(Integer state, String msg,Integer step,Integer interval) {
		new Thread(() -> keepalive(state, msg,step,interval,Delay)).start();
	}

	//延时
	public void runDelay(Integer state, String msg,Integer Delay) {
		new Thread(() -> keepalive(state, msg,step,interval,Delay)).start();
	}

	// 提示
	public void keepalive(Integer state, String msg,Integer step,Integer interval,Integer Delay) {
		if (Delay>0){
			try {
				Thread.sleep(Delay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		JLabel jlabel = new JLabel(msg);
		jlabel.setVerticalAlignment(JLabel.CENTER);
		jlabel.setHorizontalAlignment(JLabel.CENTER);
		switch (state) {
		case 0:
			jlabel.setBackground(Color.blue);
			break;
		case -1:
			jlabel.setBackground(Color.red);
			break;
		case -5:
			jlabel.setBackground(Color.yellow);
			break;
		}
		drawingBoard.add(jlabel, 0);
		// 获取当前宽高
		Dimension xy = client.drawingBoard.getSize();
		int x = xy.width / 2 - (msg.length() * 13 / 2);
		int y = xy.height / 2 - 15;

		while (y > 0) {
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			jlabel.setBounds(x, y, msg.length() * 13, 30);
			client.drawingBoard.repaint();
			y-=step;
		}
		client.drawingBoard.remove(jlabel);
	}

}
