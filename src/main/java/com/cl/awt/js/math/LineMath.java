package com.cl.awt.js.math;
import com.alibaba.fastjson.JSONObject;
import com.cl.awt.core.geometry.Point;
import org.apache.commons.math3.geometry.euclidean.twod.Line;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

public class LineMath {

        public static Point lineIntersect(Point p0, Point p1, Point p2, Point p3) {
            Line line1 = new Line(new Vector2D(p0.x, p0.y), new Vector2D(p1.x, p1.y));
            Line line2 = new Line(new Vector2D(p2.x, p2.y), new Vector2D(p3.x, p3.y));

            Vector2D intersection = line1.intersection(line2);

            if (intersection == null) {
                // 直线平行或重合，没有交点
                return null;
            }

            int intersectX = (int) intersection.getX();
            int intersectY = (int) intersection.getY();

            return new Point(intersectX, intersectY);
        }

        public static void main(String[] args) {
            Point point = lineIntersect(new Point(15, 2335), new Point(2415, 2335),
                    new Point(2335, 15), new Point(2335, 2415));
            System.out.println(JSONObject.toJSONString(point));
        }


    // 自己实现的直线取交点
    public static Point lineIntersectTest(Point p0, Point p1, Point p2, Point p3) {
        int x1 = p0.x, y1 = p0.y;
        int x2 = p1.x, y2 = p1.y;
        int x3 = p2.x, y3 = p2.y;
        int x4 = p3.x, y4 = p3.y;

        int A1 = y2 - y1;
        int B1 = x1 - x2;
        int C1 = A1 * x1 + B1 * y1;
        int A2 = y4 - y3;
        int B2 = x3 - x4;
        int C2 = A2 * x3 + B2 * y3;
        int denominator = A1 * B2 - A2 * B1;

        if (denominator == 0) {
            // 直线平行或重合，没有交点
            return null;
        }

        int intersectX, intersectY;

        if (B1 == 0 && A2 == 0) {
            intersectX = x1;
            intersectY = y4;
        }else if (B2 == 0 && A1 == 0) {
            intersectX = x3;
            intersectY = y2;
        }else  if (B1 == 0) {
            intersectX = x1;
            intersectY = (A2 * C1 - A1 * C2) / denominator;
        } else if (B2 == 0) {
            intersectX = x3;
            intersectY = (A1 * C2 - A2 * C1) / denominator;
        }else if (A1 == 0) {
            intersectX = (B2 * C1 - B1 * C2) / denominator;
            intersectY = y2;
        } else if (A2 == 0) {
            intersectX = (B2 * C1 - B1 * C2) / denominator;
            intersectY = y4;
        }else {
            intersectX = (B2 * C1 - B1 * C2) / denominator;
            intersectY = (A1 * C2 - A2 * C1) / denominator;
        }

        return new Point(intersectX, intersectY);
    }


}


