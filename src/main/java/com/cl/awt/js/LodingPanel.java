package com.cl.awt.js;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class LodingPanel extends JPanel {

    private static final long serialVersionUID = 1551571546L;

    private Timer timer;
    private Timer timer2;
    private int delay;
    private int startAngle;
    private int arcAngle = 0;
    private int orientation;
    private int flagSize = 0;

    private Color color;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    //百分比
    private int bfb = 0;

    public int getBfb() {
        return bfb;
    }

    public void setBfb(int bfb) {
        this.bfb = bfb;
    }

    private int endBfb = 0;

    public int getEndBfb() {
        return endBfb;
    }

    public void setEndBfb(int endBfb) {
        this.endBfb = endBfb;
    }

    public static final int CLOCKWISE = 0;
    public static final int ANTICLOCKWISE = 1;

    public LodingPanel(Color color) {
        this.color = color;
        this.delay = 10;
        this.orientation = CLOCKWISE;
        setBackground(Color.WHITE);
        init();
    }

    @Override
    public void show() {
        this.timer.start();
        this.timer2.start();
    }


    private void init() {
        this.timer = new Timer(delay, new ReboundListener());
        this.timer2 = new Timer(200, new ReboundListener2());
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawArc(g);
    }

    private void drawArc(Graphics g) {
        Graphics2D g2d = (Graphics2D) g.create();
        //抗锯齿
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //设置画笔颜色
        g2d.setColor(Color.WHITE);
        g2d.drawArc(0, 0, getWidth(), getHeight(), 0, 360);
        g2d.setColor(color);
        g2d.fillArc(0, 0, getWidth(), getHeight(), startAngle, arcAngle);
        g2d.setColor(Color.WHITE);
        g2d.fillArc(5, 5, getWidth() - 10, getHeight() - 10, 0, 360);
        g2d.setColor(Color.BLUE);

        if (endBfb == 0) {
            g2d.setFont(new Font("Monospaced", Font.BOLD, 22));
            switch (flagSize) {
                case 0:
                    g2d.drawString("正在随机匹配位面中", 130, 240);
                    break;
                case 1:
                    g2d.drawString("正在随机匹配位面中.", 140, 240);
                    break;
                case 2:
                    g2d.drawString("正在随机匹配位面中..", 140, 240);
                    break;
                case 3:
                    g2d.drawString("正在随机匹配位面中...", 140, 240);
                    break;
            }
        } else {
            g2d.setFont(new Font("Monospaced", Font.BOLD, 22));
            g2d.drawString("匹配成功!资源渲染中", 140, 200);
            g2d.setFont(new Font("Monospaced", Font.BOLD, 66));
            if (bfb <= 100) {
                g2d.drawString(bfb + "%", 205, 260);
            } else if (bfb <= 100) {
                g2d.drawString(bfb + "%", 190, 260);
            } else {
                g2d.drawString(bfb + "%", 175, 260);
            }
        }
        g2d.dispose();
    }

    private class ReboundListener2 implements ActionListener {

        private int o = 0;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (endBfb == 0) {
                if (flagSize < 3) {
                    flagSize++;
                } else {
                    flagSize = 0;
                }
            }
        }
    }

    private class ReboundListener implements ActionListener {

        private int o = 0;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (bfb < endBfb) {
                bfb++;
            }
            if (startAngle < 360) {
                //控制每个DELAY周期旋转的角度，+ 为逆时针  - 为顺时针
                switch (orientation) {
                    case CLOCKWISE:
                        startAngle = startAngle + 5;
                        break;
                    case ANTICLOCKWISE:
                        startAngle = startAngle - 5;
                        break;
                    default:
                        startAngle = startAngle + 5;
                        break;
                }
            } else {
                startAngle = 0;
            }
            if (o == 0) {
                if (arcAngle >= 355) {
                    o = 1;
                    orientation = ANTICLOCKWISE;
                } else {
                    if (orientation == CLOCKWISE) {
                        arcAngle += 5;
                    }
                }
            } else {
                if (arcAngle <= 5) {
                    o = 0;
                    orientation = CLOCKWISE;
                } else {
                    if (orientation == ANTICLOCKWISE) {
                        arcAngle -= 5;
                    }
                }
            }

            repaint();
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 1000);
        frame.setLocationRelativeTo(null);
        LodingPanel lodingPanel = new LodingPanel(Color.green);
        lodingPanel.setBackground(Color.WHITE);
        lodingPanel.show();
        frame.add(lodingPanel);
        frame.setVisible(true);
    }
}
