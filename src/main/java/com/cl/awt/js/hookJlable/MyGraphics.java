package com.cl.awt.js.hookJlable;

import java.awt.*;

public class MyGraphics {

    private Graphics graphics;

    public Graphics getGraphics() {
        return graphics;
    }

    public void setGraphics(Graphics graphics) {
        this.graphics = graphics;
    }
}
