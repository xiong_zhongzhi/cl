package com.cl.awt.js.hookJlable;

import com.cl.been.Container;
import com.cl.util.AllPowerful;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class RoomMapJlabel extends JLabel {

    public List<AllPowerful<MyGraphics, Container>> allPowerfulList= new ArrayList<AllPowerful<MyGraphics, Container>>();

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        hookRun(g);
    }

    //执行钩子
    public void hookRun(Graphics g) {
        MyGraphics myGraphics = new MyGraphics();
        myGraphics.setGraphics(g);
        allPowerfulList.forEach(allPowerful->{
            try {
                allPowerful.hook(myGraphics);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
