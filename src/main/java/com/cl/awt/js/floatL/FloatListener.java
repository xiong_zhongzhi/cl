package com.cl.awt.js.floatL;

import com.cl.util.AllPowerful;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class FloatListener<T> implements MouseListener, MouseMotionListener  {

    private T data;

    public FloatListener(){

    }

    public FloatListener(JLabel jLabel,T data){
        this.data = data;
        jLabel.addMouseMotionListener(this);
        jLabel.addMouseListener(this);
    }

    public FloatListener(JPanel jPanel,T data){
        this.data = data;
        jPanel.addMouseMotionListener(this);
        jPanel.addMouseListener(this);
    }

    public void setMouseMovedAllPowerful(AllPowerful<FloatListener, Object> mouseMovedAllPowerful) {
        this.mouseMovedAllPowerful = mouseMovedAllPowerful;
    }

    public void setMouseExitedAllPowerful(AllPowerful<FloatListener, Object> mouseExitedAllPowerful) {
        this.mouseExitedAllPowerful = mouseExitedAllPowerful;
    }

    public void setMouseClickedAllPowerful(AllPowerful<FloatListener, Object> mouseClickedAllPowerful) {
        this.mouseClickedAllPowerful = mouseClickedAllPowerful;
    }

    private AllPowerful<FloatListener,Object> mouseMovedAllPowerful;

    private AllPowerful<FloatListener,Object> mouseExitedAllPowerful;

    private AllPowerful<FloatListener,Object> mouseClickedAllPowerful;

    public MouseEvent mouseEvent;

    /**
     * 鼠标移入
     * @param e
     */
    @Override
    public void mouseMoved(MouseEvent e) {
        mouseEvent = e;
        if (mouseMovedAllPowerful != null){
            mouseMovedAllPowerful.hook(this);
        }
    }

    /**
     * 鼠标移出
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {
        mouseEvent = e;
        if (mouseExitedAllPowerful != null){
            mouseExitedAllPowerful.hook(this);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        mouseEvent = e;
        if (mouseClickedAllPowerful != null){
            mouseClickedAllPowerful.hook(this);
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

}
