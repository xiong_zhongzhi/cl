package com.cl.awt.js.levelJ;

import javax.swing.*;
import java.awt.*;

public class LiveJ extends JLayeredPane {

    public Component addAndLayer(Component comp,Integer Layer){
        add(comp);
        setLayer(comp,Layer);
        return comp;
    }

    public Component addAndLayer(Component comp){
        return addAndLayer(comp,0);
    }


}
