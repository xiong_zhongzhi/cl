package com.cl.awt.js;

import com.cl.util.EnumTime;
import com.cl.util.Newjs;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.time.LocalDateTime;

public class ImgPanel extends javax.swing.JPanel {

    private static final long serialVersionUID = 1L;
    private Image image;
    private int imgWidth;
    private int imgHeight;
    private int width;
    private int height;
    private String url;
    private Object key;

    private Boolean cdFlag = false;//cd

    public Boolean getCdFlag() {
        return cdFlag;
    }

    public void setCdFlag(Boolean cdFlag) {
        this.cdFlag = cdFlag;
    }

    public Object getKey() {
        return key;
    }

    public void setKey(Object key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public int getImgWidth() {
        return imgWidth;
    }

    public void setImgWidth(int imgWidth) {
        this.imgWidth = imgWidth;
    }

    public int getImgHeight() {
        return imgHeight;
    }

    public void setImgHeight(int imgHeight) {
        this.imgHeight = imgHeight;
    }

    public void setBounds(int x, int y, int width, int height) {
        this.width = width;
        this.height = height;
        super.setBounds(x, y, width, height);
    }

    public ImgPanel() {
    }

    //cd冷却显示线程
    private Thread thread;

    //当前技能总cd
    private volatile Long cdtime;

    //剩余cd
    private volatile Long surplusCd;

    private volatile LocalDateTime startLocalDateTime;//cd 开始时间

    private volatile LocalDateTime endLocalDateTime;//cd 到期时间

    //设置cd时间
    public void setCdTime(LocalDateTime startLocalDateTime, LocalDateTime endLocalDateTime) {

        this.startLocalDateTime = startLocalDateTime;

        this.endLocalDateTime = endLocalDateTime;

        cdtime = Long.parseLong(EnumTime.millisecond.dateGap(startLocalDateTime, endLocalDateTime));
        if (cdtime > 0) {
            CdRunnable cdRunnable = new CdRunnable();
            if (thread != null) {
                thread.stop();
            }
            thread = new Thread(cdRunnable);
            thread.start();
        }
    }

    public void setImageHttpURL(String url) {
        this.url = url;
        // 该方法不推荐使用，该方法是懒加载，图像并不加载到内存，当拿图像的宽和高时会返回-1；
        // image = Toolkit.getDefaultToolkit().getImage(imgPath);
        try {
            // 该方法会将图像加载到内存，从而拿到图像的详细信息。
            image = new ImageIcon(new URL(url)).getImage();
            setImgWidth(image.getWidth(this));
            setImgHeight(image.getHeight(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private volatile Integer angle;

    private class CdRunnable implements Runnable {
        @Override
        public void run() {
            while (true) {
                //计算剩余cd
                surplusCd = Long.parseLong(EnumTime.millisecond.dateGap(endLocalDateTime));
                if (surplusCd <= 0) {
                    thread = null;
                    repaint();
                    return;
                }
                Newjs newjs = new Newjs();
                angle = newjs.multiply(360, newjs.divide(surplusCd, cdtime ,6).doubleValue()).intValue();
                repaint();
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        if (null != image) {
            int x = 0;
            int y = 0;
            g.drawImage(image, x, y, this.width, this.height, this);
        }

        if (cdFlag && surplusCd != null && surplusCd > 0) {
            Graphics2D g2d = (Graphics2D) g.create();
            //设置颜色
            g2d.setColor(Color.black);

            // 设置透明度为50%
            AlphaComposite alpha = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f);
            g2d.setComposite(alpha);

            g2d.fillArc(-getWidth() / 2, -getHeight() / 2, getWidth() * 2, getHeight() * 2, 0, 360);

            // 设置透明度为50%
            alpha = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f);
            g2d.setComposite(alpha);

            g2d.fillArc(-getWidth() / 2, -getHeight() / 2, getWidth() * 2, getHeight() * 2, -270, angle);

            //cd数字显示
            g2d.setFont(new Font("Monospaced", Font.BOLD, 32));
            Newjs newjs = new Newjs();

            g2d.setColor(Color.white);
            Double time = newjs.divide(surplusCd,1000).doubleValue();
            if (time >10){
                g2d.drawString(String.valueOf(time.intValue()), 22, 50);
            }else if (time >1){
                g2d.drawString(String.valueOf(time.intValue()), 29, 50);
            }else {
                g2d.drawString(String.valueOf(time), 13, 50);
            }
        }

    }
}