package com.cl.awt.js.form;

import java.util.List;

/**
 * 列表弹框
 * 
 * @author Administrator
 *
 */
public class Call {

	public Call(String showName, String name, Integer width) {
		this.showName = showName;
		this.name = name;
		this.width = width;
	}

	//展示名称
	private String showName;

	//属性名称
	private String name;

	//表格类型
	private CallEnum callEnum = CallEnum.TXT;

	//列宽
	private Integer width;

	//操作按钮集合
	private List<Operation> operationList;

	public String getShowName() {
		return showName;
	}

	public Call setShowName(String showName) {
		this.showName = showName;
		return this;
	}

	public String getName() {
		return name;
	}

	public Call setName(String name) {
		this.name = name;
		return this;
	}

	public Integer getWidth() {
		return width;
	}

	public Call setWidth(Integer width) {
		this.width = width;
		return this;
	}

	public CallEnum getCallEnum() {
		return callEnum;
	}

	public Call setCallEnum(CallEnum callEnum) {
		this.callEnum = callEnum;
		return this;
	}

	public List<Operation> getOperationList() {
		return operationList;
	}

	public Call setOperationList(List<Operation> operationList) {
		this.operationList = operationList;
		return this;
	}
}
