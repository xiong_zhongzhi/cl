package com.cl.awt.js.form;

import com.cl.util.AllPowerful;

import javax.swing.*;
import java.util.HashMap;

/**
 *表单钩子
 */
public abstract class FormAllPowerful<C extends HashMap, C1 extends HashMap> extends AllPowerful<C, C1> {

    protected FormAllPowerful(){

    }

    public FormAllPowerful<C, C1> setFormAllPowerful(JTable table, Object value, boolean isSelected, int row, int column, PopUp popUp) {
        this.table = table;
        this.value = value;
        this.isSelected = isSelected;
        this.row = row;
        this.column = column;
        this.popUp = popUp;
        return this;
    }

    //弹框对象
    private PopUp popUp;

    //表格对象
    private JTable table;

    //按钮对象
    private Object value;

    //
    private boolean isSelected;

    //当前行
    private int row;

    //当期列
    private int column;

    public PopUp getPopUp() {
        return popUp;
    }

    public void setPopUp(PopUp popUp) {
        this.popUp = popUp;
    }

    public JTable getTable() {
        return table;
    }

    public void setTable(JTable table) {
        this.table = table;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }
}
