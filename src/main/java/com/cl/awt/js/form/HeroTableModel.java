package com.cl.awt.js.form;

import com.cl.handler.Nature;
import com.cl.log.OutPut;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.List;

public class HeroTableModel<T> extends AbstractTableModel {

    private Class t;

    public HeroTableModel(List<Call> callList, List<T> data) {
        this.callList = callList;
        this.data = data;
        if (data.size() > 0) {
            t = data.get(0).getClass();
        }
    }

    @Nature("表头")
    private List<Call> callList;

    @Nature("表单数据")
    private List<T> data;

    // 返回一共有多少行
    @Override
    public int getRowCount() {
        return data.size();
    }

    // 返回一共有多少列
    @Override
    public int getColumnCount() {
        return callList.size()-1;
    }

    // 获取每一列的名称
    @Override
    public String getColumnName(int columnIndex) {
        return callList.get(columnIndex).getShowName();
    }

    // 单元格是否可以修改
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    // 每一个单元格里的值
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (callList.get(columnIndex).getCallEnum()) {
            case BUTTON:
                JLabel jLabel = new JLabel();
                return jLabel;
            case TXT:
                String getName = "get" + captureName(callList.get(columnIndex).getName());
                String Value = "";
                try {
                    Object OBJ = data.get(rowIndex);
                    Object obj = this.t.getMethod(getName).invoke(OBJ);
                    if (obj == null ){
                        Value = "";
                    }else {
                        Value = obj.toString();
                    }

                } catch (Exception e) {
                   OutPut.out(callList.get(columnIndex).getName()+"此属性列表未找到对应方法或属性");
                }
                return Value;
        }
        return null;
    }

    public String captureName(String name) {
        char[] cs = name.toCharArray();
        cs[0] -= 32;
        return String.valueOf(cs);
    }

}