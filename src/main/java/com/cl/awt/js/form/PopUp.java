package com.cl.awt.js.form;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.cl.awt.core.MyActionListener;
import com.cl.awt.js.Js;
import com.cl.been.Container;
import com.cl.handler.Nature;

/**
 * 列表弹框
 *
 * @author Administrator
 */
public class PopUp<T> extends Js {

    public PopUp() {
    }

    @Nature("列表弹框集合")
    List<Call> popUpFormList = new ArrayList<>();

    @Nature("弹框内部")
    JLabel frameJlabel;

    @Nature("头部")
    JLabel hadeJlabel;

    @Nature("弹框本体")
    JLabel jlabel;

    @Nature("尾部")
    JLabel tailJlabel;

    @Nature("弹框名称")
    private String popUpNmae;

    @Nature("高")
    private Integer height;

    @Nature("宽")
    private Integer width;

    @Nature("x")
    private Integer x;

    @Nature("y")
    private Integer y;

    @Nature("显示")
    private Boolean show;

    @Nature("表头")
    private List<Call> callList;

    @Nature("表单数据")
    private List<T> data;

    @Nature("表单构建器")
    private HeroTableModel heroTableModel;

    @Nature("表单附加按钮")
    private List<JButton> jButtonList = new ArrayList<>();

    //初始化
    public PopUp(String name, Integer x, Integer y, Integer width, Integer height, Boolean show, List<Call> callList, List<T> data) {
        this.popUpNmae = name;
        this.height = height;
        this.width = width;
        this.x = x;
        this.y = y;
        this.show = show;
        this.callList = callList;
        this.data = data;
        addPopUp();
        frameJlabelJlabel();
        addHadeJlabel();
        addTailRemoveJlabel();
        //构建表单
        setColumnNames();
        client.drawingBoard.repaint();
    }

    //构建表单
    private void setColumnNames() {
        //处理数据
        heroTableModel = new HeroTableModel(callList, data);
        //创建滚动框
        JTable t = new JTable(heroTableModel);
        JScrollPane jScrollPane = new JScrollPane(t);
        jScrollPane.setBounds(0, 0, frameJlabel.getWidth(), frameJlabel.getHeight());
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        frameJlabel.add(jScrollPane, 0);
        t.setVisible(true);
        jScrollPane.setVisible(true);
        //按鈕处理
        for (int i = 0; i < callList.size(); i++) {
            Call call = callList.get(i);
            switch (call.getCallEnum()) {
                case BUTTON:
                    t.addColumn(new TableColumn(i, call.getWidth(), new ButtonRenderer(call.getOperationList()), new ButtonEditor(call.getOperationList(), this)));
                    break;
                case TXT:
                    t.getColumnModel().getColumn(i).setPreferredWidth(call.getWidth());
                    break;
            }
        }
    }

    //单元格装饰
    private class ButtonRenderer implements TableCellRenderer {
        private List<Operation> operationList;

        ButtonRenderer(List<Operation> operationList) {
            super();
            this.operationList = operationList;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JLabel jLabel = (JLabel) value;
            int size = 0;
            for (int i = 0; i < operationList.size(); i++) {
                size += 10;
                Operation operation = operationList.get(i);
                JButton button = new JButton();
                button.setBounds(size, 0, 100, 20);
                size += operation.getWidth();
                button.setText(operation.getPerationName());
                jLabel.add(button);
            }
            return jLabel;
        }
    }

    private class ButtonEditor extends DefaultCellEditor {

        private List<Operation> operationList;

        private PopUp popUp;

        public ButtonEditor(List<Operation> operationList, PopUp popUp) {
            super(new JTextField());
            this.setClickCountToStart(1);
            this.operationList = operationList;
            this.popUp = popUp;
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            JLabel jLabel = new JLabel();
            int size = 0;
            for (int i = 0; i < operationList.size(); i++) {
                size += 10;
                Operation operation = operationList.get(i);
                JButton button = new JButton();
                button.setBounds(size, 0, 100, 20);
                size += operation.getWidth();
                button.setText(operation.getPerationName());
                button.addActionListener(new MyActionListener() {
                    @Override
                    public void action(ActionEvent e) {
                        try {
                            operation.getAllPowerful().setFormAllPowerful(table, value, isSelected, row, column, popUp)
                                    .hook(new Container("ActionEvent", e));
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }
                });
                jLabel.add(button);
            }
            return jLabel;
        }
    }

    //添加框体
    private void addPopUp() {
        jlabel = new JLabel();
        jlabel.setOpaque(true);
        jlabel.setBackground(Color.BLUE);
        jlabel.setBounds(x, y, width, height);
        client.drawingBoard.add(jlabel, 0);
        jlabel.setVisible(true);
    }

    //添加头部
    private void addHadeJlabel() {
        hadeJlabel = new JLabel();
        hadeJlabel.setOpaque(true);
        hadeJlabel.setBackground(Color.black);
        hadeJlabel.setText(" " + popUpNmae);
        hadeJlabel.setForeground(Color.white);
        hadeJlabel.setBounds(0, 0, width, 20);
        hadeJlabel.setVisible(true);
        jlabel.add(hadeJlabel, 0);
    }

    //添加框体
    private void frameJlabelJlabel() {
        frameJlabel = new JLabel();
        frameJlabel.setOpaque(true);
        frameJlabel.setBounds(5, 20, width - 10, height - 75);
        frameJlabel.setVisible(true);
        jlabel.add(frameJlabel, 0);
    }

    //添加尾部
    public void addTailRemoveJlabel() {
        tailJlabel = new JLabel();
        tailJlabel.setOpaque(true);
        tailJlabel.setBackground(Color.black);
        tailJlabel.setBounds(0, height - 50, width, 50);
        tailJlabel.setVisible(true);
        jlabel.add(tailJlabel, 0);
    }

    //新增附加按钮
    public void addTailJlabels(JButton ... jButtons) {
        for (JButton jButton : jButtons) {
            tailJlabel.add(jButton, 0);
            jButtonList.add(jButton);
        }
        client.drawingBoard.repaint();
    }

    //删除表单
    public void delet() {
        client.drawingBoard.remove(jlabel);
        client.drawingBoard.repaint();
    }

}
