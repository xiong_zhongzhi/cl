package com.cl.awt.js.form;

import com.cl.been.Container;

import java.util.HashMap;

/**
 *
 * 表单列表中的操作按钮包装类
 *
 */
public class Operation {

    //操作名称
    private  String perationName;

    //按钮宽度
    private Integer width;

    //钩子方法
    private FormAllPowerful<Container,Container> allPowerful;

    //展示判断公式
    private String formula;

    public Operation(String perationName, Integer width, FormAllPowerful<Container, Container> allPowerful) {
        this.perationName = perationName;
        this.allPowerful = allPowerful;
        this.width = width;
    }

    public String getPerationName() {
        return perationName;
    }

    public void setPerationName(String perationName) {
        this.perationName = perationName;
    }

    public FormAllPowerful<Container, Container> getAllPowerful() {
        return allPowerful;
    }

    public void setAllPowerful(FormAllPowerful<Container, Container> allPowerful) {
        this.allPowerful = allPowerful;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Integer getWidth() {
        return width;
    }
}
