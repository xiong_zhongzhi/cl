package com.cl.awt.js;

import javax.swing.*;
import java.awt.*;

public class MouseStyle {

    // 创建光标对象
    public static final Cursor customCursor1 = Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("src/main/resources/img/1.png").getImage(), new Point(0, 0), "Custom Cursor1");
    public static final Cursor customCursor2 = Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("src/main/resources/img/2.png").getImage(), new Point(0, 0), "Custom Cursor2");

    public static final Cursor customCursor3 = Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("src/main/resources/img/3.png").getImage(), new Point(0, 0), "Custom Cursor3");

    public static final Cursor customCursor4 = Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("src/main/resources/img/4.png").getImage(), new Point(0, 0), "Custom Cursor4");


}
