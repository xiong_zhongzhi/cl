package com.cl.awt.js;

import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.annotation.Resource;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import com.cl.awt.js.hookJlable.RoomMapJlabel;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;

import com.cl.awt.Client;
import com.cl.awt.core.PageCache;
import com.cl.awt.core.MyPage;
import com.cl.awt.core.Route;
import com.cl.been.Container;
import com.cl.handler.Jurisdiction.Drawer;
import com.cl.handler.page.PageUrl;
import com.cl.handler.page.PageUrlHandler;
import com.cl.util.MyExceptionIncomplete;

public class Util extends Js {

    /**
     * @param length    文本框长度
     * @param x         输入框横坐标
     * @param y         输入框纵坐标
     * @param width     输入框款度
     * @param height    输入框长度
     * @param byDefault 输入框默认值
     * @return jTextField 输入框对象
     * @Description: TODO(封装输入栏)
     */
    public JTextField addJTextField(Integer length, int x, int y, int width, int height, String byDefault) {
        // 文本框
        JTextField jTextField = new JTextField(length);
        jTextField.setBounds(x, y, width, height);
        if (byDefault != null && !byDefault.equals("")) {
            jTextField.setText(byDefault);
        }
        return jTextField;
    }

    /**
     * @param length    文本框长度
     * @param x         输入框横坐标
     * @param y         输入框纵坐标
     * @param width     输入框款度
     * @param height    输入框长度
     * @param byDefault 输入框默认值
     * @return jTextField 输入框对象
     * @Description: TODO(封装输入栏)
     */
    public JTextField addJTextField(Integer length, int x, int y, int width, int height, String byDefault, JLabel jLabel) {
        // 文本框
        JTextField jTextField = new JTextField(length);
        jTextField.setBounds(x, y, width, height);
        if (byDefault != null && !byDefault.equals("")) {
            jTextField.setText(byDefault);
        }
        jLabel.add(jTextField);
        return jTextField;
    }

    public LodingPanel setLodingPanel(int x, int y, int width, int height, Color color) {
        return setLodingPanel(x, y, width, height, color, null);
    }

    public LodingPanel setLodingPanel(int x, int y, int width, int height, Color color, JLabel JLabelAdd) {
        LodingPanel lodingPanel = new LodingPanel(color);
        lodingPanel.setBounds(x, y, width, height);
        lodingPanel.show();
        lodingPanel.setOpaque(true);
        if (JLabelAdd != null) {
            JLabelAdd.add(lodingPanel, 0);
        }
        return lodingPanel;
    }


    /**
     * @param length    密码框长度
     * @param x         密码框横坐标
     * @param y         密码框纵坐标
     * @param width     密码框款度
     * @param height    密码框长度
     * @param byDefault 密码框默认值
     * @return jPasswordField 密码框对象
     * @Description: TODO(封装密码框)
     */
    public JPasswordField addJPasswordField(Integer length, int x, int y, int width, int height,
                                            String byDefault) {
        // 密码框
        JPasswordField jPasswordField = new JPasswordField(length);
        jPasswordField.setBounds(x, y, width, height);
        if (byDefault != null && !byDefault.equals("")) {
            jPasswordField.setText(byDefault);
        }
        return jPasswordField;
    }

    /**
     * @param url  页面路由
     * @param data 路由带参
     * @Description: TODO(页面跳转器)
     */
    public void skip(String url, Container data,Boolean flag) {
        PageUrlHandler pageUrlHandler = Drawer.get(PageUrlHandler.class);
        // 获取路由对象
        Route route = pageUrlHandler.routeMap.get(url);
        if (route == null) {
            throw new MyExceptionIncomplete("未找到[" + url + "]的路由对象,请检查对应@PageUrl注解中的命名");
        }
        // 加载page对象
        MyPage myPage = null;
        if (flag){
             myPage = route.getPage();
        }
        if (myPage == null) {
            try {
                Class<?> clazz = Class.forName(route.getUrl());
                Object object = clazz.newInstance();
                //注入
                // 获取实体类的所有属性，返回Field数组
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    Resource resource = field.getAnnotation(Resource.class);
                    if (resource == null) {
                        continue;
                    }
                    Object api = Drawer.get(field.getType());
                    if (api != null) {
                        field.setAccessible(true);
                        field.set(object, api);
                    }
                }
                myPage = (MyPage) object;
                for (Method method : clazz.getMethods()) {
                    if (method.getName().equals("load")) {
                        myPage.load = method;
                        myPage.methodParamNameList = getMethodParamName(method);
                    }
                }
                //缓存
                myPage.pageCache = new PageCache();
                myPage.pageCache.setPageUrl(route.getPageUrl());
                MyPage.client = Client.getClient();
                myPage.drawingBoard = Client.getClient().drawingBoard;
                route.setPage(myPage);
            } catch (Exception e) {
                throw new MyExceptionIncomplete("@PageUrl[" + url + "]的路由对象,转换异常 :", e);
            }
        }
        pageUrlHandler.linkRoute.add(myPage);
        //当前页缓存清空
        if (drawingBoard.myPage != null) {
            drawingBoard.myPage.remove();
        }
        // 进行页面跳转
        myPage.run(data);
        drawingBoard.myPage = myPage;
    }

    /**
     * @param clazz page类
     * @param data  路由带参
     * @Description: TODO(页面跳转器)
     */
    public void skip(Class<? extends MyPage> clazz, Container data,Boolean flag) {
        PageUrl pageUrl = clazz.getAnnotation(PageUrl.class);
        if (pageUrl == null) {
            throw new MyExceptionIncomplete("未找到[" + clazz.getCanonicalName() + "]的路由对象,请检查对应类是否存在@PageUrl注解");
        }
        skip(pageUrl.url(), data,flag);
    }

    /**
     * @param clazz page类
     * @Description: TODO(页面跳转器)
     */
    public void skip(Class<? extends MyPage> clazz,Boolean flag) {
        skip(clazz, new Container(),flag);
    }

    /**
     * @param url 页面路由
     * @Description: TODO(页面跳转器)
     */
    public void skip(String url,Boolean flag) {
        skip(url, new Container(),flag);
    }


    /**
     * @param clazz page类
     * @param data  路由带参
     * @Description: TODO(页面跳转器)
     */
    public void skip(Class<? extends MyPage> clazz, Container data) {
        PageUrl pageUrl = clazz.getAnnotation(PageUrl.class);
        if (pageUrl == null) {
            throw new MyExceptionIncomplete("未找到[" + clazz.getCanonicalName() + "]的路由对象,请检查对应类是否存在@PageUrl注解");
        }
        skip(pageUrl.url(), data,true);
    }

    /**
     * @param clazz page类
     * @Description: TODO(页面跳转器)
     */
    public void skip(Class<? extends MyPage> clazz) {
        skip(clazz, new Container(),true);
    }

    /**
     * @param url 页面路由
     * @Description: TODO(页面跳转器)
     */
    public void skip(String url) {
        skip(url, new Container(),true);
    }

    /**
     * @param method 方法对象
     * @Description: TODO(获取对象参数名称数组)
     */
    public static String[] getMethodParamName(Method method) {
        try {
            LocalVariableTableParameterNameDiscoverer parameterNameDiscoverer = new LocalVariableTableParameterNameDiscoverer();
            return parameterNameDiscoverer.getParameterNames(method);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String[0];
    }

    private LineBorder lb = new LineBorder(Color.BLACK, 1, false);

    public RoomMapJlabel setJLabel(String text, int x, int y, int width, int height, JLabel JLabelAdd, Boolean lbFlag, Integer index) {
        return setJLabel(text, x, y, width, height, JLabelAdd, lbFlag, Color.WHITE, index);
    }

    public RoomMapJlabel setJLabel(String text, int x, int y, int width, int height, Boolean lbFlag) {
        return setJLabel(text, x, y, width, height, null, lbFlag, Color.WHITE, 0);
    }

    public RoomMapJlabel setJLabel(String text, int x, int y, int width, int height, JLabel JLabelAdd, Boolean lbFlag) {
        return setJLabel(text, x, y, width, height, JLabelAdd, lbFlag, Color.WHITE, 0);
    }

    public RoomMapJlabel setJLabel(String text, int x, int y, int width, int height, JLabel JLabelAdd, Boolean lbFlag, Color Color) {
        return setJLabel(text, x, y, width, height, JLabelAdd, lbFlag, Color.WHITE, 0);
    }


    public RoomMapJlabel setJLabel(String text, int x, int y, int width, int height, Boolean lbFlag, Color Color) {
        return setJLabel(text, x, y, width, height, null, lbFlag, Color, null);
    }

    public RoomMapJlabel setJLabel(String text, int x, int y, int width, int height, JLabel JLabelAdd, Boolean lbFlag, Color Color, Integer index) {
        RoomMapJlabel jLabel = new RoomMapJlabel();
        if (lbFlag) {
            jLabel.setBorder(lb);
        }
        jLabel.setBounds(x, y, width, height);
        if ( Color != null){
            jLabel.setBackground(Color);
            jLabel.setOpaque(true);
        }
        jLabel.setText(text);
        jLabel.setVerticalAlignment(JLabel.CENTER);
        jLabel.setHorizontalAlignment(JLabel.CENTER);
        if (JLabelAdd != null) {
            JLabelAdd.add(jLabel, index);
        }
        return jLabel;
    }

    public MyButton setMyButton(String buttonName, int x, int y, int width, int height) {
        return setMyButton(buttonName, x, y, width, height, null, null, null, 0);
    }

    public MyButton setMyButton(String buttonName, int x, int y, int width, int height, JLabel JLabelAdd) {
        return setMyButton(buttonName, x, y, width, height, JLabelAdd, null, null, 0);
    }

    public MyButton setMyButton(String buttonName, int x, int y, int width, int height, JLabel JLabelAdd, ActionListener actionListener) {
        return setMyButton(buttonName, x, y, width, height, JLabelAdd, actionListener, null, 0);
    }

    public MyButton setMyButton(String buttonName, int x, int y, int width, int height, JLabel JLabelAdd, Color color) {
        return setMyButton(buttonName, x, y, width, height, JLabelAdd, null, color, 0);
    }

    public MyButton setMyButton(String buttonName, int x, int y, int width, int height, JLabel JLabelAdd, ActionListener actionListener, Color color, Integer index) {
        MyButton myButton = new MyButton(buttonName);
        myButton.setColor(color);
        myButton.setBounds(x, y, width, height);
        if (actionListener != null) {
            myButton.addActionListener(actionListener);
        }
        if (JLabelAdd != null) {
            JLabelAdd.add(myButton, index);
        }
        return myButton;
    }

    public ImgPanel setImgPanel(String url, int x, int y, int width, int height, JLabel JLabelAdd, Integer index) {
        ImgPanel goodsImg = new ImgPanel();
        goodsImg.setImageHttpURL(url);
        goodsImg.setBounds(x, y, width, height);
        if (JLabelAdd != null) {
            JLabelAdd.add(goodsImg, index);
        }
        return goodsImg;
    }

    public ImgPanel setImgPanel(String url, int x, int y, int width, int height, JLabel JLabelAdd) {

        return setImgPanel(url, x, y, width, height, JLabelAdd, 0);
    }

    public ImgPanel setImgPanel(String url, int x, int y, int width, int height) {
        return setImgPanel(url, x, y, width, height, null, null);
    }

}
