package com.cl.awt;

import java.awt.*;
import javax.swing.JFrame;

public class Client extends JFrame {

	public static Client client;

	public DrawingBoard drawingBoard;

	private Client() {
		// 添加画布
		drawingBoard = new DrawingBoard();
		drawingBoard.setBackground(Color.white);
		drawingBoard.setLayout(null);
		this.add(drawingBoard);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		super.setResizable(true);
		System.setProperty("sun.java2d.noddraw", "true");//加的这句话
		Toolkit tool=super.getToolkit(); //得到一个Toolkit对象
		Image myimage=tool.getImage("C:\\Users\\Administrator\\Desktop\\杂物\\b.jpg"); //由tool获取图像
		super.setIconImage(myimage);

		Thread drawingBoardThread = new Thread(drawingBoard);
		drawingBoardThread.start();
	}

	public static Client getClient() {
		if (client == null) {
			Client.client = new Client();
		}
		return client;
	}

}
