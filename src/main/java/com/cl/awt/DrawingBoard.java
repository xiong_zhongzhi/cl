package com.cl.awt;

import java.awt.Graphics;

import javax.swing.JPanel;

import com.cl.awt.core.MyPage;

public class DrawingBoard extends JPanel implements Runnable{

	private static final long serialVersionUID = 1L;

	public Graphics graphics;

	//当前页
	public MyPage myPage;

	@Override
	public void paint(Graphics g) {
		super.paint(g);
	}

	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
	}


	@Override
	public void run() {
		this.repaint();
	}


}
