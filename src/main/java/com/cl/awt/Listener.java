package com.cl.awt;

import java.awt.event.*;

import com.cl.awt.js.Js;

public class Listener extends Js implements MouseListener, MouseMotionListener, KeyListener {
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    /**
     * 按下
     * @param e
     */
    @Override
    public void mousePressed(MouseEvent e) {

    }

    /**
     * 抬起
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    /**
     * 鼠标移出
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    /**
     * 鼠标移入 移动
     * @param e
     */
    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * 键盘按下
     * @param e
     */
    @Override
    public void keyPressed(KeyEvent e) {
    }

    /**
     * 键盘抬起
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
    }
}
