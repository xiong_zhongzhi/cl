package com.cl.awt;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public abstract class  Monitor implements KeyListener,FocusListener {
	@Override
	public void keyTyped(KeyEvent e) {
		//System.out.println("键入 :" + e.getKeyChar());	
	}

	// 按下某个键时调用此方法。
	@Override
	public void keyPressed(KeyEvent e) {
		//System.out.println("按压 :" + e.getKeyChar());
	}

	// 释放某个键时调用此方法。
	@Override
	public void keyReleased(KeyEvent e) {
		//System.out.println("松开了 :" + e.getKeyChar());
		// JOptionPane.showMessageDialog(null, "你松开了键");
	}
	
	//进入
	@Override
	public void focusGained(FocusEvent e) {
		
	}
	
	//离开
	@Override
	public void focusLost(FocusEvent e) {
		
	}
}
