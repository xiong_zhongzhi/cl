package com.cl.awt.page.roomPage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.annotation.Resource;
import javax.swing.JButton;

import com.cl.api.Response;
import com.cl.api.room.RoomApi;
import com.cl.api.room.dto.RoomDTO;
import com.cl.api.room.request.FindRoomListRequest;
import com.cl.api.room.response.FindRoomListResponse;
import com.cl.awt.core.MyPage;
import com.cl.awt.js.Hint;
import com.cl.handler.page.PageUrl;

@PageUrl( url = "roomList",pageName  = "房间列表")
public class RoomList extends MyPage {

    @Resource
    private RoomApi roomApi;

    // 房间列表
    public void load() {
        // 查询房间列表
        FindRoomListRequest findRoomListRequest = new FindRoomListRequest();
        Response<FindRoomListResponse> findRoomListResponse = roomApi.findRoomList(findRoomListRequest);
        new Hint().run(findRoomListResponse.getState(), findRoomListResponse.getMsg());
        List<RoomDTO> roomDTOList = findRoomListResponse.getData().getRoomDTOList();

        int i = 10;
        for (RoomDTO roomDTO : roomDTOList) {
            JButton roomJButton = new JButton("进入 :" + roomDTO.getRoomName());
            roomListAddActionListener(roomJButton,roomDTO);
            roomJButton.setBounds(150, i, 250, 30);
            client.drawingBoard.add(roomJButton);
            i += 40;
        }
    }

    private void roomListAddActionListener(JButton saveButton,RoomDTO roomDTO ) {
        // 为按钮绑定监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JButton jButton = (JButton) e.getSource();
                // 加入房间通知
                client.setTitle(roomDTO.getRoomName());
                //util.skip(RoomMyPage.class,new Container("roomDTO",roomDTO));
            }
        });
    }
}
