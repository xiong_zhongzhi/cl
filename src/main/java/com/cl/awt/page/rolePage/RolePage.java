package com.cl.awt.page.rolePage;

import com.cl.api.Response;
import com.cl.api.gameRole.GameRoleUserApi;
import com.cl.api.gameRole.dto.GameRoleSkillDTO;
import com.cl.api.gameRole.dto.GameRoleUserDTO;
import com.cl.api.gameRole.response.FindGameRoleUserListResponse;
import com.cl.api.mall.vo.OrderPartVO;
import com.cl.api.userInTime.UserInTimeApi;
import com.cl.api.userInTime.dto.UserInTimeDTO;
import com.cl.api.userInTime.request.UpdateUserInTimeRequest;
import com.cl.api.userInTime.response.FindUserInTimeResponse;
import com.cl.awt.core.MyPage;
import com.cl.awt.js.Hint;
import com.cl.awt.js.ImgPanel;
import com.cl.awt.js.MyButton;
import com.cl.awt.js.floatL.FloatListener;
import com.cl.awt.js.hookJlable.MyGraphics;
import com.cl.awt.js.levelJ.LiveJ;
import com.cl.handler.page.PageUrl;
import com.cl.util.AllPowerful;

import javax.annotation.Resource;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


@PageUrl(url = "RolePage", pageName = "角色栏", rollbackFlag = false)
public class RolePage extends MyPage {

    @Resource
    private GameRoleUserApi gameRoleUserApi;

    @Resource
    private UserInTimeApi userInTimeApi;

    private JLabel roleListShowJLabel;

    private List<GameRoleUserDTO> list;

    //层级控制器
    private LiveJ liveJ;

    // 页面
    public void load(List<OrderPartVO> orderPartVOList, Boolean snapshoot) throws MalformedURLException {

        //返回页跳转
        MyButton rollback = new MyButton("返回");
        rollback.setBounds(0, 1, 200, 60);
        drawingBoard.add(rollback);
        super.rollback(rollback);
        drawingBoard.add(util.setJLabel("", 200, 1, 1200, 60, true));

        //查询及时备战属性接口
        Response<FindGameRoleUserListResponse> findGameRoleUserListResponse = gameRoleUserApi.findGameRoleUserList();
        new Hint().run(findGameRoleUserListResponse.getState(), findGameRoleUserListResponse.getMsg());
        this.list = findGameRoleUserListResponse.getData().getGameRoleUserDTOList();


        liveJ = new LiveJ();
        liveJ.setBounds(0,0,1400,800);
        drawingBoard.add(liveJ);

        JLabel roleListJLabel = util.setJLabel("", 1000, 60, 400, 710,true);
        liveJ.add(roleListJLabel);

        util.setJLabel("角色列表", 0, 0, 400, 60, roleListJLabel, true);
        this.roleListShowJLabel = util.setJLabel("", 0, 60, 400, list.size() * 60, roleListJLabel, false);

        this.showJLabelRoleList();

        if (list.size()>0){
            this.showRole(this.list.get(0));
        }
    }

    private void showRole(GameRoleUserDTO gameRoleUserDTO) {
        int targetLayer = 1; // 要移除的组件的层级值
        for (Component component : liveJ.getComponents()) {
            if (liveJ.getLayer(component) == targetLayer) {
                liveJ.remove(component);
            }
        }
        //渲染
        ImgPanel gameRoleImgPanel = util.setImgPanel(gameRoleUserDTO.getRoleImg(), 1, 61, 398, 398);
        JLabel nature = util.setJLabel("", 400, 60, 600, 260,true);

        liveJ.addAndLayer(nature, 1);
        liveJ.addAndLayer(gameRoleImgPanel, 1);

        util.setJLabel("名称 : " + gameRoleUserDTO.getName(), 10, 10, 260, 30, nature, true);
        util.setJLabel("简介 : " + gameRoleUserDTO.getDescribe(), 10, 45, 260, 200, nature, true);
        util.setJLabel("攻击力 : " + gameRoleUserDTO.getAtk(), 300, 10, 240, 30, nature, true);
        util.setJLabel("攻击距离 : " + gameRoleUserDTO.getSd(), 300, 50, 240, 30, nature, true);
        util.setJLabel("防御力 : " + gameRoleUserDTO.getDef(), 300, 90, 240, 30, nature, true);
        util.setJLabel("生命值 : " + gameRoleUserDTO.getHp(), 300, 130, 240, 30, nature, true);
        util.setJLabel("法力值 : " + gameRoleUserDTO.getSp(), 300, 170, 240, 30, nature, true);
        util.setJLabel("移速 : " + gameRoleUserDTO.getDd(), 300, 210, 240, 30, nature, true);

        JLabel skillJLabel = util.setJLabel("", 400, 320, 600, 140, null, true, 10);
        liveJ.addAndLayer(skillJLabel, 1);
        util.setJLabel("技能列表", 0, 0, 600, 20, skillJLabel, true);
        List<GameRoleSkillDTO> gameRoleSkillDTOList = gameRoleUserDTO.getGameRoleSkillDTOList();
        AtomicInteger index = new AtomicInteger(0);
        for (GameRoleSkillDTO gameRoleSkillDTO : gameRoleSkillDTOList) {
            ImgPanel imgPanel = util.setImgPanel(gameRoleSkillDTO.getImgUrl(), index.getAndIncrement() * (80 + 10) + 10, 40, 80, 80, skillJLabel);
            SkillFloatShow skillFloatShow = new SkillFloatShow();
            FloatListener<SkillFloatShow> floatListener = new FloatListener(imgPanel, skillFloatShow);
            floatListener.setMouseMovedAllPowerful(new AllPowerful<FloatListener, Object>() {
                @Override
                public Object hook(FloatListener floatL) {
                    if (skillFloatShow.getjLabel() == null) {
                        int x = skillJLabel.getX() + imgPanel.getX()+imgPanel.getWidth();
                        int y = skillJLabel.getY() + imgPanel.getY()+imgPanel.getHeight();
                        JLabel jLabel = util.setJLabel("", x,y, 500, 200, null, true, Color.WHITE, 0);
                        util.setJLabel("技能名称 : " + gameRoleSkillDTO.getName(),0,0,500,30,jLabel,true);
                        util.setJLabel("描述 : " + gameRoleSkillDTO.getDescribe(),0,30,500,40,jLabel,true);
                        util.setJLabel("效果 : " + gameRoleSkillDTO.getPracticalDescribe(),0,70,500,110,jLabel,true);
                        util.setJLabel("cd : " + gameRoleSkillDTO.getRefrigeration(),0,180,500,20,jLabel,true);
                        liveJ.addAndLayer(jLabel, 2);
                        skillFloatShow.setjLabel(jLabel);
                        drawingBoard.repaint();
                    }
                    return null;
                }
            });
            floatListener.setMouseExitedAllPowerful(new AllPowerful<FloatListener, Object>() {
                @Override
                public Object hook(FloatListener floatList) {
                    if (skillFloatShow.getjLabel() != null) {
                        liveJ.remove(skillFloatShow.getjLabel());
                        skillFloatShow.setjLabel(null);
                        drawingBoard.repaint();
                    }
                    return null;
                }
            });
        }
        util.setJLabel("装备面板:暂未开发", 0, 460, 400, 310, true);
        util.setJLabel("装备列表:暂未开发", 400, 460, 600, 155, true);
        util.setJLabel("道具列表:暂未开发", 400, 615, 400, 155, true);
        util.setJLabel("道具使用记录:暂未开发", 800, 615, 200, 155, true);
    }

    //出战
    public void goOut(MyButton saveButton, Integer myGameRoleId) {
        // 为按钮绑定监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //变更及时备战信息
                UpdateUserInTimeRequest updateUserInTimeRequest = new UpdateUserInTimeRequest();
                updateUserInTimeRequest.setGameRoleId(myGameRoleId);
                Response response = userInTimeApi.updateUserInTime(updateUserInTimeRequest);
                new Hint().run(response.getState(), response.getMsg());
                if (response.getState() == 0) {
                    showJLabelRoleList();
                }
            }
        });
    }

    //重新渲染
    public void showJLabelRoleList() {
        roleListShowJLabel.removeAll();
        Response<FindUserInTimeResponse> findUserInTimeResponse = userInTimeApi.findUserInTime();
        UserInTimeDTO userInTimeDTO = findUserInTimeResponse.getData().getUserInTimeDTO();
        List<JLabel> roleListListener = new ArrayList<>();
        for (int i = 0; i < this.list.size(); i++) {
            GameRoleUserDTO gameRoleUserDTO = list.get(i);
            Boolean flag = userInTimeDTO.getGameRoleId() != null ? userInTimeDTO.getGameRoleId() == gameRoleUserDTO.getMyGameRoleId() ? true : false : false;
            JLabel ro = util.setJLabel(gameRoleUserDTO.getName(), 0, i * 60, 320, 60, roleListShowJLabel, true, flag ? Color.green : Color.white);
            roleListListener.add(ro);
            AllPowerful<GameRoleUserDTO, GameRoleUserDTO> allPowerful = new AllPowerful<GameRoleUserDTO, GameRoleUserDTO>() {
                @Override
                public GameRoleUserDTO hook() {
                    showRole(gameRoleUserDTO);
                    drawingBoard.repaint();
                    return null;
                }
            };
            RoleListener roleListener = new RoleListener(ro, util, roleListListener, allPowerful);
            ro.addMouseListener(roleListener);
            ro.addMouseMotionListener(roleListener);
            if (flag) {
                util.setJLabel("已出战", 320, i * 60, 80, 60, roleListShowJLabel, true);
            } else {
                MyButton button = new MyButton("未出战");
                button.setBounds(320, i * 60, 80, 60);
                roleListShowJLabel.add(button);
                goOut(button, gameRoleUserDTO.getMyGameRoleId());
            }
        }
    }


}
