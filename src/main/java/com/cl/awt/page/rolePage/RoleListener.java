package com.cl.awt.page.rolePage;

import com.cl.api.gameRole.dto.GameRoleUserDTO;
import com.cl.awt.Listener;
import com.cl.awt.js.Util;
import com.cl.util.AllPowerful;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.util.List;

public class RoleListener extends Listener {

    private JLabel jLabel;
    private JLabel asjLabel;
    private Util util;
    private JLabel jLabelShow;
    private List<JLabel> roleListListener;
    private AllPowerful<GameRoleUserDTO, GameRoleUserDTO> allPowerful;

    public RoleListener(JLabel jLabel, Util util, List<JLabel> roleListListener,AllPowerful<GameRoleUserDTO, GameRoleUserDTO> allPowerful) {
        this.jLabel = jLabel;
        this.util = util;
        this.roleListListener = roleListListener;
        this.allPowerful = allPowerful;
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (jLabelShow != null) {
            jLabel.remove(jLabelShow);
            jLabelShow = null;
            jLabel.repaint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (jLabelShow == null ) {
            jLabelShow = util.setJLabel("点击查看", 10, 20, 60, 20, null, true);
            jLabel.add(jLabelShow, 0);
            jLabel.repaint();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        asjLabel = util.setJLabel("正在查看", 10, 20, 60, 20, null, true);
        for (JLabel jLab:roleListListener) {
            jLab.removeAll();
            try {
                hookRun();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        jLabel.add(asjLabel, 0);
        jLabel.repaint();
    }

    //执行钩子
    public void hookRun() throws Exception {
        allPowerful.hook();
    }

}
