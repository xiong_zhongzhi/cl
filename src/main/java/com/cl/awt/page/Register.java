package com.cl.awt.page;

import com.cl.api.Response;
import com.cl.api.user.UserApi;
import com.cl.api.user.pojo.User;
import com.cl.api.user.request.LoginRequest;
import com.cl.api.user.request.RegisterRequest;
import com.cl.api.user.response.LoginResponse;
import com.cl.awt.core.MyPage;
import com.cl.awt.js.Hint;
import com.cl.awt.js.MyButton;
import com.cl.been.Container;
import com.cl.handler.page.PageUrl;
import com.cl.util.localCache.Cache;

import javax.annotation.Resource;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@PageUrl( url = "register",pageName  = "注册",rollbackFlag = false)
public class Register extends MyPage {

    // 登录调用
	private JTextField account;
    private JTextField userName;


	private JPasswordField password1;
    private JPasswordField password2;
    
    @Resource
    private UserApi userApi;

    public void load() {
        // 渲染
        client.setSize(400, 300);
        client.setLocation(300, 300);


        drawingBoard.add(util.setJLabel("昵称", 20, 15, 40, 30,null,false, Color.white));
        userName = util.addJTextField(10, 90, 15, 250, 30, "");
        drawingBoard.add(userName);

        drawingBoard.add(util.setJLabel("账号", 20, 55, 40, 30,null,false, Color.white));
        account = util.addJTextField(10, 90, 55, 250, 30, "");
        drawingBoard.add(account);

        drawingBoard.add(util.setJLabel("密码", 20, 95, 40, 30,null,false, Color.white));
        password1 = util.addJPasswordField(10,90 , 95, 250, 30, "");
        drawingBoard.add(password1);

        drawingBoard.add(util.setJLabel("确认密码", 20, 135, 60, 30,null,false, Color.white));
        password2 = util.addJPasswordField(10, 90, 135, 250, 30, "");
        drawingBoard.add(password2);

        JButton register = new JButton("注册");
        registerAddActionListener(register);
        register.setBounds(70, 180, 250, 30);
        drawingBoard.add(register);

        MyButton rollback = util.setMyButton("返回登陆",70, 220, 250, 30);
        rollback(rollback);
        drawingBoard.add(rollback);
    }

    //注册
    private void registerAddActionListener(JButton saveButton) {
        // 为按钮绑定监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RegisterRequest registerRequest = new RegisterRequest();
                registerRequest.setUserName(userName.getText());
                registerRequest.setAccount(account.getText());
                registerRequest.setPassword(new String(password1.getPassword()));
                Response response = userApi.register(registerRequest);
                new Hint().run(response.getState(), response.getMsg());
                rollbackExecute();
            }
        });
    }
}
