package com.cl.awt.page;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.cl.awt.core.MyPage;
import com.cl.handler.page.PageUrl;

@PageUrl( url = "reconnection",pageName  = "服务器断链接")
public class Reconnection extends MyPage {

    // 系统输出
    public JTextArea textArea;
    public JScrollPane scrollPane;

    public void load() {
    	String text = "";
        if (textArea == null) {
            if (drawingBoard != null){
                drawingBoard.removeAll();
            }
            client.setSize(290, 200);
            client.setLocation(300, 300);
            scrollPane = new JScrollPane();
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            scrollPane.setBounds(1, 1, drawingBoard.getSize().width, drawingBoard.getSize().height);
            drawingBoard.add(scrollPane);
            textArea = new JTextArea();
            textArea.setBounds(1, 1, drawingBoard.getSize().width, drawingBoard.getSize().height);
            scrollPane.setViewportView(textArea);
        }
        textArea.append(" " + text + "\n");
        textArea.setSelectionStart(textArea.getText().length());
    }

}
