package com.cl.awt.page.login;

import com.cl.api.user.pojo.User;
import com.cl.awt.Listener;
import com.cl.awt.js.hookJlable.RoomMapJlabel;
import com.cl.util.AllPowerful;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.List;

public class LoginListener extends Listener {

    private User user;
    private RoomMapJlabel myJLabel;
    private List<RoomMapJlabel> roomMapJlabels;
    private AllPowerful allPowerful;

    public LoginListener(User user, RoomMapJlabel myJLabel,List<RoomMapJlabel> roomMapJlabels,AllPowerful allPowerful) {
        this.user = user;
        this.myJLabel = myJLabel;
        this.roomMapJlabels = roomMapJlabels;
        this.allPowerful = allPowerful;
    }

    @Override
    public void mouseExited(MouseEvent e) {
        myJLabel.setBackground(Color.white);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        System.out.println("移入");
        System.out.println(user.getName());
        for (RoomMapJlabel roomMapJlabel:roomMapJlabels){
            if (roomMapJlabel == myJLabel){
                roomMapJlabel.setBackground(Color.CYAN);
            }else {
                roomMapJlabel.setBackground(Color.white);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        allPowerful.hook(user);
    }

}
