package com.cl.awt.page.login;

import com.cl.api.Response;
import com.cl.api.user.UserApi;
import com.cl.api.user.pojo.User;
import com.cl.api.user.request.LoginRequest;
import com.cl.api.user.response.LoginResponse;
import com.cl.awt.core.MyPage;
import com.cl.awt.js.Hint;
import com.cl.awt.js.hookJlable.RoomMapJlabel;
import com.cl.awt.js.levelJ.LiveJ;
import com.cl.awt.page.Lobby;
import com.cl.awt.page.Register;
import com.cl.been.Container;
import com.cl.handler.page.PageUrl;
import com.cl.util.AllPowerful;
import com.cl.util.FileToPojo.FileTOPojo;
import com.cl.util.localCache.Cache;

import javax.annotation.Resource;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

@PageUrl(url = "login", pageName = "登录")
public class Login extends MyPage {

    //层级控制器
    private LiveJ liveJ;

    // 登录调用
    private JTextField account;
    private JPasswordField password;

    @Resource
    private UserApi userApi;

    private RoomMapJlabel roomMapJlabel;

    public void load() {
        // 渲染
        client.setSize(400, 300);
        client.setLocation(300, 300);

        liveJ = new LiveJ();
        liveJ.setBounds(0,0,400,300);
        drawingBoard.add(liveJ);

        liveJ.add(util.setJLabel("账 号", 50, 40, 40, 30, null, false, Color.white));
        account = util.addJTextField(10, 90, 40, 180, 30, "");

        liveJ.add(account);

        liveJ.add(util.setJLabel("密 码", 50, 70, 40, 30, null, false, Color.white));
        password = util.addJPasswordField(10, 90, 80, 245, 30, "");
        liveJ.add(password);

        JButton jl = new JButton("记录");
        jl.setBounds(274, 40, 60, 30);
        liveJ.add(jl);

        JButton login = new JButton("登录");
        loginAddActionListener(login);
        login.setBounds(70, 130, 250, 30);
        liveJ.add(login);

        JButton register = new JButton("注册");
        registerAddActionListener(register);
        register.setBounds(70, 170, 250, 30);
        liveJ.add(register);

        // 为按钮绑定监听器
        jl.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (roomMapJlabel != null){
                    liveJ.remove(roomMapJlabel);
                    roomMapJlabel = null;
                    password.setEnabled(true);
                    login.setEnabled(true);
                    register.setEnabled(true);
                    liveJ.repaint();
                    return;
                }
                //渲染历史登录记录
                List<User> userList = FileTOPojo.getSettingList(LOGIN_LOG, User.class);
                if (userList.size() > 0){
                    //影响展示的按钮设置禁选
                    password.setEnabled(false);
                    login.setEnabled(false);
                    register.setEnabled(false);
                    roomMapJlabel = util.setJLabel("", 50, 75, 300, 40*userList.size(), null, true, Color.white);
                    List<RoomMapJlabel> roomMapJlabels = new ArrayList<RoomMapJlabel>();
                    for(int i = 0;i < userList.size();i++){
                        User user = userList.get(i);
                        RoomMapJlabel jlabel = util.setJLabel("", 0, i*40, 300, 40, roomMapJlabel, true, Color.white);
                        util.setJLabel(user.getAccount(), 0, 0, 300, 20, jlabel, true, Color.white);
                        RoomMapJlabel myJLabel = util.setJLabel(user.getName(), 0, 20, 300, 20, jlabel, true, Color.white);
                        roomMapJlabels.add(myJLabel);

                        AllPowerful<User, User> allPowerful = new AllPowerful<User, User>() {
                            @Override
                            public User hook(User u) {
                                liveJ.remove(roomMapJlabel);
                                roomMapJlabel = null;
                                password.setEnabled(true);
                                login.setEnabled(true);
                                register.setEnabled(true);
                                liveJ.repaint();
                                //放入数据
                                account.setText(u.getAccount());
                                password.setText(u.getPassword());
                                drawingBoard.repaint();
                                return null;
                            }
                        };

                        LoginListener loginListener = new LoginListener(user,myJLabel,roomMapJlabels,allPowerful);
                        myJLabel.addMouseListener(loginListener);
                        myJLabel.addMouseMotionListener(loginListener);
                    }
                    liveJ.add(roomMapJlabel,2);
                    liveJ.repaint();
                }
            }
        });
    }

    private static final String LOGIN_LOG = "LoginLog";

    private void loginAddActionListener(JButton saveButton) {
        // 为按钮绑定监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoginRequest loginRequest = new LoginRequest();
                loginRequest.setAccount(account.getText());
                loginRequest.setPassword(new String(password.getPassword()));
                Response<LoginResponse> response = userApi.login(loginRequest);
                new Hint().run(response.getState(), response.getMsg());
                // 跳转大厅
                if (response.getState() == 0) {
                    User user = new User();
                    user.setId(response.getData().getId());
                    user.setName(response.getData().getUserName());
                    user.setPhoto(response.getData().getPhoto());
                    //保存登录信息
                    List<User> userList = FileTOPojo.getSettingList(LOGIN_LOG, User.class);
                    for (int i = 0; i < userList.size(); i++) {
                        if (userList.get(i).getAccount().equals(loginRequest.getAccount())) {
                            userList.remove(i);
                            break;
                        }
                    }
                    User u = new User();
                    u.setAccount(loginRequest.getAccount());
                    u.setPassword(loginRequest.getPassword());
                    u.setName(user.getName());
                    userList.add(0, u);
                    FileTOPojo.setSetting(LOGIN_LOG,userList);

                    //赋值全局信息
                    Cache.container.put("user", user);
                    Cache.container.put("token", response.getData().getToken());
                    util.skip(Lobby.class, new Container());

                }
            }
        });
    }

    //注册
    private void registerAddActionListener(JButton saveButton) {
        // 为按钮绑定监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                util.skip(Register.class);
            }
        });
    }
}
