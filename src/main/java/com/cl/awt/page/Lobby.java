package com.cl.awt.page;

import com.cl.api.account.Account;
import com.cl.api.account.response.FindAccountCLResponse;
import com.cl.api.user.pojo.User;
import com.cl.awt.core.MyPage;
import com.cl.awt.js.MyButton;
import com.cl.awt.page.establishRoom.EstablishRoom;
import com.cl.awt.page.mall.Mall;
import com.cl.awt.page.play.PlaYGame;
import com.cl.awt.page.rolePage.RolePage;
import com.cl.awt.page.roomPage.RoomList;
import com.cl.handler.page.PageUrl;
import com.cl.util.localCache.Cache;

import javax.annotation.Resource;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;


@PageUrl(url = "lobby", pageName = "游戏大厅", rollbackFlag = false)
public class Lobby extends MyPage {

    @Resource
    private Account account;

    // 页面
    public void load() throws MalformedURLException {

        client.setSize(1400, 800);
        client.setLocation(230, 150);

        //角色页跳转
        MyButton rolePage = util.setMyButton("角色", 0, 0, 200, 100);
        drawingBoard.add(rolePage);
        lobbySkip(rolePage, RolePage.class);

        //背包页面跳转
        MyButton knapsackPage = util.setMyButton("背包", 200, 0, 200, 100);
        drawingBoard.add(knapsackPage);

        //进入游戏
        MyButton playGamePage = util.setMyButton("进入无尽位面", 400, 0, 700, 100);
        drawingBoard.add(playGamePage);
        lobbySkip(playGamePage, PlaYGame.class, false);

        //用户信息
        User user = Cache.container.get("user", User.class);
        JLabel userJLabel = util.setJLabel("", 1100, 0, 300, 100, false);
        drawingBoard.add(userJLabel);
        //头像
        util.setImgPanel(user.getPhoto(), 0, 0, 100, 100, userJLabel);
        //用户名称渲染
        util.setJLabel("昵称 : ", 150, 0, 50, 50, userJLabel, false);
        util.setJLabel(user.getName(), 200, 0, 100, 50, userJLabel, false);
        //查询账号
        FindAccountCLResponse findAccountCLResponse = account.findAccount().getData();
        //用户积分渲染
        util.setJLabel("积分 : ", 150, 50, 50, 50, userJLabel, false);
        util.setJLabel(findAccountCLResponse.getIntegral().toString(), 200, 50, 100, 50, userJLabel, false);

        //公告栏
        JLabel bulletinBoard = util.setJLabel("公告栏:暂未开发", 0, 100, 400, 400, true);
        drawingBoard.add(bulletinBoard);

        //商城
        MyButton mallPage = new MyButton("商城");
        mallPage.setBounds(400, 100, 500, 400);
        drawingBoard.add(mallPage);
        lobbySkip(mallPage, Mall.class);

        //任务列表
        JLabel taskList = util.setJLabel("", 900, 100, 500, 700,true);
        drawingBoard.add(taskList);
        //标题
        util.setJLabel("任务栏 : ", 0, 0, 500, 60, taskList, true);
        //成就任务
        util.setJLabel("成就任务", 0, 60, 500, 30, taskList, true);
        util.setJLabel("暂未开发", 0, 90, 500, 180, taskList, true);
        //周常任务
        util.setJLabel("周常任务", 0, 270, 500, 30, taskList, true);
        util.setJLabel("暂未开发", 0, 300, 500, 180, taskList, true);
        //活动任务
        util.setJLabel("活动任务", 0, 480, 500, 30, taskList, true);
        util.setJLabel("暂未开发", 0, 510, 500, 180, taskList, true);

        //创造位面
        MyButton createPage = new MyButton("创造位面");
        createPage.setBounds(0, 500, 300, 300);
        client.drawingBoard.add(createPage);
        lobbySkip(createPage, EstablishRoom.class);

        //加入位面
        MyButton roomListPage = new MyButton("加入位面");
        roomListPage.setBounds(300, 500, 300, 300);
        client.drawingBoard.add(roomListPage);
        lobbySkip(roomListPage, RoomList.class);

        //位面信标
        MyButton beaconPage = new MyButton("位面信标");
        beaconPage.setBounds(600, 500, 300, 300);
        client.drawingBoard.add(beaconPage);
    }

    //页面跳转功能
    private void lobbySkip(JButton saveButton, Class c) {
        // 为按钮绑定监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                util.skip(c);
            }
        });
    }

    private void lobbySkip(JButton saveButton, Class c ,Boolean flag) {
        // 为按钮绑定监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                util.skip(c,flag);
            }
        });
    }

}
