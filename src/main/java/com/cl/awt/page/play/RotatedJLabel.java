package com.cl.awt.page.play;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class RotatedJLabel extends JLabel {
    private double rotationAngle;  // 旋转角度（以弧度表示）

    public RotatedJLabel(String text) {
        super(text);
        rotationAngle = 0.0;
    }

    public void setRotationAngle(double angle) {
        rotationAngle = angle;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g.create();
        int width = getWidth();
        int height = getHeight();

        // 设置绘制环境，包括字体、颜色等
        g2d.setFont(getFont());
        g2d.setColor(getForeground());

        // 将坐标原点移动到JLabel的中心
        g2d.translate(width / 2, height / 2);

        // 旋转坐标系统
        g2d.rotate(rotationAngle);

        // 将坐标原点移回左上角
        g2d.translate(-width / 2, -height / 2);

        // 绘制JLabel的内容
        super.paintComponent(g2d);

        g2d.dispose();
    }

}
