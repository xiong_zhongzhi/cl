package com.cl.awt.page.play.been;

import com.cl.awt.js.ImgPanel;
import com.cl.instructlibrary.been.MapInUser;

import javax.swing.*;

public class UserLocation {

    public UserLocation(ImgPanel jLabelLocation, JLabel jLabelName, MapInUser mapInUser) {
        this.jLabelLocation = jLabelLocation;
        this.jLabelName = jLabelName;
        this.mapInUser = mapInUser;
    }

    private ImgPanel jLabelLocation;

    private JLabel jLabelName;

    private MapInUser mapInUser;

    public ImgPanel getjLabelLocation() {
        return jLabelLocation;
    }

    public void setjLabelLocation(ImgPanel jLabelLocation) {
        this.jLabelLocation = jLabelLocation;
    }

    public JLabel getjLabelName() {
        return jLabelName;
    }

    public void setjLabelName(JLabel jLabelName) {
        this.jLabelName = jLabelName;
    }

    public MapInUser getMapInUser() {
        return mapInUser;
    }

    public void setMapInUser(MapInUser mapInUser) {
        this.mapInUser = mapInUser;
    }
}
