package com.cl.awt.page.play;

import com.alibaba.fastjson.JSONArray;
import com.cl.api.Response;
import com.cl.api.room.DivApi;
import com.cl.api.room.dto.DivDTO;
import com.cl.api.room.dto.RoomMapDTO;
import com.cl.api.room.pojo.Coordinates;
import com.cl.api.room.response.FindDivListResponse;
import com.cl.api.room.vo.RoomMapVO;
import com.cl.api.skill.dto.SkillDTO;
import com.cl.api.user.pojo.User;
import com.cl.awt.core.MyPage;
import com.cl.awt.core.blackHole.BlackHoleHandle;
import com.cl.awt.core.geometry.Line;
import com.cl.awt.core.geometry.Point;
import com.cl.awt.js.*;
import com.cl.awt.js.floatL.FloatListener;
import com.cl.awt.js.hookJlable.MyGraphics;
import com.cl.awt.js.hookJlable.RoomMapJlabel;
import com.cl.awt.js.levelJ.LiveJ;
import com.cl.awt.js.math.LineMath;
import com.cl.awt.js.tree.quadtree.BoundingBox;
import com.cl.awt.js.tree.quadtree.Quadtree;
import com.cl.awt.page.establishRoom.UserAttributes;
import com.cl.awt.page.play.been.UserLocation;
import com.cl.awt.page.rolePage.SkillFloatShow;
import com.cl.been.Container;
import com.cl.been.Msg;
import com.cl.handler.page.PageUrl;
import com.cl.instructlibrary.been.*;
import com.cl.link.Link;
import com.cl.util.AllPowerful;
import com.cl.util.localCache.Cache;
import org.springframework.beans.BeanUtils;

import javax.annotation.Resource;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.math.BigDecimal;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;


@PageUrl(url = "PlaYGame", pageName = "无尽位面降临位面匹配中", rollbackFlag = false)
public class PlaYGame extends MyPage {

    @Resource
    private DivApi divApi;

    private LodingPanel lodingPanel;

    // 正方形长宽数值
    private final int zhi = 80;

    //房间黑洞
    private BlackHoleHandle<Coordinates> blackHoleHandle;

    User user = (User) Cache.container.get("user");

    private int jLabelNameKuan = 200;
    private int jLabelNameGao = 28;

    //玩家对象map
    private Map<String, UserLocation> userLocationMap = new HashMap<>();

    //本人对象UserLocation
    UserLocation userLocation;

    UserAttributes userAttributes = new UserAttributes();

    //房间画布
    public RoomMapJlabel roomMapJlabel;

    //技能栏
    private RoomMapJlabel skillSJLabel;

    //技能画布
    private RoomMapJlabel skillShowJLabel;

    //触发主动技能类型
    public volatile Integer myPointToType;

    //当前触发的技能
    private SkillDTO mySkillDTO;

    // 页面
    public void load() {

        //添加层级控制器
        liveJ = new LiveJ();
        liveJ.setBounds(0, 0, 1400, 800);
        drawingBoard.add(liveJ);

        //加载动画
        lodingPanel = util.setLodingPanel(0, 0, 1400, 800, Color.RED);
        liveJ.addAndLayer(lodingPanel, 3);
        drawingBoard.setCursor(MouseStyle.customCursor4);

        loadModel();
    }

    private int size = 0;

    //匹配位面
    public void loadModel() {
        if (size <= 3) {
            //链接服务端匹配位面
            Link.link.write(new Msg("无尽位面降临位面匹配"));
            size += 1;
        } else {
            //退出
            new Hint().run(-1, "未能匹配到合适的位面");
            System.out.println("调用了几次?");
            this.rollbackExecute();
        }
    }

    //消息广播
    public void broadcast(String information){
        RoomMapJlabel text = util.setJLabel("",100, 100,360, 100, false, null);
        liveJ.addAndLayer(text, 1);
        text.setOpaque(false);
        text.allPowerfulList.add(new AllPowerful<MyGraphics, Container>() {
            @Override
            public Container hook(MyGraphics myGraphics) {
                Graphics g = myGraphics.getGraphics();
                Graphics2D g2 = (Graphics2D) g;
                // 设置文字抗锯齿渲染
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                // 绘制文字描边
                String text =information;
                Font font = new Font("微软雅黑", Font.BOLD, 20);
                Color foregroundColor = Color.BLACK;
                Color outlineColor = Color.CYAN;
                int outlineSize = 2;
                g2.setFont(font);
                int x = 360 / 2 - g2.getFontMetrics().stringWidth(text) / 2;
                int y = 20;
                // 绘制描边效果
                g2.setColor(outlineColor);
                for (int dx = -outlineSize; dx <= outlineSize; dx++) {
                    for (int dy = -outlineSize; dy <= outlineSize; dy++) {
                        if (dx != 0 || dy != 0) {
                            g2.drawString(text, x + dx, y + dy);
                        }
                    }
                }
                // 绘制前景色文字
                g2.setColor(foregroundColor);
                g2.drawString(text, x, y);
                return null;
            }
        });
        drawingBoard.repaint();
    }

    public void renderer(List<RoomMapDTO> roomMapList, RoomDTOExtend roomDTOExtend, Map<String, MapInUser> mapInUserMap) {

        client.setTitle(roomDTOExtend.getRoomName());

        lodingPanel.setEndBfb(1);

        //菜单栏
        JLabel escJLabel = util.setJLabel("", 450, 200, 500, 500, true, Color.white);
        MyButton escButton = util.setMyButton("菜单", 0, 0, 200, 60, null, new ActionListener() {
            private Boolean flag = false;

            @Override
            public void actionPerformed(ActionEvent e) {
                if (!flag) {
                    liveJ.addAndLayer(escJLabel, 2);
                    drawingBoard.repaint();
                    flag = true;
                } else {
                    liveJ.remove(escJLabel);
                    drawingBoard.repaint();
                    flag = false;
                }
            }
        }, Color.white, 1);
        liveJ.addAndLayer(escButton, 1);

        util.setMyButton("退出位面", 100, 100, 300, 100, escJLabel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Link.link.write(new Msg("退出位面"));
                drawingBoard.setCursor(Cursor.getDefaultCursor());
                rollbackExecute();
            }
        });

        //绘制技能栏
        skillSJLabel = util.setJLabel("", 460, 670, 800, 100, true, Color.WHITE);
        liveJ.addAndLayer(skillSJLabel, 1);

        //玩家属性栏
        userAttributes.userGameRole = util.setJLabel("", 320, 630, 140, 140, true, Color.white);
        liveJ.addAndLayer(userAttributes.userGameRole, 1);
        userAttributes.userAttributes = util.setJLabel("", 120, 630, 200, 140, true, Color.white);
        liveJ.addAndLayer(userAttributes.userAttributes, 1);

        //生命值
        userAttributes.hp = util.setJLabel("", 460, 630, 800, 20, null, true, Color.cyan);
        liveJ.addAndLayer(userAttributes.hp, 1);

        //法力值
        userAttributes.sp = util.setJLabel("", 460, 650, 800, 20, null, true, Color.BLACK);
        liveJ.addAndLayer(userAttributes.sp, 1);

        // 开启鼠标监听
        RoomPageListener roomPageListener = new RoomPageListener(this);
        drawingBoard.addMouseListener(roomPageListener);
        drawingBoard.addMouseMotionListener(roomPageListener);
        drawingBoard.addKeyListener(roomPageListener);
        drawingBoard.requestFocusInWindow(); //让其获得焦点，这样才能是键盘监听能够正常使用
        loadMap(roomMapList, roomDTOExtend, mapInUserMap);
    }

    //层级控制器
    private LiveJ liveJ;

    /**
     * 地图四叉树
     */
    private Quadtree mapQuadtree;

    public void loadMap(List<RoomMapDTO> roomMapDTOList, RoomDTOExtend roomDTOExtend, Map<String, MapInUser> mapInUserMap) {

        Response<FindDivListResponse> findDivListResponse = divApi.queryDiv();
        //ew Hint().run(findDivListResponse.getState(), findDivListResponse.getMsg());
        List<DivDTO> divDTOList = findDivListResponse.getData().getDivDTOList();
        Map<Integer, DivDTO> divDTOMap = new HashMap<>();
        divDTOList.forEach(divDTO -> divDTOMap.put(divDTO.getDivId(), divDTO));

        //游戏地图画布
        roomMapJlabel = util.setJLabel("", 220, 100, zhi * roomDTOExtend.getWidth(), zhi * roomDTOExtend.getHigh(), null, true, 2);
        liveJ.addAndLayer(roomMapJlabel, 0);
        //游戏技能画布
        skillShowJLabel = util.setJLabel("", 220, 100, zhi * roomDTOExtend.getWidth(), zhi * roomDTOExtend.getHigh(), null, true, null, 2);
        liveJ.addAndLayer(skillShowJLabel, 1);

        mapQuadtree = new Quadtree(0, new BoundingBox(0, 0, zhi * roomDTOExtend.getWidth(), zhi * roomDTOExtend.getHigh()));

        // 获取所有网格绘制线
        List<Line> lineList = new ArrayList<>();
        List<Line> lineListjs = new ArrayList<>();
        for (int i = 0; i <= roomDTOExtend.getWidth(); i++) {
            Line line = new Line(i * zhi, 0, i * zhi, roomDTOExtend.getHigh() * zhi);
            lineList.add(line);
            lineListjs.add(line);
        }

        // 黑洞点
        List<com.cl.awt.core.geometry.Point> pointList = new ArrayList<>();
        for (int i = 0; i <= roomDTOExtend.getHigh(); i++) {
            Line line = new Line(0, i * zhi, roomDTOExtend.getWidth() * zhi, i * zhi);
            lineList.add(line);
            // 计算交集黑洞点
            if (i == roomDTOExtend.getHigh()) {
                continue;
            }
            for (int z = 0; z < lineListjs.size() - 1; z++) {
                Line lineThis = lineListjs.get(z);
                com.cl.awt.core.geometry.Point<Coordinates> point = LineMath.lineIntersect(new com.cl.awt.core.geometry.Point(line.starx, line.stary), new com.cl.awt.core.geometry.Point(line.endx, line.endy),
                        new com.cl.awt.core.geometry.Point(lineThis.starx, lineThis.stary), new Point(lineThis.endx, lineThis.endy));
                //缓存节点坐标
                point.setInformation(new Coordinates(z, i));
                if (point != null) {
                    pointList.add(point);
                }
            }
        }
        this.blackHoleHandle = new BlackHoleHandle(pointList, 40);
        blackHoleHandle.scopeDebugging(roomMapJlabel, false);

        // 网格画布
        roomMapJlabel.allPowerfulList.add(new AllPowerful<MyGraphics, com.cl.been.Container>() {
            @Override
            public com.cl.been.Container hook(MyGraphics myGraphics) {
                Graphics g = myGraphics.getGraphics();
                g.setColor(Color.black);
                for (Line line : lineList) {
                    g.drawLine(line.getStarx(), line.getStary(), line.getEndx(), line.getEndy());
                }
                return null;
            }
        });

        //渲染新数据
        roomMapDTOList.forEach(roomMapDTO -> {
            blackHoleHandle.getBlackHoleList().forEach(blackHole -> {
                //比对坐标
                Coordinates coordinates = blackHole.getInformation();
                if (roomMapDTO.getX() == coordinates.getPointX() && roomMapDTO.getY() == coordinates.getPointY()) {
                    ImgPanel imgPanel = util.setImgPanel(divDTOMap.get(roomMapDTO.getMapType()).getDivImg(), blackHole.getX(), blackHole.getY(), zhi, zhi, roomMapJlabel);
                    imgPanel.setKey(roomMapDTO.getMapType());
                    RoomMapVO roomMapVO = new RoomMapVO();

                    //存储地图信息
                    BeanUtils.copyProperties(roomMapDTO, roomMapVO);
                    roomMapVO.setImgPanel(imgPanel);

                    //点击处理
                    BoundingBox divBoundingBox = new BoundingBox(imgPanel.getX(), imgPanel.getY(), zhi, zhi);
                    divBoundingBox.setRenderAllPowerful(new AllPowerful<BoundingBox, Object>() {
                        @Override
                        public Container hook(BoundingBox gameObject) {
                            return null;
                        }
                    });
                    mapQuadtree.insert(divBoundingBox);
                    return;
                }
            });
        });

        //渲染玩家角色
        List<String> listId = Container.getkeys(mapInUserMap);
        for (String id : listId) {
            MapInUser mapInUser = mapInUserMap.get(id);
            addNewUser(mapInUser);
        }

        //关闭加载渲染显示
        lodingPanel.setEndBfb(100);
        while (lodingPanel.getBfb() != 100) {
            try {
                Thread.sleep(500);
            } catch (Exception e) {
            }
        }
        liveJ.remove(lodingPanel);
        drawingBoard.repaint();

    }

    private AllPowerful skillShowJLabelAllPowerful;

    //新玩家加入
    public void addListNewUser(List<MapInUser> addMapInUser) {
        for (MapInUser mapInUser :
                addMapInUser) {
            addNewUser(mapInUser);
        }
    }

    //自身属性渲染
    public void applyUser(MapInUser mapInUser) {
        UserLocation userLocation = userLocationMap.get(mapInUser.getUserDTO().getId());
        userLocation.getMapInUser().getGameRoleUserDTO().setSp(mapInUser.getGameRoleUserDTO().getSp());
        userLocation.getMapInUser().getGameRoleUserDTO().setHp(mapInUser.getGameRoleUserDTO().getHp());
    }

    //渲染玩家
    public void addNewUser(MapInUser mapInUser) {
        //渲染玩家位置
        ImgPanel jLabelLocation = util.setImgPanel(mapInUser.getGameRoleUserDTO().getRoleImg(),
                mapInUser.getX(), mapInUser.getY(),
                mapInUser.getGameRoleUserDTO().getWidth(), mapInUser.getGameRoleUserDTO().getHeight());
        roomMapJlabel.add(jLabelLocation, 0);
        RoomMapJlabel text = util.setJLabel("", jLabelLocation.getX() - (jLabelNameKuan - mapInUser.getGameRoleUserDTO().getWidth()) / 2, jLabelLocation.getY() - jLabelNameGao,
                jLabelNameKuan, jLabelNameGao, false, null);
        roomMapJlabel.add(text, 0);
        text.setOpaque(false);
        text.allPowerfulList.add(new AllPowerful<MyGraphics, Container>() {
            @Override
            public Container hook(MyGraphics myGraphics) {
                Graphics g = myGraphics.getGraphics();
                Graphics2D g2 = (Graphics2D) g;

                // 设置文字抗锯齿渲染
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                // 绘制文字描边
                String text = mapInUser.getUserDTO().getUserName();
                Font font = new Font("微软雅黑", Font.BOLD, 20);
                Color foregroundColor = Color.BLACK;
                Color outlineColor = Color.CYAN;
                int outlineSize = 2;
                g2.setFont(font);
                int x = jLabelNameKuan / 2 - g2.getFontMetrics().stringWidth(text) / 2;
                int y = 20;
                // 绘制描边效果
                g2.setColor(outlineColor);
                for (int dx = -outlineSize; dx <= outlineSize; dx++) {
                    for (int dy = -outlineSize; dy <= outlineSize; dy++) {
                        if (dx != 0 || dy != 0) {
                            g2.drawString(text, x + dx, y + dy);
                        }
                    }
                }
                // 绘制前景色文字
                g2.setColor(foregroundColor);
                g2.drawString(text, x, y);
                return null;
            }
        });

        drawingBoard.repaint();
        UserLocation userLocation = new UserLocation(jLabelLocation, text, mapInUser);
        if (user.getId().equals(mapInUser.getUserDTO().getId())) {
            this.userLocation = userLocation;
            //渲染玩家自身的属性栏
            userAttributes.userGameRoleImgPanel = util.setImgPanel(mapInUser.getGameRoleUserDTO().getRoleImg(), 10, 10, 120, 120, userAttributes.userGameRole);

            //攻击力
            util.setJLabel("攻击 : " + mapInUser.getGameRoleUserDTO().getAtk(), 10, 30, 80, 30, userAttributes.userAttributes, false, null, 0);
            util.setJLabel("攻距 : " + mapInUser.getGameRoleUserDTO().getSd(), 110, 30, 80, 30, userAttributes.userAttributes, false, null, 0);
            util.setJLabel("防御 : " + mapInUser.getGameRoleUserDTO().getDef(), 10, 80, 80, 30, userAttributes.userAttributes, false, null, 0);
            util.setJLabel("移速 : " + mapInUser.getGameRoleUserDTO().getDd(), 110, 80, 80, 30, userAttributes.userAttributes, false, null, 0);

            //生命条渲染
            AllPowerful allPowerful = new AllPowerful<MyGraphics, com.cl.been.Container>() {
                @Override
                public com.cl.been.Container hook(MyGraphics myGraphics) {
                    Graphics2D g2d = (Graphics2D) myGraphics.getGraphics();
                    // 设置抗锯齿渲染
                    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                    int barWidth = userAttributes.hp.getWidth(); // 血条的宽度
                    int barHeight = userAttributes.hp.getHeight(); // 血条的高度

                    int remainingWidth = (int) ((mapInUser.getGameRoleUserDTO().getHp() / (double) mapInUser.getGameRoleUserDTO().getRealityHp()) * barWidth); // 剩余血量的宽度

                    // 绘制血条背景
                    g2d.setColor(Color.black);
                    g2d.fillRect(0, 0, barWidth, barHeight);

                    // 绘制剩余血量
                    g2d.setColor(Color.GREEN);
                    g2d.fillRect(0, 0, remainingWidth, barHeight);

                    // 绘制血量数据
                    g2d.setColor(Color.white);
                    g2d.setFont(new Font("Arial", Font.BOLD, 12));
                    String healthText = mapInUser.getGameRoleUserDTO().getHp() + " / " + mapInUser.getGameRoleUserDTO().getRealityHp();
                    Rectangle2D textBounds = g2d.getFontMetrics().getStringBounds(healthText, g2d);
                    int textX = (int) (barWidth / 2 - textBounds.getWidth() / 2);
                    int textY = (int) (barHeight / 2 + textBounds.getHeight() / 2);
                    g2d.drawString(healthText, textX, textY);
                    return null;
                }
            };
            userAttributes.hp.allPowerfulList.add(allPowerful);

            //蓝条渲染
            AllPowerful allPowerfulSp = new AllPowerful<MyGraphics, com.cl.been.Container>() {
                @Override
                public com.cl.been.Container hook(MyGraphics myGraphics) {
                    Graphics2D g2d = (Graphics2D) myGraphics.getGraphics();
                    // 设置抗锯齿渲染
                    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

                    int barWidth = userAttributes.hp.getWidth(); // 蓝量的宽度
                    int barHeight = userAttributes.hp.getHeight(); // 蓝量的高度

                    int remainingWidth = (int) (((double) mapInUser.getGameRoleUserDTO().getRealitySp() / mapInUser.getGameRoleUserDTO().getSp()) * barWidth); // 剩余蓝量的宽度

                    // 绘制蓝条背景
                    g2d.setColor(Color.black);
                    g2d.fillRect(0, 0, barWidth, barHeight);

                    // 绘制剩余蓝量
                    g2d.setColor(Color.BLUE);
                    g2d.fillRect(0, 0, remainingWidth, barHeight);

                    // 绘制蓝量数据
                    g2d.setColor(Color.white);
                    g2d.setFont(new Font("Arial", Font.BOLD, 12));
                    String healthText = mapInUser.getGameRoleUserDTO().getRealitySp() + " / " + mapInUser.getGameRoleUserDTO().getSp();
                    Rectangle2D textBounds = g2d.getFontMetrics().getStringBounds(healthText, g2d);
                    int textX = (int) (barWidth / 2 - textBounds.getWidth() / 2);
                    int textY = (int) (barHeight / 2 + textBounds.getHeight() / 2);
                    g2d.drawString(healthText, textX, textY);
                    return null;
                }
            };
            userAttributes.sp.allPowerfulList.add(allPowerfulSp);
        }
        userLocationMap.put(mapInUser.getUserDTO().getId(), userLocation);
    }

    //玩家位置变更
    public void userMove(List<MapInUser> mapInUserList) {
        for (MapInUser mapInUser : mapInUserList) {
            UserLocation userLocation = userLocationMap.get(mapInUser.getUserDTO().getId());
            userLocation.getMapInUser().setX(mapInUser.getX());
            userLocation.getMapInUser().setY(mapInUser.getY());
            userLocation.getjLabelLocation().setLocation(mapInUser.getX(), mapInUser.getY());
            userLocation.getjLabelName().setLocation(
                    userLocation.getjLabelLocation().getX() - (jLabelNameKuan - mapInUser.getGameRoleUserDTO().getWidth()) / 2,
                    userLocation.getjLabelLocation().getY() - jLabelNameGao);
        }
        drawingBoard.repaint();
    }

    //其它玩家退出房间
    public void userQuitGame(List<MapInUser> mapInUserList) {
        for (MapInUser mapInUser : mapInUserList) {
            UserLocation userLocation = userLocationMap.get(mapInUser.getUserDTO().getId());
            if (userLocation != null) {
                roomMapJlabel.remove(userLocation.getjLabelLocation());
                roomMapJlabel.remove(userLocation.getjLabelName());
                userLocationMap.remove(mapInUser.getUserDTO().getId());
            }
        }
        drawingBoard.repaint();
    }

    //房间移动
    public void roomMapJlabelMove(int x, int y) {
        if (roomMapJlabel != null) {
            roomMapJlabel.setLocation(roomMapJlabel.getX() + x, roomMapJlabel.getY() + y);
            skillShowJLabel.setLocation(roomMapJlabel.getX(), roomMapJlabel.getY());
        }
    }

    //清空房间本地缓存
    public void removeCache() {
        userLocationMap = new HashMap<>();
    }

    //删除技能辅助
    public void skillMyPointToTypeRemove() {
        switch (myPointToType) {
            case 3:
                myPointToType = null;
                if (skillShowJLabelAllPowerful != null) {
                    skillShowJLabel.allPowerfulList.remove(skillShowJLabelAllPowerful);
                }
                skillShowJLabel.repaint();
                break;
            default:
                break;
        }
    }

    //渲染3类技能辅助
    public void skillMyPointToType3(Point skillPoint) {
        Point myPoint = new Point(userLocation.getjLabelLocation().getX() + userLocation.getMapInUser().getGameRoleUserDTO().getWidth() / 2,
                userLocation.getjLabelLocation().getY() + userLocation.getMapInUser().getGameRoleUserDTO().getHeight() / 2);

        BigDecimal startX = BigDecimal.valueOf(myPoint.getX());
        BigDecimal startY = BigDecimal.valueOf(myPoint.getY());
        BigDecimal endX = BigDecimal.valueOf(skillPoint.getX());
        BigDecimal endY = BigDecimal.valueOf(skillPoint.getY());

        BigDecimal dx = endX.subtract(startX);
        BigDecimal dy = endY.subtract(startY);
        BigDecimal angle = BigDecimal.valueOf(Math.atan2(dy.doubleValue(), dx.doubleValue()));

        List<Point> pointList = JSONArray.parseArray(mySkillDTO.getModel(), Point.class);
        for (Point point : pointList) {
            //根据实际主角坐标换算辅助线xy坐标
            point.x = userLocation.getMapInUser().getX() + point.x;
            point.y = userLocation.getMapInUser().getY() + point.y;
            point.x = point.x * zhi;
            point.y = point.y * zhi;
        }

        if (skillShowJLabelAllPowerful != null) {
            skillShowJLabel.allPowerfulList.remove(skillShowJLabelAllPowerful);
        }

        skillShowJLabelAllPowerful = new AllPowerful<MyGraphics, com.cl.been.Container>() {
            @Override
            public com.cl.been.Container hook(MyGraphics myGraphics) {
                Graphics2D g2d = (Graphics2D) myGraphics.getGraphics();
                // 设置抗锯齿渲染
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                // 技能提示
                Graphics2D originalG2d = (Graphics2D) g2d.create();
                originalG2d.setColor(Color.BLACK);
                originalG2d.rotate(angle.doubleValue(), myPoint.getX(), myPoint.getY());
                for (Point point : pointList) {
                    originalG2d.setColor(Color.BLUE); // 设置填充颜色
                    originalG2d.fillRect(point.getX(), point.getY(), zhi, zhi);
                }
                originalG2d.dispose();
                return null;
            }
        };
        skillShowJLabel.allPowerfulList.add(skillShowJLabelAllPowerful);
        skillShowJLabel.repaint();
    }

    private Map<String, ImgPanel> imgBeenMap = new HashMap<>();

    //图像渲染
    public void showImg(List<ImgBeen> imgBeenList) {
        for (ImgBeen imgBeen : imgBeenList) {
            ImgPanel imgPanel = imgBeenMap.get(imgBeen.getId());
            if (imgBeen.getRemoveFlag()) {
                if (imgPanel == null) {
                    ImgPanel imgBeenPanel = util.setImgPanel(imgBeen.getImgUrl(), imgBeen.getAbsoluteX().intValue(), imgBeen.getAbsoluteY().intValue(), imgBeen.getWidth(), imgBeen.getLength());
                    imgBeenMap.put(imgBeen.getId(), imgBeenPanel);
                    roomMapJlabel.add(imgBeenPanel, 0);
                } else {
                    imgPanel.setLocation(imgBeen.getAbsoluteX().intValue(), imgBeen.getAbsoluteY().intValue());
                }
            }
        }
        drawingBoard.repaint();
    }

    public void removeImg(List<ImgBeen> imgBeenList) {
        for (ImgBeen imgBeen : imgBeenList) {
            ImgPanel imgPanel = imgBeenMap.get(imgBeen.getId());
            if (imgPanel != null && !imgBeen.getRemoveFlag()) {
                imgBeenMap.remove(imgBeen.getId());
                roomMapJlabel.remove(imgPanel);
            }
        }
        drawingBoard.repaint();
    }

    public void skillCd(List<SkillCd> skillCdList) {
        for (SkillCd skillCd : skillCdList) {
            ImgPanel imgPanel = imgPanelHashMap.get(skillCd.getId());
            imgPanel.setCdTime(skillCd.getStartTime(), skillCd.getEndTime());
        }
    }

    //角色属性变更
    public void alterationInUser(List<MapInUser> alterationInUserList) {
        for (MapInUser mapInUser : alterationInUserList) {
            //判断是不是自身
            if (mapInUser.getUserDTO().getId().equals(user.getId())) {
                applyUser(mapInUser);
            }
            if (mapInUser.getGameRoleUserDTO().getFlag() == 1){
                UserLocation userLocation = userLocationMap.get(mapInUser.getUserDTO().getId());
                if (userLocation != null) {
                    roomMapJlabel.remove(userLocation.getjLabelLocation());
                    roomMapJlabel.remove(userLocation.getjLabelName());
                    userLocationMap.remove(mapInUser.getUserDTO().getId());
                    if (mapInUser.getUserDTO().getId().equals(user.getId())){
                        userLocation = null;
                    }
                }
            }
        }
    }

    //玩家死亡
    public void gameOver(List<MapInUser> alterationInUserList) {
        for (MapInUser mapInUser : alterationInUserList) {
            if (mapInUser.getUserDTO().getId().equals(user.getId())) {
                //渲染自身死亡


                //黑屏告知读秒是不是主动退出
            }else {
                //渲染别的玩家死亡尸体
            }
        }
    }

    //技能 id : 技能图画对象
    Map<Integer, ImgPanel> imgPanelHashMap = new HashMap<>();

    //技能 id : 技能图画对象
    private List<Integer> skillKeyList = Arrays.asList(81, 87, 69, 82);
    Map<Integer, AllPowerful<FloatListener, Object>> skillKeyMap = new HashMap<>();

    //技能渲染
    public void showSkill(List<SkillDTO> skillDTOList) {
        for (int i = 0; i < skillDTOList.size(); i++) {
            SkillDTO skillDTO = skillDTOList.get(i);
            ImgPanel imgPanel = util.setImgPanel(skillDTO.getImgUrl(), i * (80 + 10) + 10, 10, 80, 80, skillSJLabel);
            imgPanel.setCdFlag(true);
            imgPanelHashMap.put(skillDTO.getId(), imgPanel);

            SkillFloatShow skillFloatShow = new SkillFloatShow();
            FloatListener<SkillFloatShow> floatListener = new FloatListener(imgPanel, skillFloatShow);
            floatListener.setMouseMovedAllPowerful(new AllPowerful<FloatListener, Object>() {
                @Override
                public Object hook(FloatListener floatL) {
                    if (skillFloatShow.getjLabel() == null) {
                        int x = skillSJLabel.getX() + imgPanel.getX();
                        int y = skillSJLabel.getY() + imgPanel.getY() - 200;
                        JLabel jLabel = util.setJLabel("", x, y, 500, 200, null, true, Color.WHITE, 0);
                        util.setJLabel("技能名称 : " + skillDTO.getName(), 0, 0, 500, 30, jLabel, true);
                        util.setJLabel("描述 : " + skillDTO.getDescribe(), 0, 30, 500, 40, jLabel, true);
                        util.setJLabel("效果 : " + skillDTO.getPracticalDescribe(), 0, 70, 500, 110, jLabel, true);
                        util.setJLabel("cd : " + skillDTO.getRefrigeration(), 0, 180, 500, 20, jLabel, true);
                        liveJ.addAndLayer(jLabel, 2);
                        skillFloatShow.setjLabel(jLabel);
                        drawingBoard.repaint();
                    }
                    return null;
                }
            });

            floatListener.setMouseExitedAllPowerful(new AllPowerful<FloatListener, Object>() {
                @Override
                public Object hook(FloatListener floatList) {
                    if (skillFloatShow.getjLabel() != null) {
                        liveJ.remove(skillFloatShow.getjLabel());
                        skillFloatShow.setjLabel(null);
                        drawingBoard.repaint();
                    }
                    return null;
                }
            });


            AllPowerful allPowerful = new AllPowerful<FloatListener, Object>() {
                @Override
                public Object hook(FloatListener floatList) {
                    mySkillDTO = skillDTO;
                    //释放预效
                    //查看技能是否在cd
                    //技能类型检测
                    String pointToType = skillDTO.getPointToType();
                    List<Integer> pointToTypeList = Arrays.stream(pointToType.split(",")).map(type -> Integer.parseInt(type)).collect(Collectors.toList());
                    //判定顺序 弹道=模型>自身>自身=友方=敌方  目标类型0自身 1友方 2敌方3弹道4模型
                    if (pointToTypeList.contains(3) || pointToTypeList.contains(4)) {
                        //渲染鼠标为弹道蓝色
                        drawingBoard.setCursor(MouseStyle.customCursor1);
                        //弹道渲染
                        if (pointToTypeList.contains(3)) {
                            myPointToType = 3;
                            if (floatList.mouseEvent == null) {
                                //根据鼠标点与玩家坐标计算偏移角度
                                int x = skillSJLabel.getX() + imgPanel.getX() + floatList.mouseEvent.getX() - roomMapJlabel.getX();
                                int y = skillSJLabel.getY() + imgPanel.getY() + floatList.mouseEvent.getY() - roomMapJlabel.getY();
                                Point skillPoint = new Point(x, y);
                                skillMyPointToType3(skillPoint);
                            }
                        } else {
                            //模型渲染

                        }
                    } else if (pointToTypeList.size() == 1 && pointToTypeList.contains(0)) {//纯自身
                        //通知服务器立即释放该技能
                        Link.link.write(new Msg("使用技能", new SkillUse(mySkillDTO.getId())));
                    } else if (pointToTypeList.size() == 1 && pointToTypeList.contains(2)) {//纯自身
                        //渲染鼠标为弹道为红色
                        drawingBoard.setCursor(MouseStyle.customCursor2);
                    } else {
                        //鼠标渲染交由监听器动态完成
                    }
                    drawingBoard.repaint();
                    return null;
                }
            };

            //点击技能
            floatListener.setMouseClickedAllPowerful(allPowerful);
            //键值绑定qwer
            skillKeyMap.put(skillKeyList.get(i), allPowerful);
        }
    }

}
