package com.cl.awt.page.play;

import com.alibaba.fastjson.JSONObject;
import com.cl.awt.Listener;
import com.cl.awt.core.geometry.Point;
import com.cl.awt.js.floatL.FloatListener;
import com.cl.been.Msg;
import com.cl.link.Link;
import com.cl.util.AllPowerful;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

public class RoomPageListener extends Listener {

    private PlaYGame plaYGame;

    public RoomPageListener(PlaYGame plaYGame) {
        this.plaYGame = plaYGame;
    }

    private Boolean keyFlag = false;

    private Boolean mouseFlag = false;

    private Map<String, Boolean> directionMap = new HashMap() {{
        put("上", false);
        put("下", false);
        put("左", false);
        put("右", false);
    }};

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 32) {
            keyFlag = true;
        }
        switch (e.getKeyCode()) {
            case 38://上
                directionMap.put("上", true);
                break;
            case 39://右
                directionMap.put("右", true);
                break;
            case 40://下
                directionMap.put("下", true);
                break;
            case 37://左
                directionMap.put("左", true);
                break;
        }

        if (plaYGame.userLocation != null) {
            switch (JSONObject.toJSONString(directionMap)) {
                case "{\"右\":false,\"左\":false,\"上\":false,\"下\":false}":
                    break;
                case "{\"右\":true,\"左\":true,\"上\":true,\"下\":true}":
                    break;
                case "{\"右\":true,\"左\":false,\"上\":false,\"下\":false}":
                    Link.link.write(new Msg("操作移动", "右"));
                    break;
                case "{\"右\":false,\"左\":true,\"上\":false,\"下\":false}":
                    Link.link.write(new Msg("操作移动", "左"));
                    break;
                case "{\"右\":false,\"左\":false,\"上\":true,\"下\":false}":
                    Link.link.write(new Msg("操作移动", "上"));
                    break;
                case "{\"右\":false,\"左\":false,\"上\":false,\"下\":true}":
                    Link.link.write(new Msg("操作移动", "下"));
                    break;

                case "{\"右\":true,\"左\":false,\"上\":true,\"下\":false}":
                    Link.link.write(new Msg("操作移动", "右上"));
                    break;
                case "{\"右\":true,\"左\":false,\"上\":false,\"下\":true}":
                    Link.link.write(new Msg("操作移动", "右下"));
                    break;

                case "{\"右\":false,\"左\":true,\"上\":true,\"下\":false}":
                    Link.link.write(new Msg("操作移动", "左上"));
                    break;
                case "{\"右\":false,\"左\":true,\"上\":false,\"下\":true}":
                    Link.link.write(new Msg("操作移动", "左下"));
                    break;

                case "{\"右\":false,\"左\":true,\"上\":true,\"下\":true}":
                    Link.link.write(new Msg("操作移动", "左"));
                    break;
                case "{\"右\":true,\"左\":false,\"上\":true,\"下\":true}":
                    Link.link.write(new Msg("操作移动", "右"));
                    break;
                case "{\"右\":true,\"左\":true,\"上\":false,\"下\":true}":
                    Link.link.write(new Msg("操作移动", "下"));
                    break;
                case "{\"右\":true,\"左\":true,\"上\":true,\"下\":false}":
                    Link.link.write(new Msg("操作移动", "上"));
                    break;
            }

            AllPowerful<FloatListener, Object> allPowerful = plaYGame.skillKeyMap.get(e.getKeyCode());
            if (allPowerful != null) {
                allPowerful.hook(new FloatListener());
            }
        }
        if (e.getKeyCode() == 82) {
            plaYGame.broadcast("xxx");
        }


    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == 32) {
            keyFlag = false;
            mouseFlag = false;
        }
        switch (e.getKeyCode()) {
            case 38://上
                directionMap.put("上", false);
                break;
            case 39://右
                directionMap.put("右", false);
                break;
            case 40://下
                directionMap.put("下", false);
                break;
            case 37://左
                directionMap.put("左", false);
                break;
        }
    }

    private Point point;

    @Override
    public void mousePressed(MouseEvent e) {
        if (keyFlag) {
            mouseFlag = true;
            point = new Point(e.getX(), e.getY());
        }
    }


    @Override
    public void mouseReleased(MouseEvent e) {
        if (keyFlag && mouseFlag) {
            int x = e.getX() - point.getX();
            int y = e.getY() - point.getY();
            //设置拖动样式
            plaYGame.roomMapJlabelMove(x, y);
            mouseFlag = false;
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (plaYGame.myPointToType != null && plaYGame.myPointToType == 3) {
            plaYGame.skillMyPointToType3(new Point(e.getX() - plaYGame.roomMapJlabel.getX(), e.getY() - plaYGame.roomMapJlabel.getY()));
        }
    }

    //点击事件
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON3) {//右键点击
            if (plaYGame.myPointToType == 3) {
                plaYGame.skillMyPointToTypeRemove();
            }
        }
    }

}
