package com.cl.awt.page.mall;

import com.cl.api.Page;
import com.cl.api.Response;
import com.cl.api.mall.GoodsApi;
import com.cl.api.mall.GoodsCateApi;
import com.cl.api.mall.dto.GoodsCateDTO;
import com.cl.api.mall.dto.GoodsDTO;
import com.cl.api.mall.dto.GoodsSkuDTO;
import com.cl.api.mall.request.FindGoodsCateListRequest;
import com.cl.api.mall.request.FindGoodsPageRequest;
import com.cl.api.mall.response.FindGoodsCateListResponse;
import com.cl.api.mall.response.FindGoodsPageResponse;
import com.cl.awt.core.MyPage;
import com.cl.awt.js.Hint;
import com.cl.awt.js.ImgPanel;
import com.cl.awt.js.MyButton;
import com.cl.been.Container;
import com.cl.handler.page.PageUrl;
import com.cl.util.Newjs;

import javax.annotation.Resource;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@PageUrl(url = "mall", pageName = "商城", rollbackFlag = false)
public class Mall extends MyPage {

    @Resource
    private GoodsApi GoodsApi;

    @Resource
    private GoodsCateApi goodsCateApi;

    //商品品类map数据集
    private Map<Integer, String> goodsCateMap;

    //商品渲染画框
    private JLabel goodsS;

    //分页信息渲染画框
    private JLabel pageJLabel;

    //分页信息对象
    Page page = new Page();

    // 页面
    public void load() throws MalformedURLException {
        //分页信息初始化
        {
            page.setCurrentPage(1);
            page.setPageSize(12);
        }
        //返回页跳转
        MyButton rollback = new MyButton("返回");
        rollback.setBounds(0, 0, 200, 60);
        client.drawingBoard.add(rollback);
        super.rollback(rollback);
        LineBorder lb = new LineBorder(Color.BLACK, 1, false);
        JLabel taskList = new JLabel();
        taskList.setOpaque(true);
        taskList.setBounds(200, 0, 1000, 60);
        taskList.setBorder(lb);
        taskList.setBackground(Color.WHITE);
        drawingBoard.add(taskList, 0);
        //购物车页跳转
        MyButton shoppingTrolleyPage = new MyButton("购物车");
        shoppingTrolleyPage.setBounds(1200, 0, 200, 60);
        client.drawingBoard.add(shoppingTrolleyPage);
        //查询商品品类
        FindGoodsCateListRequest findGoodsCateListRequest = new FindGoodsCateListRequest();
        findGoodsCateListRequest.setStatus(0);
        Response<FindGoodsCateListResponse> findGoodsCateListResponseResponse = goodsCateApi.clientFindGoodsCateList(findGoodsCateListRequest);
        new Hint().run(findGoodsCateListResponseResponse.getState(), findGoodsCateListResponseResponse.getMsg());
        List<GoodsCateDTO> goodsCateDTOList = findGoodsCateListResponseResponse.getData().getGoodsCateDTOList();
        goodsCateMap = goodsCateDTOList.stream().collect(Collectors.toMap(GoodsCateDTO::getCateId, GoodsCateDTO::getCateName));
        //商品列表渲染
        this.goodsS = new JLabel();
        this.goodsS.setOpaque(true);
        this.goodsS.setBounds(20, 70, 1355, 650);
        this.goodsS.setBorder(lb);
        this.goodsS.setBackground(Color.WHITE);
        this.goodsS.setVerticalAlignment(JLabel.CENTER);
        this.goodsS.setHorizontalAlignment(JLabel.CENTER);
        drawingBoard.add(this.goodsS, 0);
        //分页框渲染
        this.pageJLabel = new JLabel();
        this.pageJLabel.setOpaque(true);
        this.pageJLabel.setBounds(900, 725, 400, 40);
        this.pageJLabel.setBorder(lb);
        this.pageJLabel.setBackground(Color.WHITE);
        this.pageJLabel.setVerticalAlignment(JLabel.CENTER);
        this.pageJLabel.setHorizontalAlignment(JLabel.CENTER);
        drawingBoard.add(this.pageJLabel, 0);
        this.findGoods();
    }

    //查询商品列表页
    private void findGoods() {
        //清空数据
        this.goodsS.removeAll();
        //查询商品列表数据
        LineBorder lb = new LineBorder(Color.BLACK, 1, false);
        FindGoodsPageRequest findGoodsPageRequest = new FindGoodsPageRequest();
        findGoodsPageRequest.setPageSize(page.getPageSize());
        findGoodsPageRequest.setCurrentPage(page.getCurrentPage());

        Response<FindGoodsPageResponse> response = GoodsApi.mallFindGoodsPage(findGoodsPageRequest);
        new Hint().run(response.getState(), response.getMsg());
        FindGoodsPageResponse findGoodsPageResponse = response.getData();
        List<GoodsDTO> goodsDTOList = findGoodsPageResponse.getGoodsDTOList();
        this.page = findGoodsPageResponse.getPage();
        Newjs newjs = new Newjs();
        for (int i = 0; goodsDTOList.size() > i; i++) {
            GoodsDTO goodsDTO = goodsDTOList.get(i);
            int settingSize = 4;//每行显示数量
            int interval = 13;//商品显示间隔
            int goodsWidth = 322;//展示栏宽度
            int goodsHeight = 200;//展示栏高度
            int goodsXSize = i % settingSize;
            int goodsYSize = 0;
            if (i != 0) {
                goodsYSize = newjs.divide(i, settingSize).intValue();
            }
            int x = interval + interval * goodsXSize + goodsXSize * goodsWidth;
            int y = interval + interval * goodsYSize + goodsYSize * goodsHeight;

            JLabel goodsShow = new JLabel();
            goodsShow.setOpaque(true);
            goodsShow.setBounds(x, y, goodsWidth, goodsHeight);
            goodsShow.setBorder(lb);
            goodsShow.setBackground(Color.WHITE);
            goodsShow.setVerticalAlignment(JLabel.CENTER);
            goodsShow.setHorizontalAlignment(JLabel.CENTER);
            this.goodsS.add(goodsShow, 0);

            //商品图片
            ImgPanel goodsImg = new ImgPanel();
            goodsImg.setImageHttpURL(goodsDTO.getGoodsImg());
            goodsImg.setBounds(10, 10, 100, 100);
            goodsShow.add(goodsImg, 0);

            //商品名称
            JLabel goodsName = new JLabel();
            goodsName.setOpaque(true);
            goodsName.setBounds(130, 10, 140, 30);
            goodsName.setBackground(Color.WHITE);
            goodsName.setText(goodsDTO.getGoodsName());
            goodsName.setVerticalAlignment(JLabel.CENTER);
            goodsName.setHorizontalAlignment(JLabel.CENTER);
            goodsShow.add(goodsName, 0);

            //展示价格
            JLabel goodsPrice = new JLabel();
            goodsPrice.setOpaque(true);
            goodsPrice.setBounds(130, 40, 140, 30);
            goodsPrice.setBackground(Color.WHITE);
            //查询最小的积分到最大积分
            Integer smallPrice = 0;
            Integer bigPrice = 0;
            for (GoodsSkuDTO goodsSkuDTO : goodsDTO.getGoodsSkuDTOList()) {
                //最小价值的商品价格
                if (smallPrice == 0) {
                    smallPrice = goodsSkuDTO.getPrice();
                } else {
                    if (smallPrice > goodsSkuDTO.getPrice()) {
                        smallPrice = goodsSkuDTO.getPrice();
                    }
                }
                //最大价值的商品价格
                if (bigPrice < goodsSkuDTO.getPrice()) {
                    bigPrice = goodsSkuDTO.getPrice();
                }
            }

            goodsPrice.setText(smallPrice + "-" + bigPrice + " 积分");
            goodsPrice.setVerticalAlignment(JLabel.CENTER);
            goodsPrice.setHorizontalAlignment(JLabel.CENTER);
            goodsShow.add(goodsPrice, 0);

            //商品品类
            JLabel goodsCate = new JLabel();
            goodsCate.setOpaque(true);
            goodsCate.setBounds(130, 70, 140, 30);
            goodsCate.setBackground(Color.WHITE);
            goodsCate.setText(" 品类3 :" + goodsCateMap.get(goodsDTO.getCateId()));
            goodsCate.setVerticalAlignment(JLabel.CENTER);
            goodsCate.setHorizontalAlignment(JLabel.CENTER);
            goodsShow.add(goodsCate, 0);

            //商品简介
            JLabel goodsSynopsis = new JLabel();
            goodsSynopsis.setOpaque(true);
            goodsSynopsis.setBounds(10, 110, 240, 50);
            goodsSynopsis.setBackground(Color.WHITE);
            goodsSynopsis.setText("简介 : " + goodsDTO.getSynopsis());
            goodsShow.add(goodsSynopsis, 0);

            if (goodsDTO.getGoodsSkuDTOList().size()>0){
                //进入详情页
                MyButton knapsackPage = new MyButton("进入商品详情页");

                knapsackPage.setBounds(130, 165, 180, 30);
                goodsShow.add(knapsackPage);
                this.goGoodsSkuS(knapsackPage,goodsDTO);
            }

        }
        this.showPage();
    }

    public void showPage() {

        pageJLabel.removeAll();

        LineBorder lb = new LineBorder(Color.BLACK, 1, false);

        //根据总页数 和当前页渲染选框
        int kuozhan = 2;
        List<Integer> kuozhanList = new ArrayList<>();


        //初始扩展
        int small = 1;

        if (this.page.getPageCount() - this.page.getCurrentPage() - kuozhan <0 ){
            int zhi = kuozhan - (this.page.getPageCount() - this.page.getCurrentPage());
            int kuozhanfk = kuozhan+zhi;
            if (this.page.getCurrentPage() - kuozhanfk > 0) {
                small = this.page.getCurrentPage() - kuozhanfk;

            }
            while (small < this.page.getCurrentPage()) {
                kuozhanList.add(small);
                small++;
            }
        }else {
            if (this.page.getCurrentPage() - kuozhan > 0) {
                small = this.page.getCurrentPage() - kuozhan;

            }
            while (small < this.page.getCurrentPage()) {
                kuozhanList.add(small);
                small++;
            }
        }


        //中序扩展
        kuozhanList.add(this.page.getCurrentPage());

        //上序扩展
        int size = kuozhan * 2 + 1;
        kuozhan = size - kuozhanList.size();

        int big = this.page.getCurrentPage() + 1;
        if (this.page.getCurrentPage() + kuozhan > this.page.getPageCount()) {
            while (big <= this.page.getPageCount()) {
                kuozhanList.add(big);
                big++;
            }
        } else {
            while (big <= this.page.getCurrentPage() + kuozhan) {
                kuozhanList.add(big);
                big++;
            }
        }
        System.out.println("kuozhanList.toString() : " + kuozhanList.toString());
        for (int i = 0; i < kuozhanList.size(); i++) {
            MyButton myButton = new MyButton(kuozhanList.get(i).toString());
            if (kuozhanList.get(i) == this.page.getCurrentPage()) {
                myButton.setBounds(10 + i * 40, 2, 36, 36);
            } else {
                myButton.setBounds(10 + i * 40, 5, 30, 30);
            }
            pageJLabel.add(myButton);
            this.pageTurning(myButton);
        }

    }

    //翻页功能
    private void pageTurning(JButton saveButton) {
        // 为按钮绑定监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                page.setCurrentPage(Integer.parseInt(saveButton.getText()));
                findGoods();
            }
        });
    }

    //跳转商品详情页
    private void goGoodsSkuS(JButton saveButton,GoodsDTO goodsDTO) {
        // 为按钮绑定监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)  {
                util.skip(goodsSkuS.class, new Container().set("goodsDTO",goodsDTO).set("goodsCateMap",goodsCateMap));
            }
        });
    }
}
