package com.cl.awt.page.mall;

import com.cl.api.Response;
import com.cl.api.gameRole.GameRoleApi;
import com.cl.api.gameRole.dto.GameRoleDTO;
import com.cl.api.gameRole.request.FindGameRolePageRequest;
import com.cl.api.gameRole.response.FindGameRolePageResponse;
import com.cl.api.mall.dto.GoodsDTO;
import com.cl.api.mall.dto.GoodsSkuDTO;
import com.cl.api.mall.vo.OrderPartVO;
import com.cl.awt.core.MyPage;
import com.cl.awt.js.Hint;
import com.cl.awt.js.MyButton;
import com.cl.been.Container;
import com.cl.handler.page.PageUrl;

import javax.annotation.Resource;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@PageUrl(url = "goodsSkuS", pageName = "商品详情页", rollbackFlag = false)
public class goodsSkuS extends MyPage {

    @Resource
    private GameRoleApi gameRoleApi;

    private Map<Integer, String> goodsCateMap;

    private GoodsSkuDTO thisGoodsSku;

    // 页面
    public void load(GoodsDTO goodsDTO, Map<Integer, String> goodsCateMap) throws MalformedURLException {

        //返回页跳转
        MyButton rollback = new MyButton("返回");
        rollback.setBounds(1195, 1, 200, 60);
        drawingBoard.add(rollback);
        super.rollback(rollback);

        this.goodsCateMap = goodsCateMap;
        GoodsSkuDTO goodsSkuDTO = goodsDTO.getGoodsSkuDTOList().get(0);
        {
            //渲染sku信息
            JLabel goodsJLabel = util.setJLabel("", 330, 5, 800, 160, null, true);
            drawingBoard.add(goodsJLabel);
            util.setJLabel("商品名称 : " + goodsSkuDTO.getGoodsName(), 10, 10, 180, 30, goodsJLabel, true);
            util.setJLabel("商品简介 : " + goodsSkuDTO.getSynopsis(), 10, 50, 180, 60, goodsJLabel, true);
            util.setJLabel("商品价格 : " + goodsSkuDTO.getPrice(), 10, 120, 180, 30, goodsJLabel, true);
        }
        if (goodsSkuDTO.getGoodsType() == 0) {
            //渲染角色
            showGameRole(goodsSkuDTO);
        } else {
            //渲染道具
        }
        //suku 挑选栏
        {
            JLabel selectSkuJLabel = util.setJLabel("", 20, 360, 180, 360, null, true);
            drawingBoard.add(selectSkuJLabel);
            int i = 10;
            List<MyButton> knapsackList = new ArrayList<>();
            for (GoodsSkuDTO goodsSku : goodsDTO.getGoodsSkuDTOList()) {

                MyButton knapsack = new MyButton(goodsSku.getGoodsName());
                if (i == 10) {
                    knapsack.setBounds(20, i - 2, 140, 24);
                    knapsackThis = knapsack;
                } else {
                    knapsack.setBounds(30, i, 120, 20);
                }
                selectSkuJLabel.add(knapsack);
                knapsackList.add(knapsack);
                this.selectSku(knapsack, goodsSku, knapsackList);
                i += 30;
            }
        }

        //按钮信息渲染框
        {
            JLabel purchaseJLabel = util.setJLabel("", 860, 640, 300, 120, null, true);
            drawingBoard.add(purchaseJLabel);
            //购买数量
            util.setJLabel("数量", 60, 5, 50, 30, purchaseJLabel, false);
            JTextField jTextField = util.addJTextField(10, 115, 5, 30, 30, "");
            purchaseJLabel.add(jTextField);
            //角色是否已持有?
            {
                MyButton knapsackPage = new MyButton("立即购买");
                knapsackPage.setBounds(60, 40, 180, 30);
                purchaseJLabel.add(knapsackPage);
                this.buy(knapsackPage, jTextField);
            }
            {
                MyButton knapsackPage = new MyButton("加入购物车");
                knapsackPage.setBounds(60, 75, 180, 30);
                purchaseJLabel.add(knapsackPage);
            }
        }
    }

    //立即购买
    private void buy(JButton saveButton, JTextField jTextField) {
        // 为按钮绑定监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //获取购买数量
                int sum = Integer.parseInt(jTextField.getText());
                if (sum > 0) {
                    List<OrderPartVO> orderPartVOList = new ArrayList<>();
                    OrderPartVO orderPartVO = new OrderPartVO();
                    orderPartVO.setSum(sum);
                    orderPartVO.setGoodsSkuDTO(thisGoodsSku);
                    orderPartVOList.add(orderPartVO);
                    util.skip(Order.class, new Container().set("orderPartVOList", orderPartVOList).set("snapshoot", false));
                }
            }
        });
    }

    private MyButton knapsackThis;
    private JLabel imgJLabel;
    private JLabel gameRoleJLabel;

    //渲染角色
    private void showGameRole(GoodsSkuDTO goodsSkuDTO) {
        thisGoodsSku = goodsSkuDTO;
        if (imgJLabel != null) {
            drawingBoard.remove(imgJLabel);
        }
        if (gameRoleJLabel != null) {
            imgJLabel.remove(gameRoleJLabel);
        }
        FindGameRolePageRequest findGameRolePageRequest = new FindGameRolePageRequest();
        findGameRolePageRequest.setGameRoleId(Integer.parseInt(goodsSkuDTO.getCorrelationId()));
        Response<FindGameRolePageResponse> findGameRolePageResponseResponse = gameRoleApi.findGameRolePage(findGameRolePageRequest);
        new Hint().run(findGameRolePageResponseResponse.getState(), findGameRolePageResponseResponse.getMsg());
        List<GameRoleDTO> gameRoleDTOList = findGameRolePageResponseResponse.getData().getGameRoleDTOList();
        if (gameRoleDTOList.size() > 0) {
            GameRoleDTO gameRoleDTO = gameRoleDTOList.get(0);
            //商品图片
            imgJLabel = util.setJLabel("", 1, 0, 310, 310, null, true);
            drawingBoard.add(imgJLabel);
            util.setImgPanel(gameRoleDTO.getRoleImg(), 5, 5, 300, 300, imgJLabel);
            //角色信息渲染框
            gameRoleJLabel = util.setJLabel("", 330, 170, 800, 460, null, true);
            drawingBoard.add(gameRoleJLabel);
            //渲染角色详细信息
            util.setJLabel("角色名称 : " + gameRoleDTO.getName(), 10, 10, 180, 30, gameRoleJLabel, true);
            util.setJLabel("描述 : " + gameRoleDTO.getDescribe(), 10, 50, 300, 80, gameRoleJLabel, true);
            util.setJLabel("攻击力 : " + gameRoleDTO.getAtk(), 10, 140, 180, 30, gameRoleJLabel, true);
            util.setJLabel("攻击距离 : " + gameRoleDTO.getSd(), 10, 180, 180, 30, gameRoleJLabel, true);
            util.setJLabel("防御力 : " + gameRoleDTO.getDef(), 10, 220, 180, 30, gameRoleJLabel, true);
            util.setJLabel("生命值 : " + gameRoleDTO.getHp(), 10, 260, 180, 30, gameRoleJLabel, true);
            util.setJLabel("法力值 : " + gameRoleDTO.getSp(), 10, 300, 180, 30, gameRoleJLabel, true);
            util.setJLabel("移速 : " + gameRoleDTO.getDd(), 10, 340, 180, 30, gameRoleJLabel, true);
        }
    }

    //sku选择器
    private void selectSku(MyButton saveButton, GoodsSkuDTO goodsSkuDTO, List<MyButton> knapsackList) {
        // 为按钮绑定监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (knapsackThis == saveButton) {
                    return;
                }
                if (goodsSkuDTO.getGoodsType() == 0) {
                    //渲染角色
                    showGameRole(goodsSkuDTO);
                } else {
                    //渲染道具0
                }
                //渲染按钮
                knapsackList.forEach(myButton -> {
                    if (saveButton == myButton) {
                        myButton.setBounds(myButton.getX() - 10, myButton.getY() - 2, 140, 24);
                        knapsackThis.setBounds(knapsackThis.getX() + 10, knapsackThis.getY() + 2, 120, 20);
                        knapsackThis = myButton;
                    }
                });
            }
        });
    }

}
