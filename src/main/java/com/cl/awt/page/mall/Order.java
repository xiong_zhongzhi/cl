package com.cl.awt.page.mall;

import com.cl.api.Response;
import com.cl.api.mall.OrderApi;
import com.cl.api.mall.dto.GoodsSkuDTO;
import com.cl.api.mall.request.CreateOrderRequest;
import com.cl.api.mall.request.PayOrderRequest;
import com.cl.api.mall.response.CreateOrderResponse;
import com.cl.api.mall.vo.OrderPartVO;
import com.cl.awt.core.MyPage;
import com.cl.awt.js.Hint;
import com.cl.awt.js.MyButton;
import com.cl.handler.page.PageUrl;

import javax.annotation.Resource;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.util.List;


@PageUrl(url = "order", pageName = "订单页", rollbackFlag = false)
public class Order extends MyPage {

    @Resource
    private OrderApi orderApi;

    // 页面
    public void load(List<OrderPartVO> orderPartVOList, Boolean snapshoot) throws MalformedURLException {
        //返回页跳转
        MyButton rollback = new MyButton("返回");
        rollback.setBounds(0, 1, 200, 60);
        drawingBoard.add(rollback);
        super.rollback(rollback);
        util.setJLabel("", 200, 1, 1200, 60, null, true);

        if (snapshoot) {
            //购物车快照加载
        }
        //主订单信息
        JLabel orderJLabel = util.setJLabel("", 10, 70, 1375, 160, null, true);
        drawingBoard.add(orderJLabel);
        util.setJLabel("共 " + orderPartVOList.size() + " 种商品数量", 10, 10, 200, 30, orderJLabel, false);
        int sum = orderPartVOList.stream().map(orderPartVO -> {
            return orderPartVO.getGoodsSkuDTO().getPrice() * orderPartVO.getSum();
        }).mapToInt(i -> {
            return i;
        }).sum();
        util.setJLabel("合计 " + sum + " 积分", 220, 10, 200, 30, orderJLabel, false);
        //sku商品信息列表
        JLabel skusJLabel = util.setJLabel("", 10, 240, 1375, 400, null, true);
        drawingBoard.add(skusJLabel);
        for (int i = 0; i < orderPartVOList.size(); i++) {
            OrderPartVO orderPartVO = orderPartVOList.get(i);
            GoodsSkuDTO goodsSkuDTO = orderPartVO.getGoodsSkuDTO();
            JLabel skuJLabel = util.setJLabel("", 30, 5 + i * 5 + i * 80, 1315, 80, skusJLabel, true);

            util.setImgPanel(goodsSkuDTO.getGoodsImg(), 5, 5, 70, 70, skuJLabel);
            util.setJLabel("商品名称 : " + goodsSkuDTO.getGoodsName(), 80, 10, 200, 15, skuJLabel, false);
            util.setJLabel("spu : " + goodsSkuDTO.getSpu(), 80, 35, 200, 15, skuJLabel, false);
            util.setJLabel("sku : " + goodsSkuDTO.getSku(), 80, 55, 200, 15, skuJLabel, false);
            util.setJLabel("商品单价 : " + goodsSkuDTO.getPrice(), 290, 30, 200, 15, skuJLabel, false);
            util.setJLabel("购买数量 : " + orderPartVO.getSum(), 500, 30, 200, 15, skuJLabel, false);
            util.setJLabel("总价 : " + goodsSkuDTO.getPrice() * orderPartVO.getSum(), 710, 30, 200, 15, skuJLabel, false);
        }

        //操作按钮
        JLabel operationJLabel = util.setJLabel("", 400, 650, 600, 100, null, true);
        drawingBoard.add(operationJLabel);
        MyButton payButton = new MyButton("付款");
        payButton.setBounds(100, 10, 200, 60);
        operationJLabel.add(payButton);
        pay(payButton, orderPartVOList);

    }

    private void pay(MyButton saveButton, List<OrderPartVO> orderPartVOList) {
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //创建订单
                CreateOrderRequest createOrderRequest = new CreateOrderRequest();
                createOrderRequest.setOrderPartVOList(orderPartVOList);
                createOrderRequest.setOrderStatus(1);
                Response<CreateOrderResponse> createOrderResponseResponse = orderApi.createOrder(createOrderRequest);
                new Hint().run(createOrderResponseResponse.getState(), createOrderResponseResponse.getMsg());
                if (createOrderResponseResponse.getState() == 0) {
                    //订单支付
                    PayOrderRequest payOrderRequest = new PayOrderRequest();
                    payOrderRequest.setOrderId(createOrderResponseResponse.getData().getOrderId());
                    Response response = orderApi.payOrder(payOrderRequest);
                    new Hint().run(response.getState(), response.getMsg());
                    if (response.getState() == 0) {
                        //订单支付成功后回退页面
                        rollbackExecute();
                    }else {
                        //订单支付失败
                    }
                }

            }
        });
    }

}
