package com.cl.awt.page.establishRoom;

import com.cl.awt.js.ImgPanel;
import com.cl.awt.js.hookJlable.RoomMapJlabel;

import javax.swing.*;

public class UserAttributes {

    public JLabel userGameRole;

    public ImgPanel userGameRoleImgPanel;

    public JLabel userAttributes;

    public RoomMapJlabel hp;

    public RoomMapJlabel sp;

}
