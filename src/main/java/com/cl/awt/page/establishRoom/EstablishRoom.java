package com.cl.awt.page.establishRoom;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cl.api.Response;
import com.cl.api.room.DivApi;
import com.cl.api.room.RoomApi;
import com.cl.api.room.RoomMapApi;
import com.cl.api.room.dto.DivDTO;
import com.cl.api.room.dto.RoomDTO;
import com.cl.api.room.dto.RoomMapDTO;
import com.cl.api.room.pojo.Coordinates;
import com.cl.api.room.pojo.Room;
import com.cl.api.room.request.CreateRoomRequest;
import com.cl.api.room.request.FindRoomListRequest;
import com.cl.api.room.request.FindRoomMapListRequest;
import com.cl.api.room.request.UpDateRoomMapRequest;
import com.cl.api.room.response.FindDivListResponse;
import com.cl.api.room.response.FindRoomListResponse;
import com.cl.api.room.response.FindRoomMapListResponse;
import com.cl.api.room.vo.RoomMapVO;
import com.cl.awt.core.MyActionListener;
import com.cl.awt.core.MyPage;
import com.cl.awt.core.blackHole.BlackHole;
import com.cl.awt.core.blackHole.BlackHoleHandle;
import com.cl.awt.js.Hint;
import com.cl.awt.js.ImgPanel;
import com.cl.awt.js.MyButton;
import com.cl.awt.js.form.*;
import com.cl.awt.js.hookJlable.MyGraphics;
import com.cl.awt.js.hookJlable.RoomMapJlabel;
import com.cl.awt.js.math.LineMath;
import com.cl.awt.js.tree.quadtree.BoundingBox;
import com.cl.awt.js.tree.quadtree.Quadtree;
import com.cl.been.Container;
import com.cl.handler.page.PageUrl;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.cl.awt.core.geometry.Line;
import com.cl.awt.core.geometry.Point;
import com.cl.util.AllPowerful;
import org.springframework.beans.BeanUtils;

@PageUrl(url = "makeMap", pageName = "制作地图", rollbackFlag = false)
public class EstablishRoom extends MyPage {

    private DivApi divApi;

    private RoomApi roomApi;

    private RoomMapApi roomMapApi;

    /**
     * 地块四叉树
     */
    private Quadtree quadtree;

    /**
     * 地图四叉树
     */
    private Quadtree mapQuadtree;

    /**
     * div集合
     */
    private Map<Integer, ImgPanel> imgPanelMap = new HashMap();


    private ImgPanel selectedDivImgPanel;

    private JLabel roomDTOJLabel;

    public void load() {
        MyButton rollback = new MyButton("返回");
        rollback.setColor(Color.white);
        rollback.setBounds(0, 0, 200, 60);
        client.drawingBoard.add(rollback);
        super.rollback(rollback);

        // 查询渲染地块元素块
        Response<FindDivListResponse> findDivListResponse = divApi.queryDiv();
        new Hint().run(findDivListResponse.getState(), findDivListResponse.getMsg());
        List<DivDTO> divDTOList = findDivListResponse.getData().getDivDTOList();
        AtomicInteger index = new AtomicInteger(0);

        JLabel divSJlabel = util.setJLabel("", 0, 60, 200, divDTOList.size() * 90 + 20,  true, Color.white);
        drawingBoard.add(divSJlabel);
        BoundingBox bounds = new BoundingBox(0, 60, 200, divDTOList.size() * 90 + 20); // 指定根节点的边界
        quadtree = new Quadtree(0, bounds);
        divDTOList.forEach(divDTO -> {
            ImgPanel divImgPanel = util.setImgPanel(divDTO.getDivImg(), 60, index.getAndIncrement() * 90 + 10, zhi, zhi, divSJlabel);
            divImgPanel.setKey(divDTO.getDivId());
            imgPanelMap.put(divDTO.getDivId(), divImgPanel);
            BoundingBox divBoundingBox = new BoundingBox(divImgPanel.getX() + divSJlabel.getX(), divImgPanel.getY() + divSJlabel.getY(), zhi, zhi);
            divBoundingBox.setRenderAllPowerful(new AllPowerful<BoundingBox, Object>() {
                @Override
                public Container hook(BoundingBox gameObject) {
                    selectedDivImgPanel = util.setImgPanel(divImgPanel.getUrl(), divImgPanel.getX() + divSJlabel.getX(), divImgPanel.getY() + divSJlabel.getY(), zhi, zhi, null);
                    drawingBoard.add(selectedDivImgPanel,0);
                    selectedDivImgPanel.setKey(divImgPanel.getKey());
                    return null;
                }
            });
            quadtree.insert(divBoundingBox);
        });

        MyButton roomListButton = util.setMyButton("房间列表", 200, 0, 200, 60, null, Color.white);
        queryMapDiv(roomListButton);
        drawingBoard.add(roomListButton);

        JLabel addRoomJLabel = bounceFrame();

        MyButton addRoomButton = util.setMyButton("新增房间", 400, 0, 200, 60, null, Color.white);
        drawingBoard.add(addRoomButton);
        addRoomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                drawingBoard.add(addRoomJLabel, 0);
            }
        });

        MyButton upRoomButton = util.setMyButton("保存房间", 600, 0, 200, 60, null, Color.white);
        drawingBoard.add(upRoomButton);
        upRoomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (roomDTO == null) {
                    new Hint().run(-1, "请选择房间");
                }
                if (roomMapVOList != null) {
                    System.out.println("JSONArray.toJSONString(roomMapVOList) : "+JSONArray.toJSONString(roomMapVOList));
                    UpDateRoomMapRequest upDateRoomMapRequest = new UpDateRoomMapRequest();
                    upDateRoomMapRequest.setRoomId(roomDTO.getRoomId());
                    upDateRoomMapRequest.setRoomMapVOList(roomMapVOList);
                    roomMapApi.upDateRoomMap(upDateRoomMapRequest);
                }
            }
        });

        roomDTOJLabel = util.setJLabel("房间 : 无", 800, 0, 600, 60,null, true, Color.white);
        drawingBoard.add(roomDTOJLabel);

        // 开启鼠标监听
        EstablishRoomListener establishRoomListener = new EstablishRoomListener(this);
        drawingBoard.addMouseListener(establishRoomListener);
        drawingBoard.addMouseMotionListener(establishRoomListener);
        drawingBoard.addKeyListener(establishRoomListener);

        //让其获得焦点，这样才能是键盘监听能够正常使用
        drawingBoard.requestFocusInWindow();
    }

    //地块移动
    public void selectedDivImgPanel(int x, int y) {
        if (selectedDivImgPanel != null) {
            //与黑洞关联
            if (blackHoleHandle != null) {
                int adsorbx = x - roomMapJlabel.getX();
                int adsorby = y - roomMapJlabel.getY();
                BlackHole blackHole = blackHoleHandle.adsorb(adsorbx, adsorby);
                if (blackHole != null) {
                    selectedDivImgPanel.setLocation(blackHole.getX() + roomMapJlabel.getX(), blackHole.getY() + roomMapJlabel.getY());
                } else {
                    selectedDivImgPanel.setLocation(x, y);
                }
            } else {
                selectedDivImgPanel.setLocation(x, y);
            }
            drawingBoard.repaint();
        }
    }

    //div点击取消
    public void cancellationSelected() {
        if (selectedDivImgPanel != null) {
            drawingBoard.remove(selectedDivImgPanel);
            drawingBoard.repaint();
            selectedDivImgPanel = null;
        }
    }

    //div点击触发
    public void divSelected(BoundingBox boundingBox) {
        List<BoundingBox> collidingBlocks = new ArrayList<>();
        quadtree.retrieve(collidingBlocks, boundingBox);
        for (BoundingBox divBoundingBox : collidingBlocks) {
            if (selectedDivImgPanel != null) {
                drawingBoard.remove(selectedDivImgPanel);
            }
            divBoundingBox.render();
            drawingBoard.repaint();
        }
    }

    //mapDiv点击触发
    public void mapDivSelected(BoundingBox boundingBox) {
        if (roomDTO != null) {
            if (selectedDivImgPanel == null) {
                BoundingBox newBoundingBox = new BoundingBox(boundingBox.getX() - roomMapJlabel.getX(), boundingBox.getY() - roomMapJlabel.getY(), 1, 1);
                List<BoundingBox> collidingBlocks = new ArrayList<>();
                mapQuadtree.retrieve(collidingBlocks, newBoundingBox);
                for (BoundingBox divBoundingBox : collidingBlocks) {
                    divBoundingBox.render();
                    drawingBoard.repaint();
                }
            }
        }
    }

    //div点击放置
    public void divSelectedPlace(BoundingBox boundingBox) {

        if (selectedDivImgPanel != null) {
            if (blackHoleHandle != null) {
                int adsorbx = boundingBox.getX() - roomMapJlabel.getX();
                int adsorby = boundingBox.getY() - roomMapJlabel.getY();
                BlackHole<Coordinates> blackHole = blackHoleHandle.adsorb(adsorbx, adsorby);
                if (blackHole != null) {
                    //画面绘制
                    ImgPanel imgPanel = util.setImgPanel(selectedDivImgPanel.getUrl(), blackHole.getX(), blackHole.getY(), zhi, zhi, roomMapJlabel);
                    imgPanel.setKey(selectedDivImgPanel.getKey());


                    //删除房间实际数据
                    for (int i = 0; i < roomMapVOList.size(); i++) {
                        RoomMapVO roomMapVO = roomMapVOList.get(i);
                        if (roomMapVO.getX() == blackHole.getInformation().getPointX() && roomMapVO.getY() == blackHole.getInformation().getPointY()) {
                            roomMapVOList.remove(i);
                            roomMapJlabel.remove(roomMapVO.getImgPanel());
                            BoundingBox divBoundingBox = new BoundingBox(roomMapVO.getImgPanel().getX(), roomMapVO.getImgPanel().getY(), zhi, zhi);
                            mapQuadtree.remove(divBoundingBox);
                            i--;
                        }
                    }

                    //添加房间实际数据
                    RoomMapVO roomMapVO = new RoomMapVO();
                    roomMapVO.setX(blackHole.getInformation().getPointX());
                    roomMapVO.setY(blackHole.getInformation().getPointY());
                    roomMapVO.setMapType((Integer) selectedDivImgPanel.getKey());
                    roomMapVO.setImgPanel(imgPanel);
                    roomMapVOList.add(roomMapVO);

                    //点击事件处理
                    BoundingBox divBoundingBox = new BoundingBox(imgPanel.getX(), imgPanel.getY(), zhi, zhi);
                    divBoundingBox.setRenderAllPowerful(new AllPowerful<BoundingBox, Object>() {
                        @Override
                        public Container hook(BoundingBox gameObject) {
                            selectedDivImgPanel = imgPanel;
                            roomMapJlabel.remove(imgPanel);
                            selectedDivImgPanel.setLocation(roomMapJlabel.getX() + selectedDivImgPanel.getX(), roomMapJlabel.getY() + selectedDivImgPanel.getY());
                            drawingBoard.add(selectedDivImgPanel, 0);
                            drawingBoard.repaint();
                            //删除房间实际数据
                            roomMapVOList.remove(roomMapVO);
                            mapQuadtree.remove(divBoundingBox);
                            return null;
                        }
                    });
                    mapQuadtree.insert(divBoundingBox);

                    drawingBoard.repaint();
                }
            }
        }
    }

    // room对象
    private RoomDTO roomDTO;

    public void queryMapDiv(MyButton myButton) {
        myButton.addActionListener(new MyActionListener() {
            @Override
            public void action(ActionEvent e) {
                // 查询自己的房间
                FindRoomListRequest findRoomListRequest = new FindRoomListRequest();
                Response<FindRoomListResponse> findRoomListResponse = roomApi.findRoomList(findRoomListRequest);
                new Hint().run(findRoomListResponse.getState(), findRoomListResponse.getMsg());
                List<RoomDTO> roomDTOList = findRoomListResponse.getData().getRoomDTOList();

                if (roomDTOList != null) {
                    List<Call> listCall = new ArrayList<>();
                    listCall.add(new Call("地图名称", "roomName", 150));
                    listCall.add(new Call("地图编号", "roomNumber", 150));
                    listCall.add(new Call("创建时间", "createTime", 150));
                    listCall.add(new Call("创建时间", "updateTime", 150));
                    listCall.add(new Call("高度", "high", 150));
                    listCall.add(new Call("宽度", "width", 150));
                    listCall.add(new Call("操作", "", 150).setCallEnum(CallEnum.BUTTON)
                            .setOperationList(Arrays.asList(
                                    new Operation("载入地图", 100, new FormAllPowerful<Container, Container>() {
                                        @Override
                                        public Container hook(Container map) {
                                            //当前房间信息
                                            roomDTO = roomDTOList.get(this.getRow());
                                            //获取地图信息覆盖当前地图
                                            Response<FindRoomMapListResponse> queryRoomMapResponse = roomMapApi.findRoomMapList(new FindRoomMapListRequest(roomDTO.getRoomId()));
                                            new Hint().run(queryRoomMapResponse.getState(), queryRoomMapResponse.getMsg());
                                            List<RoomMapDTO> roomMaps = queryRoomMapResponse.getData().getRoomMapDTOList();
                                            //载入地图
                                            loadMap(roomMaps, roomDTO);
                                            new Hint().runDelay(0, "地图渲染成功", 1000);
                                            //关闭窗口
                                            this.getPopUp().delet();
                                            return map;
                                        }
                                    })
                            )));
                    PopUp<Room> popUp = new PopUp("我的地图列表", 350, 20, 750, 700, true, listCall, roomDTOList);

                    MyButton RemoveJlabel = util.setMyButton("退出", 0, 0, 200, 30);
                    RemoveJlabel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            popUp.delet();
                        }
                    });
                    popUp.addTailJlabels(RemoveJlabel);
                }
            }
        });
    }

    // 正方形长宽数值
    private final int zhi = 80;

    //  间距
    private final int backGauge = 30;

    //房间画布
    private RoomMapJlabel roomMapJlabel;

    //房间黑洞
    private BlackHoleHandle<Coordinates> blackHoleHandle;

    //元素节点
    private List<RoomMapVO> roomMapVOList;


    //载入地图
    public void loadMap(List<RoomMapDTO> roomMapDTOList, RoomDTO roomDTO) {
        if (this.roomMapJlabel != null) {
            drawingBoard.remove(roomMapJlabel);
        }

        roomDTOJLabel.setText("房间 : "+roomDTO.getRoomName());

        roomMapJlabel = util.setJLabel("", 220, 100, zhi * roomDTO.getWidth() + backGauge, zhi * roomDTO.getHigh() + backGauge, null, true, 1);
        drawingBoard.add(roomMapJlabel);
        mapQuadtree = new Quadtree(0, new BoundingBox(0, 0, zhi * roomDTO.getWidth() + backGauge, zhi * roomDTO.getHigh() + backGauge));

        // 设置长的数量&&设置宽的数量
        // 获取所有网格绘制线
        List<Line> lineList = new ArrayList<>();
        List<Line> lineListjs = new ArrayList<>();
        for (int i = 0; i <= roomDTO.getWidth(); i++) {
            Line line = new Line(backGauge / 2 + (i * zhi), backGauge / 2, backGauge / 2 + (i * zhi), roomDTO.getHigh() * zhi + backGauge / 2);
            lineList.add(line);
            lineListjs.add(line);
        }

        // 黑洞点
        List<Point> pointList = new ArrayList<>();
        for (int i = 0; i <= roomDTO.getHigh(); i++) {
            Line line = new Line(backGauge / 2, backGauge / 2 + (i * zhi), roomDTO.getWidth() * zhi + backGauge / 2, backGauge / 2 + (i * zhi));
            lineList.add(line);
            // 计算交集黑洞点
            if (i == roomDTO.getHigh()) {
                continue;
            }
            for (int z = 0; z < lineListjs.size() - 1; z++) {
                Line lineThis = lineListjs.get(z);
                Point<Coordinates> point = LineMath.lineIntersect(new Point(line.starx, line.stary), new Point(line.endx, line.endy),
                        new Point(lineThis.starx, lineThis.stary), new Point(lineThis.endx, lineThis.endy));
                //缓存节点坐标
                point.setInformation(new Coordinates(z, i));
                if (point != null) {
                    pointList.add(point);
                }
            }
        }

        this.blackHoleHandle = new BlackHoleHandle(pointList, 40);
        blackHoleHandle.scopeDebugging(roomMapJlabel, true);
        // 网格画布
        roomMapJlabel.allPowerfulList.add(new AllPowerful<MyGraphics, Container>() {
            @Override
            public Container hook(MyGraphics myGraphics) {
                Graphics g = myGraphics.getGraphics();
                g.setColor(Color.black);
                for (Line line : lineList) {
                    g.drawLine(line.getStarx(), line.getStary(), line.getEndx(), line.getEndy());
                }
                return null;
            }
        });

        roomMapVOList = new ArrayList();

        //渲染新数据
        roomMapDTOList.forEach(roomMapDTO -> {
            blackHoleHandle.getBlackHoleList().forEach(blackHole -> {
                //比对坐标
                Coordinates coordinates = blackHole.getInformation();
                if (roomMapDTO.getX() == coordinates.getPointX() && roomMapDTO.getY() == coordinates.getPointY()) {
                    ImgPanel imgPanel = util.setImgPanel(imgPanelMap.get(roomMapDTO.getMapType()).getUrl(), blackHole.getX(), blackHole.getY(), zhi, zhi, roomMapJlabel);
                    imgPanel.setKey(roomMapDTO.getMapType());
                    RoomMapVO roomMapVO = new RoomMapVO();

                    //存储地图信息
                    BeanUtils.copyProperties(roomMapDTO, roomMapVO);
                    roomMapVO.setImgPanel(imgPanel);
                    roomMapVOList.add(roomMapVO);

                    //点击处理
                    BoundingBox divBoundingBox = new BoundingBox(imgPanel.getX(), imgPanel.getY(), zhi, zhi);
                    divBoundingBox.setRenderAllPowerful(new AllPowerful<BoundingBox, Object>() {
                        @Override
                        public Container hook(BoundingBox gameObject) {
                            selectedDivImgPanel = imgPanel;
                            roomMapJlabel.remove(imgPanel);
                            selectedDivImgPanel.setLocation(roomMapJlabel.getX() + selectedDivImgPanel.getX(), roomMapJlabel.getY() + selectedDivImgPanel.getY());
                            drawingBoard.add(selectedDivImgPanel, 0);
                            drawingBoard.repaint();
                            //删除房间实际数据
                            roomMapVOList.remove(roomMapVO);
                            mapQuadtree.remove(divBoundingBox);
                            return null;
                        }
                    });
                    mapQuadtree.insert(divBoundingBox);
                    return;
                }
            });
        });
        client.drawingBoard.repaint();
    }

    //弹出新增房间表单
    public JLabel bounceFrame() {
        JLabel jlabel = util.setJLabel("", 300, 100, 360, 360, true, Color.white);

        util.setJLabel("新增房间", 0, 0, jlabel.getWidth(), 30, jlabel, true, Color.white);

        //输入区域

        util.setJLabel("名称", 10, 50, 140, 30, jlabel, false);
        JTextField roomName = util.addJTextField(12, 160, 50, 140, 30, "", jlabel);

        util.setJLabel("高度", 10, 100, 140, 30, jlabel, false);
        JTextField high = util.addJTextField(3, 160, 100, 140, 30, "", jlabel);

        util.setJLabel("宽度", 10, 150, 140, 30, jlabel, false);
        JTextField width = util.addJTextField(3, 160, 150, 140, 30, "", jlabel);

        JRadioButton roomType = new JRadioButton("地图", true);
        roomType.setBounds(10, 200, 140, 30);
        ButtonGroup roomTypeGroup = new ButtonGroup();
        roomTypeGroup.add(roomType);
        jlabel.add(roomType);

        JRadioButton roomPublicTrue = new JRadioButton("私有地图", true);
        roomPublicTrue.setBounds(10, 250, 140, 30);
        JRadioButton roomPublicVFalse = new JRadioButton("公开地图");
        roomPublicVFalse.setBounds(170, 250, 140, 30);
        ButtonGroup roomPublicGroup = new ButtonGroup();
        roomPublicGroup.add(roomPublicTrue);
        roomPublicGroup.add(roomPublicVFalse);
        jlabel.add(roomPublicTrue);
        jlabel.add(roomPublicVFalse);

        util.setMyButton("新增", 60, 300, 100, 30, jlabel, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CreateRoomRequest createRoomRequest = new CreateRoomRequest();
                createRoomRequest.setRoomName(roomName.getText());
                createRoomRequest.setHigh(Integer.parseInt(high.getText()));
                createRoomRequest.setWidth(Integer.parseInt(width.getText()));
                createRoomRequest.setRoomType("0");
                for (Enumeration buttons = roomPublicGroup.getElements(); buttons.hasMoreElements(); ) {
                    AbstractButton button = (AbstractButton) buttons.nextElement();
                    if (button.isSelected()) {
                        switch (button.getText()) {
                            case "公开地图":
                                createRoomRequest.setRoomPublic(0);
                                break;
                            case "私有地图":
                                createRoomRequest.setRoomPublic(1);
                                break;
                        }
                    }
                }
                Response response = roomApi.createRoom(createRoomRequest);
                new Hint().run(response.getState(), response.getMsg());
                if (response.getState() == 0) {
                    drawingBoard.remove(jlabel);
                    drawingBoard.repaint();
                }
            }
        });

        util.setMyButton("取消", 180, 300, 100, 30, jlabel,
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        drawingBoard.remove(jlabel);
                        drawingBoard.repaint();
                    }
                });
        return jlabel;
    }


    //房间移动
    public void roomMapJlabelMove(int x, int y) {
        if (roomMapJlabel != null) {
            roomMapJlabel.setLocation(roomMapJlabel.getX() + x, roomMapJlabel.getY() + y);
        }
    }

    @Override
    public void removeCache() {
        // 黑洞置空
    }

}
