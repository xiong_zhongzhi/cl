package com.cl.awt.page.establishRoom;

import com.cl.awt.Listener;
import com.cl.awt.core.geometry.Point;
import com.cl.awt.js.tree.quadtree.BoundingBox;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class EstablishRoomListener extends Listener {

    private EstablishRoom establishRoom;

    public EstablishRoomListener(EstablishRoom establishRoom) {
        super();
       this.establishRoom = establishRoom;
    }

    private Boolean keyFlag = false;

    private Boolean mouseFlag = false;

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 32) {
            keyFlag = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == 32) {
            keyFlag = false;
            mouseFlag = false;
        }
    }

    private Point point ;

    @Override
    public void mousePressed(MouseEvent e) {
        if (keyFlag) {
            mouseFlag = true;
            point = new Point(e.getX(), e.getY());
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (keyFlag && mouseFlag) {
            int x =e.getX()-point.getX();
            int y =e.getY()-point.getY();
            //设置拖动样式
            establishRoom.roomMapJlabelMove(x,y);
            mouseFlag = false;
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        establishRoom.selectedDivImgPanel(e.getX(),e.getY());
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON3) {
            establishRoom.cancellationSelected();
        }
        if (e.getButton() == MouseEvent.BUTTON1) {
            BoundingBox boundingBox = new BoundingBox(e.getX(),e.getY(),1,1);
            establishRoom.divSelected(boundingBox);
            establishRoom.divSelectedPlace(boundingBox);
            //不能与上面的方法顺序颠倒
            establishRoom.mapDivSelected(boundingBox);
        }
    }

}
