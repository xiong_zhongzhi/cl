package com.cl.awt.core.blackHole;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import com.cl.awt.core.geometry.Point;
import com.cl.awt.js.hookJlable.MyGraphics;
import com.cl.awt.js.hookJlable.RoomMapJlabel;
import com.cl.been.Container;
import com.cl.util.AllPowerful;

/**
 * 黑洞处理板块
 * @author
 */
public class BlackHoleHandle<T> {

	/**
	 * 吸附节点集合
	 */
	private List<BlackHole<T>> BlackHoleList = new ArrayList<>();

	public List<BlackHole<T>> getBlackHoleList() {
		return BlackHoleList;
	}

	public void scopeDebugging(RoomMapJlabel roomMapJlabel, Boolean flag){
		if (flag){
			// 黑洞画布
			roomMapJlabel.allPowerfulList.add(new AllPowerful<MyGraphics, Container>() {
				@Override
				public Container hook(MyGraphics myGraphics) {
					Graphics g = myGraphics.getGraphics();
					g.setColor(Color.black);
					for (BlackHole blackHole : BlackHoleList) {
						Graphics2D g2d = (Graphics2D) g;

						// 开启抗锯齿
						g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

						// 设置画笔颜色
						g2d.setColor(Color.BLACK);

						// 画一个没有填充颜色的圆
						int centerX =blackHole.getX();
						int centerY = blackHole.getY();
						int radius = blackHole.getSuctionSuction();
						g2d.drawOval(centerX - radius, centerY - radius, radius * 2, radius * 2);

					}
					return null;
				}
			});
		}
	}

	//批量载入黑洞
	public BlackHoleHandle(List<Point> pointList,Integer suctionSuction){
		pointList.forEach(point->{
			BlackHole blackHole = new BlackHole(point,suctionSuction);
			blackHole.setInformation(point.getInformation());
			BlackHoleList.add(blackHole);
		});
	}

	 /**
	 * @param x,y 判断一个点是否在圆形区域内,比较坐标点与圆心的距离是否小于半径
     */
	public BlackHole adsorb(Integer x,Integer y){
		List<BlackHole> blackHoleList = new ArrayList<>();
		BlackHoleList.forEach(blackHole->{
			Double c = -1.0;
			//算出三角形的2个边长
			//底边
			Integer a = 0;
			//高边
			Integer b = 0;	
			if(x > blackHole.getX()) {
				a = x - blackHole.getX();
			}else {
				a = blackHole.getX() - x;
			}
			if(y > blackHole.getY()) {
				b = y - blackHole.getY();
			}else {
				b = blackHole.getY() - y;
			}
			//判断为水平或者垂直情况
			if(a == 0) {
				c = Double.parseDouble(b.toString());
				//判断长度
				if(c <= blackHole.getSuctionSuction()) {
					blackHoleList.add(blackHole);
				}
				return;
			}
			if(b == 0) {
				c = Double.parseDouble(a.toString());
				//判断长度
				if(c <= blackHole.getSuctionSuction()) {
					blackHoleList.add(blackHole);
				}
				return;
			}
			//三角函数计算c值
			c = Math.pow(Double.parseDouble(a.toString()),2)+Math.pow(Double.parseDouble(b.toString()),2);
			c = Math.sqrt(c);
			if(c <= blackHole.getSuctionSuction()) {
				blackHoleList.add(blackHole);
			}
		});
		if(blackHoleList.size() > 0) {
			return blackHoleList.get(0);
		}
		return null;
	}
	
	
	
}
