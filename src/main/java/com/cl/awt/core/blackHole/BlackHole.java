package com.cl.awt.core.blackHole;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

import com.cl.awt.core.geometry.Point;
import com.cl.been.Container;

/**
 * 黑洞点
 * @author
 */
public class BlackHole <T>{

	public BlackHole(int x,int y){
		this.x = x;
		this.y = y;
	}
	
	public BlackHole(Point point,int suctionSuction){
		this.x = point.getX();
		this.y = point.getY();
		this.suctionSuction = suctionSuction;
	}
	
	//黑洞坐标
	private int x;
	
	private int y;

	//附加信息
	private T information;

	public T getInformation() {
		return information;
	}

	public void setInformation(T information) {
		this.information = information;
	}

	//黑洞吸力范围
	private int suctionSuction;
	
	//黑洞内物体
	List<JLabel> jLabelList = new ArrayList<JLabel>();
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getSuctionSuction() {
		return suctionSuction;
	}

	public void setSuctionSuction(int suctionSuction) {
		this.suctionSuction = suctionSuction;
	}

	public List<JLabel> getjLabelList() {
		return jLabelList;
	}

	public void setjLabelList(List<JLabel> jLabelList) {
		this.jLabelList = jLabelList;
	}

	
}
