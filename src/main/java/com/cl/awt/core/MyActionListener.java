package com.cl.awt.core;

import com.cl.awt.Client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class MyActionListener implements ActionListener {
  @Override
  public void actionPerformed(ActionEvent e){
      action(e);
      Client.getClient().drawingBoard.requestFocus();//重置焦点为整体框架
  }

  protected abstract void action(ActionEvent e);

}
