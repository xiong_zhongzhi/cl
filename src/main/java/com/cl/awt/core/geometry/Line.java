package com.cl.awt.core.geometry;
/**
 * 
 * @author csyq
 * 线
 *
 */
public class Line {

	public int starx;
	
	public int stary;
	
	public int endx;
	
	public int endy;
	
	public Line(int starx, int stary, int endx, int endy) {
		super();
		this.starx = starx;
		this.stary = stary;
		this.endx = endx;
		this.endy = endy;
	}

	@Override
	public String toString() {
		return "Line [starx=" + starx + ", stary=" + stary + ", endx=" + endx + ", endy=" + endy + "]";
	}


	public int getStarx() {
		return starx;
	}

	public void setStarx(int starx) {
		this.starx = starx;
	}

	public int getStary() {
		return stary;
	}

	public void setStary(int stary) {
		this.stary = stary;
	}

	public int getEndx() {
		return endx;
	}

	public void setEndx(int endx) {
		this.endx = endx;
	}

	public int getEndy() {
		return endy;
	}

	public void setEndy(int endy) {
		this.endy = endy;
	}
	
}
