package com.cl.awt.core.geometry;


/**
 * 
 * @author csyq
 * 点
 */
public class Point <T>{
	public int x;
	
	public int y;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	//附加信息
	private T information;

	public T getInformation() {
		return information;
	}

	public void setInformation(T information) {
		this.information = information;
	}

	public Point(int x, int y,T  information) {
		super();
		this.x = x;
		this.y = y;
		this.information = information;
	}

	public Point(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + "]";
	}
	
}
