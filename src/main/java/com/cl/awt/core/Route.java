package com.cl.awt.core;

import com.cl.handler.page.PageUrl;

/**
* @ClassName: Route
* @Description: TODO(路由对象)
* @author 沉思有顷:564715118
* @date 2022年1月2日
*/
public class Route {

	//页面文件路径
	private String url;

	//页面实例
	private MyPage myPage;

	//页面文件路径
	private PageUrl pageUrl;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public PageUrl getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(PageUrl url) {
		this.pageUrl = url;
	}

	public MyPage getPage() {
		return myPage;
	}

	public void setPage(MyPage myPage) {
		this.myPage = myPage;
	}

}
