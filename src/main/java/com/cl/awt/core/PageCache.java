package com.cl.awt.core;

import com.cl.handler.page.PageUrl;

public class PageCache {
	private 	PageUrl pageUrl;

	public PageUrl getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(PageUrl pageUrl) {
		this.pageUrl = pageUrl;
	}
}
