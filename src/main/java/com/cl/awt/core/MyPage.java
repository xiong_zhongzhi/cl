package com.cl.awt.core;

import java.awt.event.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;

import com.cl.awt.Client;
import com.cl.awt.DrawingBoard;
import com.cl.awt.js.Util;
import com.cl.been.Container;
import com.cl.handler.Jurisdiction.Drawer;
import com.cl.handler.page.PageUrlHandler;
import com.cl.util.AllPowerful;

public abstract class MyPage {
	
	//核心工具方法
	public Util util = new Util();

	//画框对象
    public static Client client;

    //画板对象
    public DrawingBoard drawingBoard;
    
    //加载方法的参数信息
    public String[] methodParamNameList = new String[0];
    
    //跳转需保存的画布hook
    public List<AllPowerful<Container, Container>> saveAllPowerfulList= new ArrayList<AllPowerful<Container, Container>>();
    
    //传入数据缓存
    public Container data;
    
    //页面缓存
    public PageCache pageCache;
    
    //加载页面对象方法
    public Method load;
  
    public MyPage() {
    }
    
    public MyPage(Method method) {
    	load = method;
    }
    
    //设置页面名称:默认使用页面名称为页面标题
    public void setTitle() {
    	client.setTitle(pageCache.getPageUrl().pageName());
    }
    
    //回退按钮渲染
    public void rollbackJButton() {
    	if(Drawer.get(PageUrlHandler.class).linkRoute.size() > 1) {
    	//判断前置
    		JButton root = new JButton("回退");
    		rollback(root);
        	root.setBounds(10, 0, 100, 30);
        	drawingBoard.add(root,0);
    	}
    }
    
    //回退加载
    public void rollback(JButton saveButton) {
    	 // 为按钮绑定监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rollbackExecute();
            }
        });
    }

    public void rollbackExecute(){
        PageUrlHandler pageUrlHandler = Drawer.get(PageUrlHandler.class);
        MyPage rollbackMyPage = Drawer.get(PageUrlHandler.class).linkRoute.get(pageUrlHandler.linkRoute.size()-2);
        pageUrlHandler.linkRoute.remove(pageUrlHandler.linkRoute.size()-1);
        remove();
        rollbackMyPage.run(rollbackMyPage.data);
    }
    
    //页面加载
    public void run(Container data) {
    	//缓存请求数据
    	this.data = data==null?new Container():data;
    	//清空数据
    	drawingBoard.removeAll();
    	//清空监听
        if(drawingBoard.getMouseListeners().length>0){
            MouseListener[] mouseListeners = drawingBoard.getMouseListeners();
            for (MouseListener mouseListener:mouseListeners) {
                drawingBoard.removeMouseListener(mouseListener);
            }
        }

        if(drawingBoard.getMouseMotionListeners().length>0){
            MouseMotionListener[] mouseListeners = drawingBoard.getMouseMotionListeners();
            for (MouseMotionListener mouseMotionListener:mouseListeners) {
                drawingBoard.removeMouseMotionListener(mouseMotionListener);
            }
        }

        if(drawingBoard.getKeyListeners().length>0){
            KeyListener[] keyListeners = drawingBoard.getKeyListeners();
            for (KeyListener keyListener:keyListeners) {
                drawingBoard.removeKeyListener(keyListener);
            }
        }

    	//设置页头
    	setTitle();
    	//加载页面
    	Object[] o = new Object[methodParamNameList.length];
    	for (int i = 0;i < methodParamNameList.length;i++) {
    		o[i] = data.get(methodParamNameList[i]);
		}
    	try {
			load.invoke(this,o);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
    	//回退
        if (pageCache.getPageUrl().rollbackFlag()){
            rollbackJButton();
        }
    	//渲染
    	drawingBoard.repaint();
    }
    
    //清空方法
    public void remove() {
		//清空内部使用数据
		removeCache();
    }
    
    //清空缓存
    public void removeCache() {
    }

}
