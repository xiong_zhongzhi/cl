package com.cl.log;

public class OutPut {
	
	private final static EnumOutPut enumOutPutConfig = EnumOutPut.INFO;
	
	public static void out(Object object) {
		out(object.toString(),EnumOutPut.INFO);
	}
	
	public static void out(Object object,EnumOutPut enumOutPut) {
		if(enumOutPut.getLevel()>= enumOutPutConfig.getLevel()) {
			System.out.println(object.toString());
		}
	}
	
}
