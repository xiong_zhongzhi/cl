package com.cl.log;
/**
 * 日志级别控制
 * @author 564715118
 */
public enum EnumOutPut { 
	TRACE(0), DEBUG(1), INFO(2), WARN(3), ERROR(4), FATAL(5), OFF(6);

	private Integer level;

	EnumOutPut(Integer level) {
		this.level = level;
	}
	
	public Integer getLevel() {
		return level;
	}

}
