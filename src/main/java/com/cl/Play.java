    package com.cl;

import com.cl.awt.Client;
import com.cl.awt.js.Util;
import com.cl.awt.page.login.Login;
import com.cl.been.Container;
import com.cl.handler.Jurisdiction.JurisdictionHandler;
import com.cl.link.Link;
import com.cl.util.hotLoading.fileListener.FileListenerRunner;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class Play {

	public static void main(String[] args) throws Exception {
	    //热加载
        new FileListenerRunner().start();
        //注解处理器
        JurisdictionHandler.handler("com.cl");
        //加载客户端画面
        Client.getClient();
        Util util = new Util();
        util.skip(Login.class, new Container());
        //客户端与服务端建立链接
		Link link = new Link(getHostIp(),8089);
		link.info();
	}

	private static String getHostIp(){
        try{
            Enumeration<NetworkInterface> allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            while (allNetInterfaces.hasMoreElements()){
                NetworkInterface netInterface = (NetworkInterface) allNetInterfaces.nextElement();
                Enumeration<InetAddress> addresses = netInterface.getInetAddresses();
                while (addresses.hasMoreElements()){
                    InetAddress ip = (InetAddress) addresses.nextElement();
                    if (ip != null
                            && !ip.isLoopbackAddress() //loopback地址即本机地址，IPv4的loopback范围是127.0.0.0 ~ 127.255.255.255
                            && ip.getHostAddress().indexOf(":")==-1){
                        return ip.getHostAddress();
                    } 
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
	
}
