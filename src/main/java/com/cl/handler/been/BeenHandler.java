package com.cl.handler.been;

import com.cl.handler.Jurisdiction.Drawer;
import com.cl.handler.Jurisdiction.Handler;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.Jurisdiction.RulesOfCompetency.Status;
import com.cl.util.MyExceptionIncomplete;

/**
 * @ClassName: BeenHandler
 * @Description: TODO(类注入)
 * @author 沉思有顷:564715118
 * @date 2020年10月27日
 */
@RulesOfCompetency(status = Status.HANDLER, annotation = Been.class)
public class BeenHandler extends Handler {
	
	Been been;

	public void Presupposition() {
		been = (Been) super.annotationObject;
	}

	@Override
	public void handler(HandlerType handlerType) {
		switch (handlerType) {
		case CLASS:
			//创建对象
			try {
				Drawer.add(classObject.getName(), classObject.newInstance());
			} catch (InstantiationException | IllegalAccessException e) {
				new MyExceptionIncomplete(classObject.getName()+" : 类实例化失败",e);
			}
			break;
		case METHOD:
			//执行方法
			
			
			break;
		}
	}

}
