package com.cl.handler.been;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;

/**
 * @ClassName: RulesOfCompetency
 * @Description: TODO(扫描器)
 * @author 564715118
 * @date 2019年10月28日
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
public @interface Been {
	public String value() default "";//页面名称
}
