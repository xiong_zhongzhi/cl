package com.cl.handler.page;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.cl.handler.Jurisdiction.RulesOfCompetency;


/**
* @ClassName: PageUrl
* @Description: TODO(页面标识)
* @author 沉思有顷:564715118
* @date 2022年1月2日
*/
@RulesOfCompetency
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PageUrl {

	public String url() ;

	public String pageName();//页面名称

	public boolean rollbackFlag() default true;//是否展会返回按钮



}

