package com.cl.handler.page;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cl.awt.core.MyPage;
import com.cl.awt.core.Route;
import com.cl.handler.Jurisdiction.Handler;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.Jurisdiction.RulesOfCompetency.Status;

/**
 * @ClassName: PageUrlHandler
 * @Description: TODO(页面处理器)
 * @author 沉思有顷:564715118
 * @date 2022年1月5日
 */
@RulesOfCompetency(status = Status.HANDLER, annotation = PageUrl.class)
public class PageUrlHandler extends Handler {
	
	private PageUrl pageUrl;
	
	public void Presupposition() {
		pageUrl = (PageUrl) super.annotationObject;
	}
	
	//链路
	public volatile List<MyPage> linkRoute = new ArrayList<>();
	
	//路由容器
	public Map<String,Route> routeMap = new HashMap<>();

	@Override
	public void handler(HandlerType handlerType) {
		Route route = new Route();
		route.setPageUrl(pageUrl);
		route.setUrl(classObject.getName());
		routeMap.put(pageUrl.url(),route);
	}

}
