package com.cl.handler.Jurisdiction;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;

/**
 * @ClassName: RulesOfCompetency
 * @Description: TODO(扫描器)
 * @author 564715118
 * @date 2019年10月28日
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RulesOfCompetency {
	public enum Status {
		ON, // ON 需要扫描注解的普通类
		HANDLER// HANDLER 表示处理器
	};
	
	Status status()

	default Status.ON;

	Class<?> annotation() default RulesOfCompetency.class;// 注解对象
}
