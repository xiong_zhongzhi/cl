package com.cl.handler.Jurisdiction;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.annotation.AnnotatedElementUtils;



/**
 * @ClassName: JurisdictionHandler
 * @Description: TODO(处理处理器)
 * @author 沉思有顷:564715118
 * @date 2020年10月20日
 */
public class JurisdictionHandler {

	public static void handler(String string) throws Exception {
		List<Class<?>> classObjectList = new ArrayList<Class<?>>();
		List<Handler> HandlerList = new ArrayList<Handler>();
		for (String url : ClazzUtils.getClazzName(string, true)) {
			Class<?> classObject = Class.forName(url);
			RulesOfCompetency rulesOfCompetency = AnnotatedElementUtils.getMergedAnnotation(classObject,
					RulesOfCompetency.class);// 通过反射查找当前类有没有RulesOfCompetency注解
			if (rulesOfCompetency != null) {
				switch (rulesOfCompetency.status()) {
				case ON:
					classObjectList.add(classObject);
					break;
				case HANDLER:
					Handler handler = (Handler) classObject.newInstance();
					handler.annotation = rulesOfCompetency.annotation();
					HandlerList.add(handler);
					// 注解对象注入比较特殊，注解对象注入Drawer 实际上是注入了各个注解对应的处理器
					Drawer.add(classObject.getName(), handler);
					break;
				}
			}
		}
		for (Class<?> cla : classObjectList) {
			for (Handler handler : HandlerList) {
				// 类注解处理
				Object annotationObject = cla.getAnnotation(handler.annotation);// 通过反射查找当前类有没有RulesOfCompetency注解
				if (annotationObject != null) {
					handler.handlerRunClass(annotationObject, cla);
				}
				// 方法注解处理
				for (Method method : cla.getDeclaredMethods()) {
					Object annotationObjectMethod = method.getAnnotation(handler.annotation);
					if (annotationObjectMethod != null) {
						handler.handlerRunMethod(annotationObjectMethod, cla, method);
					}
				}
			}
		}
		// 数据清空
		for (Handler handler : HandlerList) {
			handler.emptyHandler(null, null, null);
		}
	}
}
