package com.cl.handler.Jurisdiction;

import java.lang.reflect.Method;


public abstract class Handler {
	
	public Class annotation;//注解class对象
	
	public Object annotationObject;//注解对象
	
	public Class classObject;//类对象
	
	public Method method;//方法对象
		  
	public void handlerRunClass(Object annotationObject,Class classObject) {
		this.annotationObject = annotationObject;
		this.classObject = classObject;
		Presupposition();
		handler(HandlerType.CLASS);
	}
	
	public void handlerRunMethod(Object annotationObject,Class ClassObject,Method method) {
		this.annotationObject = annotationObject;
		this.classObject = ClassObject;
		this.method = method;
		Presupposition();
		handler(HandlerType.METHOD);
	}
	
	public void emptyHandler(Object annotationObject,Class ClassObject,Method method) {
		this.annotationObject = annotationObject;
		this.classObject = ClassObject;
		this.method = method;
	}
	
	public abstract void Presupposition();
	
	public abstract void handler(HandlerType handlerType);
	
	public enum HandlerType{
		CLASS//类处理
		,METHOD//方法处理
	}
}
