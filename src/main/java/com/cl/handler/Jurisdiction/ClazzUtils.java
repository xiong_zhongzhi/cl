package com.cl.handler.Jurisdiction;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 *资源加载器
 */

public class ClazzUtils {

	private static final String CLASS_SUFFIX = ".class";

	private static final String CLASS_FILE_PREFIX = File.separator + "classes" + File.separator;

	private static final String PACKAGE_SEPARATOR = ".";

	public static List<String> getClazzName(String packageName, boolean showChildPackageFlag) {		
		List<String> result = new ArrayList<String>();
		String suffixPath = packageName.replaceAll("\\.", "/");
		//将进行转义为 /		
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		//获取类的加载器对象
		try {
			//我们都使用相对路径来获取资源
			Enumeration<URL> urls = loader.getResources(suffixPath);
			//枚举类对象  可以通过nextElement获取下一个值   hasMoreElements 布尔去判断是否有值
			while (urls.hasMoreElements()) {
				URL url = urls.nextElement();
				if (url != null) {
					String protocol = url.getProtocol();
					//获取url的协议//这里判断当前路径是指向jar 还是指向文件
					if ("file".equals(protocol)) {
						String path = url.getPath();
						path = java.net.URLDecoder.decode(path, "utf-8");//再进行一次编码防止乱码
						//获取url路径部分
						result.addAll(getAllClassNameByFile(new File(path), showChildPackageFlag));
					} else if ("jar".equals(protocol)) {
						JarFile jarFile = null;
						try {
							jarFile = ((JarURLConnection) url.openConnection()).getJarFile();
						} catch (Exception e) {
							e.printStackTrace();
						}
						if (jarFile != null) {
							result.addAll(getAllClassNameByJar(jarFile, packageName, showChildPackageFlag));
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	static List<String> getAllClassNameByFile(File file, boolean flag) throws UnsupportedEncodingException {
		List<String> result = new ArrayList<String>();
		if (!file.exists()) {
			//判断当前文件是否还存在,存在则继续,不存在则返回空对象
			return result;
		}
		if (file.isFile()) {//判断是否为标准文件//很明显文件目录并不是标准文件
			String path = file.getPath();
			path = java.net.URLDecoder.decode(path, "utf-8");//再进行一次编码防止乱码
			// 注意：这里替换文件分割符要用replace。因为replaceAll里面的参数是正则表达式,而windows环境中File.separator="\\"的,因此会有问题
			if (path.endsWith(CLASS_SUFFIX)) {
				path = path.replace(CLASS_SUFFIX, "");
				// 从"/classes/"后面开始截取
				String clazzName = path.substring(path.indexOf(CLASS_FILE_PREFIX) + CLASS_FILE_PREFIX.length()).replace(File.separator, PACKAGE_SEPARATOR);
				if (-1 == clazzName.indexOf("$")) {
					result.add(clazzName);
				}
			}
			return result;
		} else {
			File[] listFiles = file.listFiles();//]=]=获取当前文件下的所有文件路径
			if (listFiles != null && listFiles.length > 0) {
				for (File f : listFiles) {
					if (flag) {//判断是否只需要第一层
						result.addAll(getAllClassNameByFile(f, flag));
					} else {
						if (f.isFile()) {
							String path = f.getPath();
							if (path.endsWith(CLASS_SUFFIX)) {
								path = path.replace(CLASS_SUFFIX, "");
								// 从"/classes/"后面开始截取
								String clazzName = path
										.substring(path.indexOf(CLASS_FILE_PREFIX) + CLASS_FILE_PREFIX.length())
										.replace(File.separator, PACKAGE_SEPARATOR);
								if (-1 == clazzName.indexOf("$")) {
									result.add(clazzName);
								}
							}
						}
					}
				}
			}
			return result;
		}
	}

	private static List<String> getAllClassNameByJar(JarFile jarFile, String packageName, boolean flag) {
		List<String> result = new ArrayList<String>();
		Enumeration<JarEntry> entries = jarFile.entries();
		while (entries.hasMoreElements()) {
			JarEntry jarEntry = entries.nextElement();
			String name = jarEntry.getName();
			// 判断是不是class文件
			if (name.endsWith(CLASS_SUFFIX)) {
				name = name.replace(CLASS_SUFFIX, "").replace("/", ".");
				if (flag) {
					// 如果要子包的文件,那么就只要开头相同且不是内部类就ok
					if (name.startsWith(packageName) && -1 == name.indexOf("$")) {
						result.add(name);
					}
				} else {
					// 如果不要子包的文件,那么就必须保证最后一个"."之前的字符串和包名一样且不是内部类
					if (packageName.equals(name.substring(0, name.lastIndexOf("."))) && -1 == name.indexOf("$")) {
						result.add(name);
					}
				}
			}
		}
		return result;
	}
}
