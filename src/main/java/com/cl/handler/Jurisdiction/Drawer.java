package com.cl.handler.Jurisdiction;

import java.util.HashMap;
import java.util.Map;

import com.cl.util.MyExceptionIncomplete;

/**
 * @ClassName: Drawer
 * @Description: TODO(抽取系统初始化存储类,对外只读)
 * @author 沉思有顷:564715118
 * @date 2020年10月27日
 */
@SuppressWarnings({ "unused", "unchecked" })
public class Drawer {
	private volatile static Map<String, Object> map = new HashMap<String, Object>();

	// 新增
	public static void add(String name, Object obj) {
		if (map.get(name) != null) {
			throw new MyExceptionIncomplete(name + " : 此类重复注册");
		}
		map.put(name, obj);
	}

	// 查询
	public static Object get(String name) {
		return map.get(name);
	}

	// 查询
	public static <T> T get(Class<T> clazz) {
		return (T) map.get(clazz.getName());
	}

}
