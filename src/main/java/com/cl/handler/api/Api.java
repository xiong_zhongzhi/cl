package com.cl.handler.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.http.HttpMethod;
/**
* @ClassName: MessageHandling
* @Description: TODO(定义接口服务端接口)
* @author 沉思有顷:564715118
* @date 2021年9月16日
*/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
public @interface Api {
	public String value();
	public Class<? extends ApiProxyTemplate> apiProxyClass() default ApiProxy.class;
	public String prefix() default "";
	public HttpMethod httpMethod() default HttpMethod.POST;
}