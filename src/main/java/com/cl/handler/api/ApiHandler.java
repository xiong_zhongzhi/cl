package com.cl.handler.api;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

import com.cl.been.Container;
import com.cl.handler.Jurisdiction.Drawer;
import com.cl.handler.Jurisdiction.Handler;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.Jurisdiction.RulesOfCompetency.Status;
import com.cl.util.MyExceptionIncomplete;

/**
 * @ClassName: ApiHandler
 * @Description: TODO(api处理器)
 * @author 沉思有顷:564715118
 * @date 2021年9月16日
 */
@RulesOfCompetency(status = Status.HANDLER, annotation = Api.class)
public class ApiHandler extends Handler {

	Container apiMap = new Container();

	Api api;
	Api fatherApi;

	public void Presupposition() {
		api = (Api) super.annotationObject;
		Object object = super.classObject.getAnnotation(Api.class);
		if (object != null) {
			fatherApi = (Api) object;
		}
	}

	// Cookie存储器
	public volatile Container containerCookie = new Container();

	// 代理对象存储器
	public volatile Map<String, ApiProxyTemplate> apiProxyTemplateMap = new HashMap<String, ApiProxyTemplate>();

	@Override
	public void handler(HandlerType handlerType) {
		if (handlerType == HandlerType.METHOD) {
			ApiProxyTemplate apiProxyTemplate = apiProxyTemplateMap.get(classObject.getName());
			apiProxyTemplate.getApiMap().put(super.method.getName(), api);
		} else {
			if (Drawer.get(classObject.getName()) == null) {
				Object apiProxy;
				try {
					ApiProxyTemplate apiProxyTemplate = fatherApi.apiProxyClass().newInstance();
					apiProxyTemplate.setFatherApi(fatherApi);
					apiProxy = Proxy.newProxyInstance(classObject.getClassLoader(), new Class[] { classObject },apiProxyTemplate);
					Drawer.add(classObject.getName(), apiProxy);
					apiProxyTemplateMap.put(classObject.getName(), apiProxyTemplate);
				} catch (IllegalArgumentException | InstantiationException | IllegalAccessException e) {
					throw new MyExceptionIncomplete("类型转换异常", e);
				}
			}
		}
	}

}
