package com.cl.handler.api;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

import com.alibaba.fastjson.JSONObject;
import com.cl.been.Container;
import com.cl.util.HttpsUtil;

/**
 * @author 沉思有顷:564715118
 * @ClassName: MessageHandling
 * @Description: TODO(代理处理机制)
 * @date 2021年9月16日
 */
public class ApiProxy extends ApiProxyTemplate {

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Api api = getApiMap().get(method.getName());
        if (api == null) {
            return proxy.getClass() + "@" + Integer.toHexString(hashCode());
        }
        String url = "";
        String prefix = "";
        if (api.prefix().equals("")) {
            prefix = getFatherApi().value();
            url = getFatherApi().value() + api.value();
        } else {
            prefix = api.prefix();
            url = api.prefix() + api.value();
        }
        System.out.println("请求路径------>" + url);
        Container container;
        if (args == null || args.length == 0) {
            container = new Container();
        } else {
            if (args[0] instanceof Container) {
                container = (Container) args[0];
            } else {
                container = Container.toContainer(args[0]);
            }
        }
        System.out.println("请求数据------>" + container.toJson());
        Class<?> clazz = method.getReturnType();
        java.lang.reflect.Type type = method.getGenericReturnType();
        Class<?> result;
        if (type instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType) type;
            result = (Class<?>) pType.getActualTypeArguments()[0];
            Object obj = null;
            obj = JSONObject.parseObject(HttpsUtil.mapPost(url, container, prefix, api.httpMethod()).toJson(), clazz);
            clazz.getMethod("setClazz", Class.class).invoke(obj, result);
            System.out.println("返回数据序列化后-->" + JSONObject.toJSONString(obj));
            return obj;
        } else {
            return JSONObject.parseObject(HttpsUtil.mapPost(url, container, prefix, api.httpMethod()).toJson(), clazz);
        }
    }

}
