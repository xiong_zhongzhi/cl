package com.cl.handler.api;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.cl.been.Container;
import com.cl.util.HttpsUtil;
/**
* @ClassName: MessageHandling
* @Description: TODO(代理处理机制)
* @author 沉思有顷:564715118
* @date 2021年9月16日
*/
public abstract class ApiProxyTemplate implements InvocationHandler {
	
	private Map<String,Api> apiMap = new HashMap<>();
	private Api fatherApi;

	public Api getFatherApi() {
		return fatherApi;
	}

	public void setFatherApi(Api fatherApi) {
		this.fatherApi = fatherApi;
	}

	public Map<String, Api> getApiMap() {
		return apiMap;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Api api = getApiMap().get(method.getName());
		if(api == null) {
			return proxy.getClass() + "@" + Integer.toHexString(hashCode());
		}
		String url = "";
		String prefix ="";
		if(api.prefix().equals("")) {
			prefix = getFatherApi().value();
			url = getFatherApi().value() + api.value();
		}else {
			prefix = api.prefix();
			url = api.prefix() + api.value();
		}
		System.out.println("请求路径------>"+url);
		Container container;
		if(args.length == 0) {
			container = new Container();
		}else {
			if(args[0] instanceof Container) {
				container = (Container)args[0];
			}else {
				container = Container.toContainer(args[0]);
			}
		}
	    Class<?> type = method.getReturnType();
		System.out.println("请求数据------>"+container.toJson());
		return JSONObject.parseObject(HttpsUtil.jsonPost(url,container,prefix,api.httpMethod()).toJson(),type);
	}

}
