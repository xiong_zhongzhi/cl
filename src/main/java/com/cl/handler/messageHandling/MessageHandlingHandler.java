package com.cl.handler.messageHandling;

import com.alibaba.fastjson.JSON;
import com.cl.been.Container;
import com.cl.been.Msg;
import com.cl.handler.Jurisdiction.Handler;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.Jurisdiction.RulesOfCompetency.Status;
import com.cl.log.OutPut;
import com.cl.util.MyExceptionIncomplete;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 沉思有顷:564715118
 * @ClassName: MessageHandlingHandler
 * @Description: TODO(MessageHandling 注解处理器 ; 客户端消息处理器)
 * @date 2020年10月27日
 */
@RulesOfCompetency(status = Status.HANDLER, annotation = MessageHandling.class)
public class MessageHandlingHandler extends Handler {
    MessageHandling messageHandling;

    public void Presupposition() {
        messageHandling = (MessageHandling) super.annotationObject;
    }

    // 存储各个数据处理方法 词 ：执行
    private Map<String, Method> map = new HashMap<String, Method>();
    // 存储各个数据处理方法的本体对象 词 ：对象
    private Map<String, Class> mapclass = new HashMap<>();

    @Override
    public void handler(HandlerType handlerType) {
        map.put(messageHandling.value(), super.method);
        try {
            mapclass.put(messageHandling.value(), super.classObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param msg 消息体
     * @return List<String>
     * @Title: listes1 TODO(// 指令执行器)
     */
    public void run(final Msg msg) throws Exception {
        //换成消息对象
        Method method = map.get(msg.getInstruct());
        if (method != null) {
            List<ParamTypeAndName> paramTypeAndNameList = getMethodParamName(method);
            Object[] o = new Object[paramTypeAndNameList.size()];
            if (paramTypeAndNameList.size() == 1) {
                ParamTypeAndName paramTypeAndName =  paramTypeAndNameList.get(0);
                if (paramTypeAndName.paramType == String.class) {
                    o[0] = String.valueOf(msg.getData());
                } else {
                    o[0] = JSON.parseObject(msg.getData().toString(), paramTypeAndName.paramType);
                }
            } else {
                for (int i = 0; i < paramTypeAndNameList.size(); i++) {
                    Container container = Container.jsonToContainer(msg.getData().toString());
                    ParamTypeAndName paramTypeAndName = paramTypeAndNameList.get(i);
                    if (paramTypeAndName.paramType == String.class) {
                        o[i] = container.getString(paramTypeAndName.name);
                    } else {
                        o[i] = JSON.parseObject(container.getString(paramTypeAndName.name), paramTypeAndName.paramType);
                    }
                }
            }
            method.invoke(mapclass.get(msg.getInstruct()).newInstance(), o);
        } else {
            OutPut.out("接收到服务器的未知指令 【" + msg.getInstruct() + "】 建议客户端进行版本更新");
        }
    }

    class ParamTypeAndName {
        public String name;
        public Class paramType;

        public ParamTypeAndName(String name, Class paramType) {
            this.name = name;
            this.paramType = paramType;
        }
    }

    /**
     * @param method 方法对象
     * @Description: TODO(获取对象参数名称数组)
     */
    public List<ParamTypeAndName> getMethodParamName(Method method) {
        List<ParamTypeAndName> paramTypeAndNameList = new ArrayList<>();
        try {
            LocalVariableTableParameterNameDiscoverer parameterNameDiscoverer = new LocalVariableTableParameterNameDiscoverer();
            String[] ParameterNames = parameterNameDiscoverer.getParameterNames(method);
            Class[] paramTypes = method.getParameterTypes();
            for (int i = 0; i < ParameterNames.length; i++) {
                String name = ParameterNames[i];
                Class type = paramTypes[i];
                ParamTypeAndName paramTypeAndName = new ParamTypeAndName(name, type);
                paramTypeAndNameList.add(paramTypeAndName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return paramTypeAndNameList;
    }

}
