package com.cl.handler.messageHandling;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
* @ClassName: MessageHandling
* @Description: TODO(处理客户端指令的方法定义)
* @author 沉思有顷:564715118
* @date 2020年10月27日
*/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface MessageHandling {
	public String value();//事件主体
}

