package com.cl.handler.event.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
* @ClassName: Workflow
* @Description: TODO(工作流注解)
* @author 564715118
* @date 2020年1月3日
*/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Workflow {
	public String[] subordinate();//所属流
	public String meaning();//节点名称
}
