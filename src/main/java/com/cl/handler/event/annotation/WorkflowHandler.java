package com.cl.handler.event.annotation;

import com.cl.handler.Jurisdiction.Handler;
import com.cl.handler.Jurisdiction.RulesOfCompetency;

import java.util.HashMap;
import java.util.Map;

@RulesOfCompetency(status = RulesOfCompetency.Status.HANDLER, annotation = Workflow.class)
public class WorkflowHandler extends Handler {

    public static Map<String, Map<String, WorkflowBeen>> workflowMap = new HashMap();
    Workflow workflow;

    @Override
    public void Presupposition() {
        workflow = (Workflow) super.annotationObject;
    }

    @Override
    public void handler(HandlerType handlerType) {
        for (String subordinate : workflow.subordinate()) {
            Map<String, WorkflowBeen> mapBool = workflowMap.get(subordinate);
            if (mapBool == null) {
                mapBool = new HashMap();
                workflowMap.put(subordinate, mapBool);
            }
            mapBool.put(workflow.meaning(),new WorkflowBeen(method,classObject));
        }
    }

}
