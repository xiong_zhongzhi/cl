package com.cl.handler.event.annotation;

import java.lang.reflect.Method;

public class WorkflowBeen {

    //节点方法对象
    private Method method;

    //节点类对象
    private Class classObject;

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Class getClassObject() {
        return classObject;
    }

    public void setClassObject(Class classObject) {
        this.classObject = classObject;
    }

    public WorkflowBeen(Method method, Class classObject) {
        this.method = method;
        this.classObject = classObject;
    }
}
