package com.cl.handler.event.coreCode;
import com.cl.handler.event.annotation.WorkflowBeen;
import com.cl.handler.event.annotation.WorkflowHandler;
import com.cl.util.MyExceptionIncomplete;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: eventHandler
 * @Description: TODO(事件处理类) 1:事件注册类的监听
 * 
 * @author 564715118
 * @date 2020年1月2日
 */
public class WorkflowDispose {

	/**
	 * @Title: runWorkflow TODO(流执行器 )
	 * @return List<Container>
	 * @throws Exception
	 * @throws Throwable
	 */
	public static void runWorkflow(String subordinate,List<String> EventList,Object ... objs){
		System.out.println("执行流程 : "+EventList);
		Map<String, WorkflowBeen> workflowBeenMap =  WorkflowHandler.workflowMap.get(subordinate);
		// 查询到工作流下的当前 节点运行顺序
		for (String meaning : EventList) {
			WorkflowBeen workflowBeen = workflowBeenMap.get(meaning);
			try {
				// 根据 节点路径反射
				Object o = workflowBeen.getClassObject().newInstance();
				Method method = workflowBeen.getMethod();
				method.invoke(o,objs);
			} catch (Exception e) {
				Throwable t = ((InvocationTargetException) e).getTargetException();
				if (t instanceof MyExceptionIncomplete) {
					throw new MyExceptionIncomplete(((MyExceptionIncomplete) t).getMessage());
				} else {
					throw new MyExceptionIncomplete("工作流执行失败 : 提示 : 1:可能是项目结构变更导致,热加载在数据库中的流节点相对路径与实际不符合导致报此错 ,url : "
							+ meaning, e);
				}
			}
		}
	}

}
