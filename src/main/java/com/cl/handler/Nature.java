package com.cl.handler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
* @ClassName: MessageHandling
* @Description: TODO(对象属性描述注解)
* @author 沉思有顷:564715118
* @date 2020年10月27日
*/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.TYPE})
public @interface Nature {

	public String value();//属性描述
	
}
