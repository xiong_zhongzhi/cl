package com.cl.instructlibrary.been;

import java.math.BigDecimal;

public class SkillUse {

    //技能id
    private Integer id;

    //度数
    private BigDecimal angle;


    //模型核心点坐标
    private int x;

    private int y;

    //目标对象id
    private String userId;


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getAngle() {
        return angle;
    }

    public void setAngle(BigDecimal angle) {
        this.angle = angle;
    }

    public SkillUse(Integer id) {
        this.id = id;
    }
}
