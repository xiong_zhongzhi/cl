package com.cl.instructlibrary.been;


public enum TaskType {
    加入游戏("加入游戏"),
    退出游戏("退出游戏"),
    移动("移动"),
    地图初始化("地图初始化"),
    技能列表渲染("技能列表渲染"),
    蓝图("蓝图"),
    执行技能("执行技能"),
    技能图像渲染("技能图像渲染"),
    技能图像删除("技能图像删除"),
    技能cd("技能cd"),
    角色属性值变更("角色属性值变更"),
    断线重连("断线重连");

    private String displayName;

    TaskType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}

