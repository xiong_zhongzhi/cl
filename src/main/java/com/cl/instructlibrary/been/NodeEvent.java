package com.cl.instructlibrary.been;

import java.util.List;

/**
 * 负责调用链节点
 */
public class NodeEvent {
    public NodeEvent(TaskType taskType, List<TaskType> list) {
        this.taskType = taskType;
        this.list = list;
    }

    private TaskType taskType;

    private List<TaskType> list;

    public TaskType getTaskType() {
        return taskType;
    }

    public void setTaskType(TaskType taskType) {
        this.taskType = taskType;
    }

    public List<TaskType> getList() {
        return list;
    }

    public void setList(List<TaskType> list) {
        this.list = list;
    }
}
