package com.cl.instructlibrary.been;
import java.math.BigDecimal;

public class ImgBeen {

    /**
     *图片id
     */
    private String id;

    /**
     *操作名称
     */

    private String name;

    /**
     *图片路径
     */
    private String imgUrl;

    /**
     *宽
     */
    private Integer width;

    /**
     *长
     */
    private Integer length;

    /**
     *中心坐标x
     */
    private Integer centralPointX;

    /**
     *中心坐标y
     */
    private Integer centralPointY;

    /**
     *绝对坐标x
     */
    private BigDecimal absoluteX;

    /**
     *绝对坐标y
     */
    private BigDecimal absoluteY;

    private Boolean showFlag = true;

    private Boolean removeFlag = true;

    public Boolean getShowFlag() {
        return showFlag;
    }

    public void setShowFlag(Boolean showFlag) {
        this.showFlag = showFlag;
    }

    public Boolean getRemoveFlag() {
        return removeFlag;
    }

    public void setRemoveFlag(Boolean removeFlag) {
        this.removeFlag = removeFlag;
    }

    /**
     *0自身碰撞 1友方碰撞 2敌方碰撞
     */
    private Integer crashFlag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getCentralPointX() {
        return centralPointX;
    }

    public void setCentralPointX(Integer centralPointX) {
        this.centralPointX = centralPointX;
    }

    public Integer getCentralPointY() {
        return centralPointY;
    }

    public void setCentralPointY(Integer centralPointY) {
        this.centralPointY = centralPointY;
    }

    public Integer getCrashFlag() {
        return crashFlag;
    }

    public void setCrashFlag(Integer crashFlag) {
        this.crashFlag = crashFlag;
    }

    public BigDecimal getAbsoluteX() {
        return absoluteX;
    }

    public void setAbsoluteX(BigDecimal absoluteX) {
        this.absoluteX = absoluteX;
    }

    public BigDecimal getAbsoluteY() {
        return absoluteY;
    }

    public void setAbsoluteY(BigDecimal absoluteY) {
        this.absoluteY = absoluteY;
    }
}
