package com.cl.instructlibrary.been;


import com.cl.api.gameRole.dto.GameRoleUserDTO;
import com.cl.api.user.dto.UserDTO;
import com.cl.handler.Nature;

import java.time.LocalDateTime;


/**
 * 用户处于地图中时的个人信息缓存对象
 *
 * @author csyq
 * @date 2023-05-25
 */

public class MapInUser {

    /**
     * 房间id
     */
    @Nature("房间id")
    private Long roomMapId;

    /**
     * x轴
     */
    @Nature("x轴")
    private Integer x;

    /**
     * y轴
     */
    @Nature("y轴")
    private Integer y;

    /**
     * 用户信息
     */
    @Nature("用户信息")
    private UserDTO userDTO;

    /**
     * 角色信息
     */
    @Nature("角色信息")
    private GameRoleUserDTO gameRoleUserDTO;


    /**
     * 创建时间
     */
    @Nature("创建时间")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @Nature("修改时间")
    private LocalDateTime updateTime;

    public Long getRoomMapId() {
        return roomMapId;
    }

    public void setRoomMapId(Long roomMapId) {
        this.roomMapId = roomMapId;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public GameRoleUserDTO getGameRoleUserDTO() {
        return gameRoleUserDTO;
    }

    public void setGameRoleUserDTO(GameRoleUserDTO gameRoleUserDTO) {
        this.gameRoleUserDTO = gameRoleUserDTO;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}