package com.cl.instructlibrary.been;


import com.cl.api.room.dto.RoomMapDTO;
import com.cl.api.skill.dto.SkillDTO;

import java.util.*;

public class Informatization {

    //新加入的用户
    private List<MapInUser> addMapInUser = new ArrayList<>();

    //属性值变更的用户
    private List<MapInUser> alterationInUserList = new ArrayList<>();

    //退出的用户
    private List<MapInUser> removeMapInUser = new ArrayList<>();

    //移动的用户
    private List<MapInUser> moveMapInUser = new ArrayList<>();

    //事件通知
    private Set<Integer> taskList = new HashSet();

    //技能图像集合
    private List<ImgBeen> imgBeenList = new ArrayList<>();

    //技能cd列表
    private List<SkillCd> skillCdList = new ArrayList<>();

    public List<SkillCd> getSkillCdList() {
        return skillCdList;
    }

    public List<MapInUser> getAlterationInUserList() {
        return alterationInUserList;
    }

    public void setAlterationInUserList(List<MapInUser> alterationInUserList) {
        this.alterationInUserList = alterationInUserList;
    }

    public void setSkillCdList(List<SkillCd> skillCdList) {
        this.skillCdList = skillCdList;
    }

    public List<ImgBeen> getImgBeenList() {
        return imgBeenList;
    }

    public void setImgBeenList(List<ImgBeen> imgBeenList) {
        this.imgBeenList = imgBeenList;
    }

    public Set<Integer> getTaskList() {
        return taskList;
    }

    public void setTaskList(Set<Integer> taskList) {
        this.taskList = taskList;
    }

    public List<MapInUser> getMoveMapInUser() {
        return moveMapInUser;
    }

    public void setMoveMapInUser(List<MapInUser> moveMapInUser) {
        this.moveMapInUser = moveMapInUser;
    }

    public List<MapInUser> getRemoveMapInUser() {
        return removeMapInUser;
    }

    public void setRemoveMapInUser(List<MapInUser> removeMapInUser) {
        this.removeMapInUser = removeMapInUser;
    }

    public List<MapInUser> getAddMapInUser() {
        return addMapInUser;
    }

    public void setAddMapInUser(List<MapInUser> addMapInUser) {
        this.addMapInUser = addMapInUser;
    }

    //房间信息
    private RoomDTOExtend roomDTOExtend;

    //全地图元素
    private List<RoomMapDTO> roomMapDTOList;

    //全地图成员信息
    private Map<String, MapInUser> mapInUserMap;

    //技能列表
    private List<SkillDTO> skillDTOList;

    public List<SkillDTO> getSkillDTOList() {
        return skillDTOList;
    }

    public void setSkillDTOList(List<SkillDTO> skillDTOList) {
        this.skillDTOList = skillDTOList;
    }

    public RoomDTOExtend getRoomDTOExtend() {
        return roomDTOExtend;
    }

    public void setRoomDTOExtend(RoomDTOExtend roomDTOExtend) {
        this.roomDTOExtend = roomDTOExtend;
    }

    public List<RoomMapDTO> getRoomMapDTOList() {
        return roomMapDTOList;
    }

    public void setRoomMapDTOList(List<RoomMapDTO> roomMapDTOList) {
        this.roomMapDTOList = roomMapDTOList;
    }

    public Map<String, MapInUser> getMapInUserMap() {
        return mapInUserMap;
    }

    public void setMapInUserMap(Map<String, MapInUser> mapInUserMap) {
        this.mapInUserMap = mapInUserMap;
    }
}
