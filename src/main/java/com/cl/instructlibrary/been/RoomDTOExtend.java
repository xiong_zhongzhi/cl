package com.cl.instructlibrary.been;

import java.time.LocalDateTime;

/**
 * 描述：房间信息DTO
 *
 * @author csyq
 * @date 2023-05-24
 */
public class RoomDTOExtend {

    /**
     * 房间id
     */
    private Long roomId;

    /**
     * 房主id
     */
    private String userId;

    /**
     * 房间名称
     */
    private String roomName;

    /**
     * 房间编号
     */
    private String roomNumber;

    /**
     * 房间类型
     */
    private String roomType;

    /**
     * 房间密码
     */
    private String roomPassword;

    /**
     * 房间最大参与人数
     */
    private Integer personSum;

    /**
     * 是否删除1未删除 2已删除
     */
    private Integer delFlag;

    /**
     * 是否公开1公开 2私有
     */
    private Integer roomPublic;

    /**
     * 审核状态1待审核 2审核通过 3审核驳回
     */
    private Integer checkState;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 审核时间
     */
    private LocalDateTime checkTime;

    /**
     * 地图高度
     */
    private Integer high;

    /**
     * 地图宽度
     */
    private Integer width;

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getRoomPassword() {
        return roomPassword;
    }

    public void setRoomPassword(String roomPassword) {
        this.roomPassword = roomPassword;
    }

    public Integer getPersonSum() {
        return personSum;
    }

    public void setPersonSum(Integer personSum) {
        this.personSum = personSum;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Integer getRoomPublic() {
        return roomPublic;
    }

    public void setRoomPublic(Integer roomPublic) {
        this.roomPublic = roomPublic;
    }

    public Integer getCheckState() {
        return checkState;
    }

    public void setCheckState(Integer checkState) {
        this.checkState = checkState;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(LocalDateTime checkTime) {
        this.checkTime = checkTime;
    }

    public Integer getHigh() {
        return high;
    }

    public void setHigh(Integer high) {
        this.high = high;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }
}