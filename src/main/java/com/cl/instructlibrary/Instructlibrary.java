package com.cl.instructlibrary;

import com.cl.awt.js.Hint;
import com.cl.awt.js.Js;
import com.cl.awt.page.play.PlaYGame;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.event.annotation.Workflow;
import com.cl.handler.event.coreCode.WorkflowDispose;
import com.cl.handler.messageHandling.MessageHandling;
import com.cl.instructlibrary.been.Informatization;
import com.cl.instructlibrary.been.NodeEvent;
import com.cl.instructlibrary.been.TaskType;
import com.cl.log.OutPut;
import com.cl.util.MyExceptionIncomplete;

import java.io.IOException;
import java.util.*;

/**
 * @author 沉思有顷:564715118
 * @ClassName: Instructlibrary
 * @Description: TODO(指令库)
 * @date 2020年10月27日
 */
@RulesOfCompetency
public class Instructlibrary {

    @MessageHandling(value = "链接成功")
    public void Instructlibrary1() throws IOException {
        OutPut.out("与服务端链接成功 ");
    }

    @MessageHandling(value = "无效指令")
    public void Instructlibrary2(String text) throws IOException {
        OutPut.out("text : " + text);
    }

    @MessageHandling(value = "reex")
    public void Instructlibrary3(String text) throws IOException {
        Js.drawingBoard.myPage.rollbackExecute();
        new Hint().run(-1, text);
//        //重新匹配
//        if (Js.drawingBoard.myPage instanceof PlaYGame) {//确认当前处于游戏页
//            PlaYGame plaYGame = (PlaYGame) Js.drawingBoard.myPage;
//            plaYGame.loadModel();
//        } else {
//            throw new MyExceptionIncomplete("您当前不处于位面中");
//        }
    }

    //固定调用链
    public List<NodeEvent> taskTypeList = new ArrayList() {
        {
            add(new NodeEvent(TaskType.地图初始化, Arrays.asList(TaskType.加入游戏, TaskType.移动, TaskType.退出游戏)));
            add(new NodeEvent(TaskType.断线重连,Arrays.asList(TaskType.加入游戏, TaskType.移动, TaskType.退出游戏)));
            add(new NodeEvent(TaskType.技能列表渲染, Arrays.asList()));
            add(new NodeEvent(TaskType.加入游戏, Arrays.asList()));
            add(new NodeEvent(TaskType.移动, Arrays.asList()));
            add(new NodeEvent(TaskType.退出游戏, Arrays.asList()));
            add(new NodeEvent(TaskType.技能图像渲染, Arrays.asList()));
            add(new NodeEvent(TaskType.技能图像删除, Arrays.asList()));
            add(new NodeEvent(TaskType.技能cd, Arrays.asList()));
            add(new NodeEvent(TaskType.角色属性值变更, Arrays.asList()));


        }
    };

    @MessageHandling(value = "逻辑帧变更数据")
    public void Instructlibrary4(Informatization informatization, Informatization privateInformatization) throws IOException {
        //处理用户调用链
        Set<Integer> TaskList = new HashSet<>();
        if (informatization != null) {
            TaskList.addAll(informatization.getTaskList());
        }
        if (privateInformatization != null) {
            TaskList.addAll(privateInformatization.getTaskList());
        }
        if (TaskList.size() == 0) {
            return;
        }
        //玩家实际调用链
        List<String> taskTypeLinkd = new ArrayList<>();
        taskTypeList.stream()
                .filter(task -> TaskList.contains(task.getTaskType().ordinal()))
                .forEach(task -> {
                    for (TaskType taskType : task.getList()) {
                        TaskList.remove(taskType.ordinal());
                    }
                    taskTypeLinkd.add(TaskType.values()[task.getTaskType().ordinal()].name());
                });
        //启用工作流
        if (Js.drawingBoard.myPage instanceof PlaYGame) {//确认当前处于游戏页
            PlaYGame plaYGame = (PlaYGame) Js.drawingBoard.myPage;
            WorkflowDispose.runWorkflow("游戏位面",taskTypeLinkd,plaYGame,informatization,privateInformatization);
        } else {
            throw new MyExceptionIncomplete("您当前不处于位面中");
        }
    }

    @MessageHandling(value = "使用无效技能")
    public void Instructlibrary5() throws IOException {
        OutPut.out("使用无效技能");
    }

    @Workflow(meaning = "地图初始化", subordinate = {"游戏位面"})
    public void luc1(PlaYGame plaYGame, Informatization informatization, Informatization privateInformatization) {
        plaYGame.renderer(privateInformatization.getRoomMapDTOList(), privateInformatization.getRoomDTOExtend(), privateInformatization.getMapInUserMap());
    }

    @Workflow(meaning = "断线重连", subordinate = {"游戏位面"})
    public void luc10(PlaYGame plaYGame, Informatization informatization, Informatization privateInformatization) {
        plaYGame.renderer(privateInformatization.getRoomMapDTOList(), privateInformatization.getRoomDTOExtend(), privateInformatization.getMapInUserMap());
    }

    @Workflow(meaning = "加入游戏", subordinate = {"游戏位面"})
    public void luc2(PlaYGame plaYGame, Informatization informatization, Informatization privateInformatization) {
        plaYGame.addListNewUser(informatization.getAddMapInUser());
    }

    @Workflow(meaning = "移动", subordinate = {"游戏位面"})
    public void luc3(PlaYGame plaYGame, Informatization informatization, Informatization privateInformatization) {
        plaYGame.userMove(informatization.getMoveMapInUser());
    }

    @Workflow(meaning = "退出游戏", subordinate = {"游戏位面"})
    public void luc4(PlaYGame plaYGame, Informatization informatization, Informatization privateInformatization) {
        plaYGame.userQuitGame(informatization.getRemoveMapInUser());
    }

    @Workflow(meaning = "技能列表渲染", subordinate = {"游戏位面"})
    public void luc5(PlaYGame plaYGame, Informatization informatization, Informatization privateInformatization) {
        plaYGame.showSkill(privateInformatization.getSkillDTOList());
    }

    @Workflow(meaning = "技能图像渲染", subordinate = {"游戏位面"})
    public void luc6(PlaYGame plaYGame, Informatization informatization, Informatization privateInformatization) {
        plaYGame.showImg(informatization.getImgBeenList());
    }

    @Workflow(meaning = "技能图像删除", subordinate = {"游戏位面"})
    public void luc7(PlaYGame plaYGame, Informatization informatization, Informatization privateInformatization) {
        plaYGame.removeImg(informatization.getImgBeenList());
    }

    @Workflow(meaning = "技能cd", subordinate = {"游戏位面"})
    public void luc8(PlaYGame plaYGame, Informatization informatization, Informatization privateInformatization) {
        plaYGame.skillCd(privateInformatization.getSkillCdList());
    }

    @Workflow(meaning = "角色属性值变更", subordinate = {"游戏位面"})
    public void luc9(PlaYGame plaYGame, Informatization informatization, Informatization privateInformatization) {
        plaYGame.alterationInUser(informatization.getAlterationInUserList());
    }

    @Workflow(meaning = "角色死亡", subordinate = {"游戏位面"})
    public void luc11(PlaYGame plaYGame, Informatization informatization, Informatization privateInformatization) {
        //plaYGame.gameOver();
    }

}