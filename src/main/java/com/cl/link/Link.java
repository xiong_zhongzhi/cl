package com.cl.link;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

import com.alibaba.fastjson.JSON;
import com.cl.been.LinkFlag;
import com.cl.been.Msg;
import com.cl.handler.Jurisdiction.Drawer;
import com.cl.handler.messageHandling.MessageHandlingHandler;
import com.cl.log.OutPut;
import com.cl.util.MyExceptionIncomplete;

public class Link {

	public static Link link;
	
	private String ip;

	private Integer port;
	
	public Link(String ip, Integer port) {
		super();
		this.ip = ip;
		this.port = port;
	}

	// 通道管理器(Selector)
	private Selector selector;
	private SocketChannel channel;
	private LinkFlag linkFlag;



	public void info() throws Exception {
		new Thread(() -> {
			try {
				link();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		link = this;
	}

	public void link() throws Exception {
		while (true) {
			linkFlag = new LinkFlag(false);
			selector = Selector.open();
			channel = SocketChannel.open();
			channel.configureBlocking(false);
			channel.connect(new InetSocketAddress(ip, port));
			channel.register(selector, SelectionKey.OP_CONNECT);
			while (true) {
				selector.select();
				// 获取监听事件
				Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
				// 迭代处理
				while (iterator.hasNext()) {
					// 获取事件
					SelectionKey key = iterator.next();
					// 移除事件，避免重复处理
					iterator.remove();
					// 检查是否是一个就绪的已经连接服务端成功事件
					if (key.isConnectable()) {
						handleConnect(key);
					} else if (key.isReadable()) {// 检查套接字是否已经准备好读数据
						handleRead(key);
					}
					if (linkFlag.flag) {
						key.cancel();
						key.channel().close();
					}
				}
				if (linkFlag.flag) {
					break;
				}
			}
			try {
				if (selector != null) {
					selector.close();
				}
				if (channel != null) {
					channel.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (linkFlag.flag) {
				System.out.println("与服务器断开链接");
			}
		}
	}

	/**
	 * 处理客户端连接服务端成功事件
	 * 
	 * @throws Exception
	 */
	private void handleConnect(SelectionKey key) throws Exception {
		try {
			// 获取与服务端建立连接的通道
			SocketChannel channel = (SocketChannel) key.channel();
			if (channel.isConnectionPending()) {
				channel.finishConnect();
			}
			channel.configureBlocking(false);
			// 通道注册到选择器，并且这个通道只对读事件感兴趣
			channel.register(selector, SelectionKey.OP_READ);
		} catch (ConnectException e) {
			// 处理无法链接服务器的问题
			linkFlag.flag = true;
			linkFlag.msg = "正在尝试链接服务器...";
		}
	}

	/**
	 * 监听到读事件，读取服务端发送过来的消息
	 * 
	 * @throws Exception
	 */
	private void handleRead(SelectionKey key) throws Exception {

		SocketChannel channel = (SocketChannel) key.channel();
		// 从通道读取数据到缓冲区
		ByteBuffer buffer = ByteBuffer.allocate(128);
		String value = "";
		try {
			int i = channel.read(buffer);
			if (i == -1) {
				linkFlag.flag = true;
				linkFlag.msg = "链接已自动断开";
			}

			byte[] temp = new byte[0];
			while (i > 0) {
				byte[] bs = new byte[i];// 用来存放读取的内容的数组
				buffer.flip();
				buffer.get(bs);
				buffer.clear();
				byte[] toTemp = new byte[temp.length + bs.length];
				// src - 源数组。srcPos - 源数组中的起始位置。dest - 目标数组。destPos - 目的地数据中的起始位置。length -
				// 要复制的源数组元素的数量。
				System.arraycopy(temp, 0, toTemp, 0, temp.length);
				System.arraycopy(bs, 0, toTemp, temp.length, bs.length);
				temp = toTemp;
				i = channel.read(buffer);
			}
			if (temp == null || temp.length == 0) {
				throw new MyExceptionIncomplete("无输入数据");
			}
			value= new String(temp, 0, temp.length, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
			linkFlag.flag = true;
			linkFlag.msg = "远程主机强迫关闭了一个现有的连接";
			return;
		}
		//将服务器发送来的消息发送给消息处理器
		dataHandler(value);
	}

	public void dataHandler(String value) throws Exception {
		if (value.length()>200){
			OutPut.out("接收报文 ： " + value);
		}
		new Thread(() -> {
			try {
				Msg msg = JSON.toJavaObject(JSON.parseObject(value), Msg.class);
				Drawer.get(MessageHandlingHandler.class).run(msg);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
	}

	public void write(Msg msg){
		System.out.println("发送报文 ： " + JSON.toJSONString(msg));
		try {
			channel.write(ByteBuffer.wrap(JSON.toJSONString(msg).getBytes()));
		}catch (Exception e){
			throw new MyExceptionIncomplete(e);
		}

	}
}
