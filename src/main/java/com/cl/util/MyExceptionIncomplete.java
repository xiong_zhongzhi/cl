package com.cl.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
* @ClassName: MyExceptionIncomplete
* @Description:TODO(返回异常信息的类)
* @author: 沉思有顷 564715118
* @date: 2021年5月3日 上午10:47:42
* @Copyright:
*/
@SuppressWarnings("serial")
public class MyExceptionIncomplete extends RuntimeException {
	Object object = "";

	public MyExceptionIncomplete(Object object) {
		this.object = object;
	}
	
	public MyExceptionIncomplete(Exception e) {
		this.object =log(e); 
	}
	
	public MyExceptionIncomplete(Object object,Exception e) {
		this.object =object.toString()+" : "+log(e); 
	}

	@Override
	public String getMessage() {
		return "" + this.object;
	}

	public String log(Exception exception) {
		StringWriter sw = null;
		PrintWriter pw = null;
		try {
			sw = new StringWriter();
			pw = new PrintWriter(sw);
			exception.printStackTrace(pw);
			String msg = sw.toString();
			return msg;
		} finally {
			if (pw != null) {
				pw.close();
			}
			if (sw != null) {
				try {
					sw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
