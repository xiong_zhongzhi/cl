package com.cl.util;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

public enum EnumTime {
    millisecond("yyyy-MM-dd HH:mm:ss SSS"), second("yyyy-MM-dd HH:mm:ss"), minute("yyyy-MM-dd HH:mm"),
    hou("yyyy-MM-dd HH"), day("yyyy-MM-dd"), month("yyyy-MM"), year("yyyy");

    private String description;
    private DateTimeFormatter format;

    EnumTime(String description) {
        this.description = description;
        format = new DateTimeFormatterBuilder().appendPattern(description).parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0).parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
                .toFormatter();
    }

    /**
     * @Title:getTime TODO(获取当前时间) @return @return: String 当前时间 @throws
     */
    public String getTime() {
        return LocalDateTime.now().format(format);
    }

    public String getTime(LocalDateTime localDateTime) {
        return localDateTime.format(format);
    }

    public LocalDateTime getLocalDateTime() {
        return LocalDateTime.now();
    }

    public LocalDateTime getLocalDateTime(String time) {
        LocalDateTime timeDate = null;
        try {
            timeDate = LocalDateTime.parse(time, format);
        } catch (Exception e) {
            throw new MyExceptionIncomplete("输入是时间 : " + time + " ,时间格式不符合系统统一时间格式 : " + description);
        }
        return timeDate;
    }

    /**
     * 获取当前时间最低单位的值
     */
    public Integer getInTime() {
        String time = EnumTime.millisecond.getTime();
        return getInTime(time);
    }

    /**
     * 获取当前时间最低单位的值
     */
    public Integer getInTime(String time) {
        char[] chars = time.toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        switch (this) {
            case millisecond:
                return Integer.parseInt(stringBuilder.append(chars[20]).append(chars[21]).append(chars[22]).toString());
            case second:
                return Integer.parseInt(stringBuilder.append(chars[17]).append(chars[18]).toString());
            case minute:
                return Integer.parseInt(stringBuilder.append(chars[14]).append(chars[15]).toString());
            case hou:
                return Integer.parseInt(stringBuilder.append(chars[11]).append(chars[12]).toString());
            case day:
                return Integer.parseInt(stringBuilder.append(chars[8]).append(chars[9]).toString());
            case month:
                return Integer.parseInt(stringBuilder.append(chars[5]).append(chars[6]).toString());
            case year:
                return Integer.parseInt(
                        stringBuilder.append(chars[0]).append(chars[1]).append(chars[2]).append(chars[3]).toString());
            default:
                throw new MyExceptionIncomplete("未配置对应时间格式");
        }
    }

    /**
     * 字符串时间补位
     */
    public String getSupplementTime(String time) {
        char[] chars = time.toCharArray();
        switch (this) {
            case year:
                chars[5] = '0';
                chars[6] = '0';
            case month:
                chars[8] = '0';
                chars[9] = '0';
            case day:
                chars[11] = '0';
                chars[12] = '0';
            case hou:
                chars[14] = '0';
                chars[15] = '0';
            case minute:
                chars[17] = '0';
                chars[18] = '0';
            case second:
                chars[20] = '0';
                chars[21] = '0';
                chars[22] = '0';
                break;
            default:
                throw new MyExceptionIncomplete("未配置对应时间格式");
        }
        return new String(chars);
    }


    /**
     * 计算与当前相差时间
     *
     * @param time 当前时间
     * @return
     */
    public String dateGap(String time) {
        LocalDateTime timeDate = null;
        try {
            timeDate = LocalDateTime.parse(time, format);
        } catch (Exception e) {
            throw new MyExceptionIncomplete("输入是时间 : " + time + " ,时间格式不符合系统统一时间格式 : " + description);
        }
        return dateGap(timeDate);
    }

    /**
     * 计算与当前相差时间
     *
     * @param endTime 结束时间
     * @return
     */
    public String dateGap(LocalDateTime endTime) {
        return dateGap(LocalDateTime.now(), endTime);
    }

    /**
     * 计算与当前相差时间
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return
     */
    public String dateGap(LocalDateTime startTime, LocalDateTime endTime) {
        switch (this) {
            case millisecond:
                return String.valueOf(Duration.between(startTime, endTime).toMillis());
            case second:
                return String.valueOf(Duration.between(startTime, endTime).getSeconds());
            case minute:
                return String.valueOf(Duration.between(startTime, endTime).toMinutes());
            case hou:
                return String.valueOf(Duration.between(startTime, endTime).toHours());
            case day:
                return String.valueOf(Period.between(startTime.toLocalDate(), endTime.toLocalDate()).getDays());
            case month:
                return String.valueOf(Period.between(startTime.toLocalDate(), endTime.toLocalDate()).getMonths());
            case year:
                return String.valueOf(Period.between(startTime.toLocalDate(), endTime.toLocalDate()).getYears());
        }
        return null;
    }

    /**
     * 将日期加减去指定时间
     *
     * @param targetTime
     * @param dateLong
     * @return
     */
    public String dateSub(LocalDateTime targetTime, int dateLong) {
        return dateSubLocalDateTime(targetTime, dateLong).format(format);
    }

    /**
     * 将日期加减去指定数值
     *
     * @param targetTime
     * @param dateLong
     * @return
     */
    public String dateSubMillisecond(LocalDateTime targetTime, int dateLong) {
        return dateSubLocalDateTime(targetTime, dateLong).format(EnumTime.millisecond.format);
    }


    /**
     * 将日期加减去指定时间
     *
     * @param targetTime
     * @param dateLong
     * @return
     */
    public LocalDateTime dateSubLocalDateTime(LocalDateTime targetTime, int dateLong) {
        switch (this) {
            case second:
                return targetTime.plusSeconds(dateLong);
            case minute:
                return targetTime.plusMinutes(dateLong);
            case hou:
                return targetTime.plusHours(dateLong);
            case day:
                return targetTime.plusDays(dateLong);
            case month:
                return targetTime.plusMonths(dateLong);
            case year:
                return targetTime.plusYears(dateLong);
            default:
                throw new MyExceptionIncomplete("未处理此类型");
        }
    }

    /**
     * 将日期加减去指定时间
     *
     * @param dateLong
     * @return
     */
    public LocalDateTime dateSubLocalDateTime(int dateLong) {
        return dateSubLocalDateTime(LocalDateTime.now(), dateLong);
    }


    /**
     * 将日期加减去指定秒数
     *
     * @param targetTime
     * @param dateLong
     * @return
     */
    public String dateSubMillisecond(String targetTime, int dateLong) {
        LocalDateTime timeDate = null;
        try {
            timeDate = LocalDateTime.parse(targetTime, format);
        } catch (Exception e) {
            throw new MyExceptionIncomplete("输入是时间 : " + targetTime + " ,时间格式不符合系统统一时间格式 : " + description);
        }
        return dateSubMillisecond(timeDate, dateLong);
    }

    public String dateSub(String targetTime, int dateLong) {
        LocalDateTime timeDate = null;
        try {
            timeDate = LocalDateTime.parse(targetTime, format);
        } catch (Exception e) {
            throw new MyExceptionIncomplete("输入是时间 : " + targetTime + " ,时间格式不符合系统统一时间格式 : " + description);
        }
        return dateSub(timeDate, dateLong);
    }

    /**
     * 校验时间格式
     *
     * @param str 需要效验的时间
     * @return true : 是对应的时间格式 false : 不是对应的时间格式
     */
    public boolean isValidDate(String str) {
        try {
            LocalDateTime.parse(str, format);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
