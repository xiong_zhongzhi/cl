package com.cl.util;

import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.UriTemplateHandler;

import java.net.URI;
import java.util.Map;

public class GetUriTemplateHandler implements UriTemplateHandler {

    private UriTemplateHandler uriTemplateHandler = new DefaultUriBuilderFactory();

    @Override
    public URI expand(String uriTemplate, Map<String, ?> uriVariables) {
        return uriTemplateHandler.expand(uriTemplate, uriVariables);
    }

    @Override
    public URI expand(String uriTemplate, Object... uriVariables) {
        return uriTemplateHandler.expand(uriTemplate, uriVariables);

    }

}