package com.cl.util;

/**
 * @ClassName: AllPowerful
 * @Description: TODO(万能钩子)
 * @author 沉思有顷:564715118
 * @date 2020年11月9日
 */
public abstract class AllPowerful<S, S1> {
	// 提供项目模块中的通用钩子接口
	public S1 hook(S s) {
		return null;
	};

	public S1 hook(){
		return null;
	};
}
