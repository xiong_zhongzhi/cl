package com.cl.util.hotLoading;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MyClassLoad extends ClassLoader {
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        StringBuilder sb = new StringBuilder(name.length() + 6);
        sb.append(name.replace('.', '/')).append(".class");
        /*加载我们target/classes下的class*/
        InputStream is = getResourceAsStream(sb.toString());
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int len;
            while ((len = is.read(buf)) >= 0) {
                baos.write(buf, 0, len);
            }
            buf = baos.toByteArray();
            return this.defineClass(name, buf, 0, buf.length);
        } catch (Exception e) {
            throw new RuntimeException("自定义类加载", e);
        } finally {
            try {
                is.close();
            } catch (IOException ioe) {
                //ignore
            }
        }

    }
}