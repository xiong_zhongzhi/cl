package com.cl.util.hotLoading;

import java.lang.reflect.Method;

public class Factory {
    /**
     * 原生的类加载器进行加载
     */
//    public static void getObject() {
//        try {
//            Class<?> dzl = Factory.class.getClassLoader().loadClass("com.cl.util.hotLoading.Dzl");
//            Object o = dzl.newInstance();
//            Method print = dzl.getDeclaredMethod("print", null);
//            print.invoke(o, null);
//        } catch (Exception e) {
//            throw new RuntimeException("类加载异常", e);
//        }
//    }

    /**
     * 自定义的类加载器进行加载
     */
    public static void getObjectNew(String name) {
        try {
            /*每次加载都创建一个新的类加载器*/
            MyClassLoad classLoad = new MyClassLoad();
            classLoad.findClass(name);
        } catch (Exception e) {
            throw new RuntimeException("类加载异常", e);
        }
    }
}