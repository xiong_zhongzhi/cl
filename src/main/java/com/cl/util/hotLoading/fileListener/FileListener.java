package com.cl.util.hotLoading.fileListener;

import com.cl.awt.Client;
import com.cl.awt.core.MyPage;
import com.cl.awt.core.PageCache;
import com.cl.awt.core.Route;
import com.cl.awt.js.Js;
import com.cl.been.Container;
import com.cl.handler.Jurisdiction.Drawer;
import com.cl.handler.page.PageUrl;
import com.cl.handler.page.PageUrlHandler;
import com.cl.log.OutPut;
import com.cl.util.MyExceptionIncomplete;
import com.cl.util.hotLoading.Factory;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;

import javax.annotation.Resource;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FileListener extends FileAlterationListenerAdaptor {
 
    // 文件创建执行
    @Override
    public void onFileCreate(File file) {

        String name = file.getPath();
        int index = name.indexOf("$");
        if (index!=-1){
            return;
        }
        name = name.replaceAll("\\\\",".");
        name = name.replaceAll(".class","");
        name = name.replaceAll("targetes.","");
        Factory.getObjectNew(name);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //处理页面class专属内容
        {
            PageUrlHandler pageUrlHandler = Drawer.get(PageUrlHandler.class);
            Class<?> clazz=null;
            try {
                clazz=Class.forName(name);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            PageUrl pageUrl =clazz.getAnnotation(PageUrl.class);
            Route route =pageUrlHandler.routeMap.get(pageUrl.url());
            MyPage myPage = route.getPage();
            if (myPage ==null){
                return;
            }
            if (myPage != Js.drawingBoard.myPage){
                //删除当前的page对象
                route.setPage(null);
                return;
            }
            if (myPage == Js.drawingBoard.myPage){
                //重载当前页面
                route.setPage(null);

                //当前页缓存清空
                if(Js.drawingBoard.myPage != null) {
                    Js.drawingBoard.myPage.remove();
                }
                // 进行页面跳转
                myPage.remove();
                MyPage restartPage =  restartPage(route);
                pageUrlHandler.linkRoute.remove(pageUrlHandler.linkRoute.size()-1);
                pageUrlHandler.linkRoute.add(restartPage);
                restartPage.run(myPage.data);
                Js.drawingBoard.myPage = restartPage;
                OutPut.out("热加载完成");
            }
        }


    }

    private MyPage restartPage(Route route){
        // 加载page对象
        MyPage myPage = route.getPage();
        if (myPage == null) {
            try {
                Class<?> clazz = Class.forName(route.getUrl());
                Object object = clazz.newInstance();
                //注入
                // 获取实体类的所有属性，返回Field数组
                Field[] fields = clazz.getDeclaredFields();
                for(Field field:fields) {
                    Resource resource = field.getAnnotation(Resource.class);
                    if(resource == null) {
                        continue;
                    }
                    Object api =  Drawer.get(field.getType());
                    if(api != null) {
                        field.setAccessible(true);
                        field.set(object,api);
                    }
                }
                myPage = (MyPage) object;
                for (Method method : clazz.getMethods()) {
                    if (method.getName().equals("load")) {
                        myPage.load = method;
                        myPage.methodParamNameList = getMethodParamName(method);
                    }
                }
                //缓存
                myPage.pageCache = new PageCache();
                myPage.pageCache.setPageUrl(route.getPageUrl());
                MyPage.client = Client.getClient();
                myPage.drawingBoard = Client.getClient().drawingBoard;
                route.setPage(myPage);
            } catch (Exception e) {
                throw new MyExceptionIncomplete("@PageUrl[" + route.getUrl() + "]的路由对象,转换异常 :", e);
            }
        }
        return myPage;
    }

    /**
     * @Description: TODO(获取对象参数名称数组)
     * @param method       方法对象
     */
    public static String[] getMethodParamName(Method method) {
        try {
            LocalVariableTableParameterNameDiscoverer parameterNameDiscoverer = new LocalVariableTableParameterNameDiscoverer();

            return parameterNameDiscoverer.getParameterNames(method);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String[0];
    }

 
    // 文件创建修改
    @Override
    public void onFileChange(File file) {
       //System.out.println(file.getName()+"文件创建或者修改");
    }
 
    // 文件创建删除
    @Override
    public void onFileDelete(File file) {
    	//System.out.println(file.getName()+"文件创建删除");
    }
 
    // 目录创建
    @Override
    public void onDirectoryCreate(File directory) {
    }
 
    // 目录修改
    @Override
    public void onDirectoryChange(File directory) {
    }
 
    // 目录删除
    @Override
    public void onDirectoryDelete(File directory) {
    }
 
 
    // 轮询开始
    @Override
    public void onStart(FileAlterationObserver observer) {
    }
 
    // 轮询结束
    @Override
    public void onStop(FileAlterationObserver observer) {
    }
}
