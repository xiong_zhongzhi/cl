package com.cl.util.hotLoading.fileListener;

import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class FileListenerFactory {

	// 设置监听路径
	private final String monitorDir = "target/classes/com/cl/awt/page";

	// 设置轮询间隔
	private final long interval = TimeUnit.SECONDS.toSeconds(10);

	public FileAlterationMonitor getMonitor() {
		FileAlterationObserver observer = new FileAlterationObserver(new File(monitorDir));
		// 向监听者添加监听器，并注入业务服务
		observer.addListener(new FileListener());
		// 返回监听者
		return new FileAlterationMonitor(interval, observer);
	}
}