package com.cl.util.hotLoading.fileListener;

import org.apache.commons.io.monitor.FileAlterationMonitor;

public class FileListenerRunner extends Thread {

	@Override
    public void run(){	
        // 创建监听者
        FileAlterationMonitor fileAlterationMonitor = new FileListenerFactory().getMonitor();
        try {
            fileAlterationMonitor.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
