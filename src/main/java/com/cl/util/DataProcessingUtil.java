package com.cl.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cl.been.Container;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.BeanUtils;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class DataProcessingUtil {


	/**
	 * @MdoeName createFile TODO(集合内对象转换)
	 * @param objects 需要转换的集合
	 * @param type 需要转换成对象类型
	 * @return
	 * @throws Exception boolean
	 */
	public static <T> List<T> copyPropertiesList(List objects, Class<T> type) {
		if (objects == null || type == null) {
			return null;
		}
		List<T> desDataList = new ArrayList<>();
		for (Object object:objects) {
			try {
				T desData = type.newInstance();
				BeanUtils.copyProperties(object,desData);
				desDataList.add(desData);
			}catch (Exception e){
				throw new MyExceptionIncomplete("类对象间转换异常",e);
			}
		}
		return desDataList;
	}



	/**
	 * 其它工具方法
	 */
	/**
	 * @MdoeName getEncoding TODO(字符串字符集判断)
	 * @param str
	 * @return 对应字符集
	 * @throws Exception
	 */
	public static String getEncoding(String str) {
		List<Charset> encodes = new java.util.ArrayList<>();
		encodes.add(StandardCharsets.US_ASCII);
		//encodes.add(Charset.forName("GB2312")); //早期的中文编码  6763个
		//encodes.add(Charset.forName("GB18030")); //最新的中文编码 70244个  汉字收录范围包含繁体汉字以及日韩汉字
		encodes.add(StandardCharsets.ISO_8859_1);
		encodes.add(StandardCharsets.UTF_8);
		encodes.add(StandardCharsets.UTF_16BE);
		encodes.add(StandardCharsets.UTF_16LE);
		encodes.add(StandardCharsets.UTF_16);
		encodes.add(Charset.forName("GBK")); // 兼容gb2312 21886个
		for (Charset charset : encodes) {
			if (charset.newEncoder().canEncode(str)) {

				return charset.name();
			}
		}
		throw new MyExceptionIncomplete("无法判断字符集");
	}

	/**
	 * @MdoeName createFile TODO(放入文件地址,如果不存在就创建)
	 * @param fileName
	 * @return
	 * @throws Exception boolean
	 */
	public static boolean createFile(File fileName) throws Exception {
		boolean flag = false;
		try {
			if (!fileName.exists()) {
				fileName.createNewFile();
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * @MdoeName getUuid TODO(获取uuid)
	 * @return String
	 */
	public static String getUuid() {
		return UUID.randomUUID().toString();
	}

	/**
	 * 生成指定长度的随机码
	 *
	 * @param len
	 * @return
	 */
	public static String getRandomChar(int len) {
		Random random = new Random();
		char ch = '0';
		LinkedList<String> ls = new LinkedList<String>();

		for (int i = 0; i < 10; i++) { // 0-9
			ls.add(String.valueOf(48 + i));
		}

		for (int i = 0; i < 26; i++) { // A-Z
			ls.add(String.valueOf(65 + i));
		}

		for (int i = 0; i < 26; i++) { // a-z
			ls.add(String.valueOf(97 + i));
		}

		StringBuilder sb = new StringBuilder();
		int index;

		for (int i = 0; i < len; i++) {
			index = random.nextInt(ls.size());

			if (index > (ls.size() - 1)) {
				index = ls.size() - 1;
			}

			ch = (char) Integer.parseInt(String.valueOf(ls.get(index)));
			sb.append(ch);
		}

		return sb.toString();
	}

	/**
	 * @MdoeName writeTxtFile TODO(将文本写入文件)
	 * @param content  //要输入到文件中的话
	 * @param fileName //文件地址
	 * @return
	 * @throws Exception boolean
	 */
	public static boolean writeTxtFile(String content, File fileName) throws Exception {
		RandomAccessFile mm = null;
		boolean flag = false;
		FileOutputStream o = null;
		try {
			o = new FileOutputStream(fileName);
			o.write(content.getBytes("UTF-8"));
			o.close();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mm != null) {
				mm.close();
			}
		}
		return flag;
	}

	/**
	 * @MdoeName Capture TODO(视频截屏工具依赖于ffmpeg.exe软件)
	 * @param videoLocation 忘了
	 * @param videoCover    忘了
	 * @param timeStart     截屏时间
	 * @param frames        忘了
	 * @return boolean
	 */
	public static boolean Capture(String videoLocation, String videoCover, String timeStart, String frames) {
		List<String> commend = new java.util.ArrayList<String>();
		commend.add("/data120/java/apache-tomcat-8.5.3/webapps/hs_file/ffmpeg.exe");
		commend.add("-ss");
		commend.add(timeStart);
		commend.add("-i");
		commend.add(videoLocation);
		commend.add("-y");
		commend.add("-f");
		commend.add("image2");
		commend.add("-vframes");
		commend.add(frames);
		commend.add(videoCover);
		try {
			ProcessBuilder builder = new ProcessBuilder();
			builder.command(commend);
			builder.start();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * @MdoeName instructions TODO(生成随机数)
	 * @param end   最大随机数
	 * @param start 最小随机数
	 * @return Integer
	 */
	public static Integer instructions(Integer end, Integer start) {
		return new Random().nextInt(end - start + 1) + start;
	}

	/**
	 * @MdoeName flagNul TODO(空值判断)
	 * @param container
	 * @param list
	 * @return Boolean list中 Container value 取其判断空值 name 取其为返回
	 */
	public static Boolean flagNul(Container container, List<Container> list) {
		for (Container Cont : list) {
			if (container.get(Cont.get("value")) == null || container.getString(Cont.getString("value")).equals("")) {
				list = new ArrayList<>();
				list.add(Cont);
				return false;
			}
		}
		return true;
	}

	/**
	 * @MdoeName isItEmpty TODO(类型判断)
	 * @param obj
	 * @return Boolean
	 */
	public static Boolean isItEmpty(Object obj) {
		if (obj != null) {
			if (obj instanceof String) {
				if (((String) obj).equals("")) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @MdoeName StringTurn TODO(将字符串按,隔开变成数组)
	 * @param array
	 * @return String[]
	 */
	public static String[] StringTurn(String array) {
		String arrays[] = array.split(",");
		return arrays;
	}

	/**
	 * @MdoeName StringList TODO(将字符串按,隔开变成list集合)
	 * @param array
	 * @return List<String>
	 */
	public static List<String> StringList(String array) {
		List<String> list = new ArrayList<>();
		if (array.equals("")) {
			return list;
		}
		for (String str : StringTurn(array)) {
			list.add(str);
		}
		return list;
	}

	/**
	 * @MdoeName listaddattribute TODO(为list集合中的map对象添加一个属性)
	 * @param listaddattribute 需要添加属性的集合
	 * @param str              添加的属性字段
	 * @return List<String>
	 */
	public static List<String> listaddattribute(List<Map<String, Object>> listaddattribute, String str) {
		List<String> list = new ArrayList<>();
		for (Map<String, Object> map : listaddattribute) {
			list.add(String.valueOf(map.get(str)));
		}
		return list;
	}

	/**
	 * @MdoeName getMap TODO(快速生成map的方法)
	 * @param key
	 * @param value
	 * @return Map<String,Object>
	 */
	public static Map<String, Object> getMap(String key, Object value) {
		Map<String, Object> map = new HashMap<>();
		map.put(key, value);
		return map;
	}

	/**
	 * @MdoeName ioin TODO(将字符串转换为2进制,可以用来辅助加密)
	 * @param io
	 * @return String
	 */
	public static String ioin(String io) {
		String[] binaryArr = new String[io.length() / 8];
		for (int i = 0; i < io.length(); i = i + 8) {
			binaryArr[i / 8] = io.substring(i, i + 8);
		}
		String s1 = String.valueOf(binaryArr);
		StringBuilder strBuilder = new StringBuilder();
		byte[] mm3 = new byte[binaryArr.length];
		for (int i = 0; i < binaryArr.length; i++) {

			String m = binaryArr[i];
			int nn4 = Integer.parseInt(m, 2);
			byte mm = (byte) (nn4);
			mm3[i] = mm;

		}
		return new String(mm3, Charset.forName("UTF-8"));
	}

	/**
	 * @Title: returnClass TODO(将map转为对象结构)
	 * @param JSON map对象
	 * @param b    对象
	 * @return
	 * @throws Exception T
	 */
	public static <T> T returnClass(Map<String, Object> JSON, T b) throws Exception {
		Class a = b.getClass();
		Field[] fields = a.getDeclaredFields();
		Integer length = fields.length;
		List<String> list = new ArrayList<>();
		while (length-- > 0) {
			list.add(fields[length].getName());
		}
		for (String string : list) {
			if (JSON.get(string) == null) {
			} else {
				Field field = a.getDeclaredField(string);
				field.setAccessible(true);
				if (field.getGenericType() == String.class) {
					field.set(b, JSON.get(string) + "");
				} else {
					field.set(b, Integer.parseInt(JSON.get(string) + ""));
				}
			}
		}
		return b;
	}

	/**
	 * @MdoeName listes TODO(双list<String>去重复)
	 * @param list1
	 * @param list2
	 * @return List<String>
	 */
	public static List<String> listes(List<String> list1, List<String> list2) {
		List<String> set = new ArrayList<>();
		for (String string1 : list1) {
			Boolean falg = true;
			for (int i = 0; i < list2.size(); i++) {
				if (string1.equals(list2.get(i))) {
					list2.remove(i);
					falg = false;
					break;
				}
			}
			if (falg) {
				set.add(string1);
			}

		}
		return set;
	}

	/**
	 * @MdoeName sdkl TODO(对对象的深度克隆方法) 缺点,无法对Container对象进行克隆,调用Container对象自己的clone即可
	 * @param object 放入需要被克隆的对象
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException Object
	 */
	public static Object sdkl(Object object) throws IOException, ClassNotFoundException {
		ByteArrayOutputStream bos = null;
		ObjectOutputStream oos = null;
		ByteArrayInputStream bais = null;
		ObjectInputStream ois = null;
		try {
			bos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(bos);
			oos.writeObject(object);
			bais = new ByteArrayInputStream(bos.toByteArray());
			ois = new ObjectInputStream(bais);
			Object obj = ois.readObject();
			return obj;
		} catch (Exception e) {
			throw new MyExceptionIncomplete("克隆失败");
		} finally {
			if (ois != null) {
				ois.close();
			}
			if (bais != null) {
				bais.close();
			}
			if (oos != null) {
				oos.close();
			}
			if (bos != null) {
				bos.close();
			}
		}
	}

	/**
	 * @MdoeName reflect TODO(多重解析:将JSONArray 对象内容解析为List<HashMap>格式)
	 * @param json
	 * @return Object
	 */
	public static List<Object> reflectContainerArray(JSONArray json) {
		List<Object> list = new ArrayList<Object>();
		for (Object o : json) {
			if (o instanceof JSONArray)
				list.add(reflectContainerArray((JSONArray) o));
			else if (o instanceof JSONObject)
				list.add(reflectContainer((JSONObject) o));
			else
				list.add(o);
		}
		return list;
	}
	
	public static Container reflectContainer(JSONObject json) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		Set keys = json.keySet();
		for (Object key : keys) {
			Object o = json.get(key);
			if (o instanceof JSONArray)
				map.put((String) key, reflectContainerArray((JSONArray) o));
			else if (o instanceof JSONObject)
				map.put((String) key, reflectContainer((JSONObject) o));
			else
				map.put((String) key, o);
		}
		return new Container(map);
	}

	/**
	 * @MdoeName reflect TODO(多重解析:将JSONArray 对象内容解析为List<HashMap>格式)
	 * @param json
	 * @return Object
	 */
	public static List<Object> reflect(JSONArray json) {
		List<Object> list = new ArrayList<Object>();
		for (Object o : json) {
			if (o instanceof JSONArray)
				list.add(reflect((JSONArray) o));
			else if (o instanceof JSONObject)
				list.add(reflect((JSONObject) o));
			else
				list.add(o);
		}
		return list;
	}

	/**
	 * @MdoeName reflectOne TODO(单层解析的map类型json:常用于验签)
	 * @param json map类型json
	 * @return HashMap<String, String>
	 */
	public static HashMap<String, String> reflectOne(JSONObject json) {
		HashMap<String, String> map = new HashMap<String, String>();
		Set keys = json.keySet();
		for (Object key : keys) {
			Object o = json.get(key);
			if (o instanceof JSONArray)
				map.put((String) key, ((JSONArray) o).toString());
			else if (o instanceof JSONObject)
				map.put((String) key, ((JSONObject) o).toString());
			else
				map.put((String) key, String.valueOf(o).equals("null") ? null : String.valueOf(o));
		}
		return map;
	}

	/**
	 * @MdoeName reflect TODO(多重解析:将JSONArray 对象内容解析为HashMap<String, Object>格式)
	 * @param json
	 * @return Object
	 */
	private static HashMap<String, Object> reflect(JSONObject json) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		Set keys = json.keySet();
		for (Object key : keys) {
			Object o = json.get(key);
			if (o instanceof JSONArray)
				map.put((String) key, reflect((JSONArray) o));
			else if (o instanceof JSONObject)
				map.put((String) key, reflect((JSONObject) o));
			else
				map.put((String) key, o);
		}
		return map;
	}

	/**
	 * @param <T>
	 * @Title:listGroupSize TODO(分组 :相同数据归于同一个集合)
	 * @param list    需要分组的数据
	 * @param str     连接符
	 * @param strings 分组属性
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static <T extends Map> Map<String, List<T>> listGroupList(List<T> list, String str, String... strings) {
		Map<String, List<T>> mapRet = new HashMap<>();
		for (Map map : list) {
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < strings.length; i++) {
				stringBuilder.append(map.get(strings[i]));
				if (i != strings.length - 1) {
					stringBuilder.append(str);
				}
			}
			if (mapRet.get(stringBuilder.toString()) == null) {
				List<T> listRet = new ArrayList<T>();
				listRet.add((T) map);
				mapRet.put(stringBuilder.toString(), listRet);
			} else {
				List<T> listRet = mapRet.get(stringBuilder.toString());
				listRet.add((T) map);
			}
		}
		return mapRet;
	}

	/**
	 * @param <T>
	 * @Title:listGroupSize TODO(分组:相同数据只保留一个)
	 * @param list    需要分组的数据
	 * @param str     分割字符串
	 * @param strings 分组依据
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static <T extends Map> Map<String, T> listForMap(List<T> list, String str, String... strings) {
		Map<String, T> maps = new HashMap<String, T>();
		for (Map map : list) {
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < strings.length; i++) {
				stringBuilder.append(map.get(strings[i]));
				if (i != strings.length - 1) {
					stringBuilder.append(str);
				}
			}
			maps.put(stringBuilder.toString(), (T) map);
		}
		return maps;
	}

	/**
	 * @Title:log TODO(异常信息)
	 * @param exception
	 */
	public static String log(Exception exception) {
		StringWriter sw = null;
		PrintWriter pw = null;
		try {
			sw = new StringWriter();
			pw = new PrintWriter(sw);
			exception.printStackTrace(pw);
			String msg = sw.toString();
			return msg;
		} finally {
			if (pw != null) {
				pw.close();
			}
			if (sw != null) {
				try {
					sw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}


	/**
	 * @param <T>
	 * @Title:listGroupSize TODO(分组:相同数据只保留一个)
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static <T> T map(Object var1, Class<T> var2){
		Mapper dozerMapper =  new DozerBeanMapper();
		return dozerMapper.map(var1,var2);
	}

	/**
	 * @param <T>
	 * @Title:listGroupSize TODO(分组:相同数据只保留一个)
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static <T> List<T>  list(List list, Class<T> var2){
		 List<T> listT = (List<T>) list.stream().map(var->{
			Mapper dozerMapper =  new DozerBeanMapper();
			return dozerMapper.map(var,var2);
		}).collect(Collectors.toList());
		return listT;
	}

}
