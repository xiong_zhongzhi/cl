package com.cl.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @ClassName: Newjs
 * @Description:TODO(加减乘除计算类)
 * @author: 沉思有顷 564715118
 * @date: 2021年5月3日 上午10:48:33
 * @Copyright:
 */
public class Newjs {

    private Integer scale;
    private RoundingMode roundingMode;
    //	RoundingMode.UP:直接进位，比如1.22222如果保留1位小数，得到的就是1.3
    //	RoundingMode.DOWN:直接省略多余的小数，比如1.28888如果保留1位小数，得到的就是1.2
    //	RoundingMode.FLOOR:向下取整，2.35保留1位，变成2.3
    //	RoundingMode.HALF_UP:四舍五入（舍入远离零的舍入模式），2.35保留1位，变成2.4
    //	RoundingMode.HALF_DOWN:四舍五入（接近零的舍入模式），2.35保留1位，变成2.3

    public Newjs() {
        this.scale = 1;
        this.roundingMode = RoundingMode.HALF_UP;
    }

    public Newjs(Integer scale, RoundingMode roundingMode) {
        this.scale = scale;
        this.roundingMode = roundingMode;
    }

    public void setScale(Integer scale) {
        this.scale = scale;
    }

    public void setRoundingMode(RoundingMode roundingMode) {
        this.roundingMode = roundingMode;
    }

    // 加
    public BigDecimal add(Object d1, Object d2) {
        BigDecimal bd1 = new BigDecimal(String.valueOf(d1));
        BigDecimal bd2 = new BigDecimal(String.valueOf(d2));
        return bd1.add(bd2).setScale(scale, roundingMode);
    }

    // 减
    public BigDecimal subtract(Object d1, Object d2) {
        BigDecimal bd1 = new BigDecimal(String.valueOf(d1));
        BigDecimal bd2 = new BigDecimal(String.valueOf(d2));
        return bd1.subtract(bd2).setScale(scale, roundingMode);
    }

    // 乘
    public BigDecimal multiply(Object d1, Object d2) {
        BigDecimal b1 = new BigDecimal(String.valueOf(d1));
        BigDecimal b2 = new BigDecimal(String.valueOf(d2));
        return b1.multiply(b2).setScale(scale,roundingMode);
    }

    // 除
    public BigDecimal divide(Object d1, Object d2) {
        BigDecimal b1 = new BigDecimal(String.valueOf(d1));
        BigDecimal b2 = new BigDecimal(String.valueOf(d2));
        return b1.divide(b2, scale, roundingMode);
    }

    // 除
    public BigDecimal divide(Object d1, Object d2 ,Integer scale) {
        BigDecimal b1 = new BigDecimal(String.valueOf(d1));
        BigDecimal b2 = new BigDecimal(String.valueOf(d2));
        return b1.divide(b2, scale, roundingMode);
    }

}
