package com.cl.util;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class DataJsonDeserialization implements ObjectDeserializer {

    @Override
    public String deserialze(DefaultJSONParser defaultJSONParser, Type type, Object o) {
        JSONLexer lexer = defaultJSONParser.getLexer();
        String timestamp = lexer.stringVal();
        if (EnumTime.second.isValidDate(timestamp)){
            return timestamp;
        }
        DateFormat df = null;
        switch (timestamp.length()){
            case 19:
                df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
                break;
            case 13:
                df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSZ");
        }
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df2.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = null;
        try {
            date = df.parse(timestamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String newData = df2.format(date);
        return newData;
    }

    @Override
    public int getFastMatchToken() {
        return  0;
    }
}
