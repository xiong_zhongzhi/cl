package com.cl.util;

import org.springframework.util.Assert;
import org.springframework.util.MultiValueMap;

import java.util.*;

public class GetParamMultiValueMap<K,V> implements MultiValueMap<K, V> {

    private final Map<K, List<V>> map;

    public GetParamMultiValueMap(Map<K, V> paramMap) {

        Assert.notNull(paramMap, "'map' must not be null");

//将请求参数转换成MultiValueMap

        this.map = new HashMap<>();

        paramMap.forEach((k, v) -> {

            if (v instanceof List) {
                map.put(k, (List<V>) v);
            } else {
                ArrayList<V> list = new ArrayList();
                list.add(v);
                map.put(k, list);

            }

        });

    }

    @Override
    public V getFirst(K k) {
        return null;
    }

    @Override
    public void add(K k, V v) {

    }

    @Override
    public void addAll(K k, List<? extends V> list) {

    }

    @Override
    public void addAll(MultiValueMap<K, V> multiValueMap) {

    }

    @Override
    public void set(K k, V v) {

    }

    @Override
    public void setAll(Map<K, V> map) {

    }

    @Override
    public Map<K, V> toSingleValueMap() {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public List<V> get(Object key) {
        return null;
    }

    @Override
    public List<V> put(K key, List<V> value) {
        return null;
    }

    @Override
    public List<V> remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends List<V>> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<List<V>> values() {
        return null;
    }

    @Override
    public Set<Entry<K, List<V>>> entrySet() {
        return null;
    }
}