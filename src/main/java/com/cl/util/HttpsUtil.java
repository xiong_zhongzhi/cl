package com.cl.util;

import com.cl.been.Container;
import com.cl.handler.Jurisdiction.Drawer;
import com.cl.handler.api.ApiHandler;
import com.cl.log.OutPut;
import com.cl.util.localCache.Cache;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class HttpsUtil {

    public static Container mapPost(String url, Container container, String prefix, HttpMethod httpMethod) {
        OutPut.out("请求数据处理后-->" + container.toJson());
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.put(HttpHeaders.COOKIE, Arrays.asList(Drawer.get(ApiHandler.class).containerCookie.getString(prefix)));
        headers.setBearerAuth(Cache.container.getString("token"));
        ResponseEntity<String> response;
        switch (httpMethod) {
            case GET: {
                //使用自定义的uri处理器，实现参数追加到url的功能
                MultiValueMap<String, String> map = new LinkedMultiValueMap();
                MultiValueMap<String, Object> map1 = new LinkedMultiValueMap();
                map1.setAll(container.getMapStrList());
                map.setAll(container.getMapStr());
                HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity(map1, headers);
                url = UriComponentsBuilder
                        .fromHttpUrl(url)
                        .queryParams(map).toUriString();
                response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
            }
            break;
            case POST: {
                headers.add("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
                HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity(container.toJson(), headers);

                response = restTemplate.postForEntity(url, request, String.class);
            }

            break;
            case PUT: {
                headers.add("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
                HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity(container.toJson(), headers);
                response = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
            }

            break;
            default:
                throw new MyExceptionIncomplete("暂不支持");
        }
        if (!prefix.isEmpty()) {
            if (response.getHeaders().get("Set-Cookie") != null) {
                Drawer.get(ApiHandler.class).containerCookie.put(prefix,
                        response.getHeaders().get("Set-Cookie").get(0));
            }
        }
        // 数据处理
        String value = response.getBody();
        OutPut.out("返回数据-->" + value);
        try {
            value = new String(value.toString().getBytes(DataProcessingUtil.getEncoding(value)), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        OutPut.out("返回数据处理后-->" + value);
        return Container.jsonToContainer(value);
    }

    public static Container jsonPost(String url, Container container, String prefix, HttpMethod httpMethod) {
        OutPut.out("请求数据处理后-->" + container.toJson());
        RestTemplate restTemplate = new RestTemplate();


        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON.toString());
        headers.put(HttpHeaders.COOKIE, Arrays.asList(Drawer.get(ApiHandler.class).containerCookie.getString(prefix)));
        //放入token
        OutPut.out("token-->" + Cache.container.getString("token"));
        headers.setBearerAuth(Cache.container.getString("token"));

        HttpEntity<String> request = new HttpEntity<String>(container.toJson(), headers);
        ResponseEntity<String> response;
        switch (httpMethod) {
            case GET:
                response = restTemplate.getForEntity(url, String.class);
                break;
            case POST:
                response = restTemplate.postForEntity(url, request, String.class);
                break;
            default:
                throw new MyExceptionIncomplete("暂不支持");
        }
        //Cookie 存储
        if (!prefix.isEmpty()) {
            if (response.getHeaders().get("Set-Cookie") != null) {
                Drawer.get(ApiHandler.class).containerCookie.put(prefix,
                        response.getHeaders().get("Set-Cookie").get(0));
            }
        }
        // 数据处理
        String value = response.getBody();
        OutPut.out("返回数据-->" + value);
        try {
            value = new String(value.toString().getBytes(DataProcessingUtil.getEncoding(value)), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        OutPut.out("返回数据处理后-->" + value);
        return Container.jsonToContainer(value);
    }

}