package com.cl.util;

import java.util.Base64;

import org.apache.commons.codec.digest.DigestUtils;

/** * @Author: xiongzhongzhi * @Date: 2021/9/24 * @Description: */
public class RetailStoreRequstDTO {
	public RetailStoreRequstDTO(String data) {
		this.appKey = "56ce67b4c9f5b4b8";
		this.timestamp = EnumTime.second.getTime();
		this.data = data;
		this.sign = getSign(timestamp, "001683ca506f4dcb424750e08c4cabd6", data);
	}

	public String appKey;
	public String data;
	public String sign;
	public String timestamp;

	public static String getSign(String timestamp, String appSecret, String dataJson) {
		String dataStr = appSecret + timestamp + dataJson + appSecret;
		return Base64.getEncoder().encodeToString(DigestUtils.md5Hex(dataStr).getBytes()).toUpperCase();
	}
}
