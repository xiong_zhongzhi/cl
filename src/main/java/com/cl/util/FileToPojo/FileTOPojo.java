package com.cl.util.FileToPojo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cl.api.user.pojo.User;
import com.cl.util.MyExceptionIncomplete;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileTOPojo {

    //文件存放地址
    private static String url = "src/main/resources/settings/";

    //读取配置文件的数据
    public static  <T> T getSettingPojo(String settingName, Class<T> clazz) {
        File file = new File(url+settingName);
        if (!file.exists()){
            try {
                return clazz.newInstance();
            }catch (Exception e){
                throw new MyExceptionIncomplete(e);
            }
        }
        String content = null;
        try (FileInputStream fis = new FileInputStream(url+settingName); InputStreamReader reader = new InputStreamReader(fis,"UTF-8");  BufferedReader br = new BufferedReader(reader);) {
            String line;
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line);
            }
            content = stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.parseObject(content, clazz);
    }

    //读取配置文件的数据
    public static <T> List<T> getSettingList(String settingName, Class<T> clazz) {
        File file = new File(url+settingName);
        if (!file.exists()){
            try {
                return new ArrayList<>();
            }catch (Exception e){
                throw new MyExceptionIncomplete(e);
            }
        }
        String content = null;
        try (FileInputStream fis = new FileInputStream(url+settingName); InputStreamReader reader = new InputStreamReader(fis,"UTF-8");  BufferedReader br = new BufferedReader(reader);) {
            String line;
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line);
            }
            content = stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONArray.parseArray(content,clazz);
    }

    //保存配置文件数据
    public static void setSetting(String settingName, Object object) {
        if (object == null){
            return;
        }
        File file = new File(url+settingName);
        if (!file.exists()){
            try {
                file.createNewFile();
            }catch (Exception e){
                throw new MyExceptionIncomplete(e);
            }
        }
        String string = JSONObject.toJSONString(object);
        try (PrintStream stream = new PrintStream(url+settingName)) {
            stream.print(string);//写入的字符串
        } catch (FileNotFoundException e) {
            throw new MyExceptionIncomplete(e);
        }
    }

    public static void main(String[] args) {
        User user = new User();
        user.setAccount("564715118");
        user.setPassword("xzz07153213");
        List<User> users = new ArrayList<>();
        users.add(user);
        FileTOPojo.setSetting("LoginLog",users);
        List<User> users2 = FileTOPojo.getSettingList("LoginLog",User.class);
        System.out.println(users2.get(0).getAccount());
    }

}
