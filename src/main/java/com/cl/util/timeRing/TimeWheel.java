package com.cl.util.timeRing;


import com.cl.util.EnumTime;
import com.cl.util.MyExceptionIncomplete;
import com.cl.util.OSinfo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 564715118 沉思有顷 时间轮
 */
public class TimeWheel implements Runnable {

    // 任务队列
    volatile public LinkedList<Task> taskList = new LinkedList<Task>();

    // 多轮队列
    volatile public Wheel[] wheels;

    // 轮级
    public Integer pointerLevel = 0;

    // 时间轮级别
    public EnumTimeWheel enumTimeWheel;

    // 初始化时间轮
    public TimeWheel(EnumTimeWheel enumTimeWheel) {
        this.enumTimeWheel = enumTimeWheel;
        List<EnumTimeWheel> list = new ArrayList<>();
        switch (enumTimeWheel) {
            case day:// 天轮
                list.add(EnumTimeWheel.day);
            case hou:// 时轮
                list.add(EnumTimeWheel.hou);
            case minute:// 分轮
                list.add(EnumTimeWheel.minute);
                break;
            default:
                throw new MyExceptionIncomplete("暂不支持此类型时间轮");
        }
        wheels = new Wheel[list.size()];
        String millisecondTime = EnumTime.millisecond.getTime();
        for (int i = 0; i < wheels.length; i++) {
            wheels[i] = new Wheel(list.get(i), millisecondTime);
        }
    }

    // 转动时间轮
    @Override
    public void run() {
        while (true) {
            turn(0L);
            pointerLevel = 0;
            //处理到相同任务后则停止循环
            List<Task> list = new ArrayList<>();
            while (taskList.size() > 1) {
                Task task = taskList.pop();
                task.cylinderNumber -= 1;
                if (task.cylinderNumber == 0) {
                    wheels[0].addWork(task);
                    System.out.println("任务降级处理 : " + task.taskValue + " 预计执行时间 : " + task.vessels + " 时间轮相对时间 : "
                            + wheels[0].absoluteTime + " 当前实际时间 : " + EnumTime.millisecond.getTime());
                } else {
                    list.add(task);
                }
            }
            for (Task task : list) {
                taskList.add(task);
            }
        }
    }

    // 转轮子
    public void turn(Long differ) {
        Wheel wheel = wheels[pointerLevel];
        wheel.difference += differ;
        if (wheel.enumTimeWheel == EnumTimeWheel.minute) {
            System.out.println("分轮处理误差"+wheel.difference);
        }
        while (wheel.bounce()) {
            // 判断是否有下级轮子
            if (pointerLevel + 1 < wheels.length) {
                pointerLevel += 1;
                // 任务降级处理
                System.out.println("任务降级处理");
                while (wheel.ticks[wheel.pointer].getTaskList().size() > 0) {
                    Task task = wheel.ticks[wheel.pointer].getTaskList().pop();
                    wheels[pointerLevel].addWork(task);
                }
                turn(wheel.calibration());
            } else {
                String startTiem = EnumTime.millisecond.getTime();
                // 执行任务
                while (wheel.ticks[wheel.pointer].getTaskList().size() > 0) {
                    Task task = wheel.ticks[wheel.pointer].getTaskList().pop();
                    System.out.println("执行任务 : " + task.taskValue + " 预计执行时间 : " + task.vessels + " 时间轮相对时间 : "
                            + wheel.absoluteTime + " 当前实际时间 : " + EnumTime.millisecond.getTime());
                }
                // 时间补差
                Long differenceTime = 1000 - Long.parseLong(EnumTime.millisecond.dateGap(startTiem));
                wheel.difference += differenceTime;
                if (wheel.difference > 0L) {
                    try {
                        if (OSinfo.isWindows()) {
                            wheel.difference = wheel.difference / 10 * 10;
                        }
                        Thread.sleep(wheel.difference);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    wheel.difference = 0L;
                }
            }
            // 时间轮相对时间跟进
            wheel.absoluteTimeBounce();
        }
        // 升级
        pointerLevel -= 1;
    }

    // 添加延迟任务 time格式:
    public void addDelayedWork(String work, Integer... vessels) {

        Integer day = 0;
        Integer hou = 0;
        Integer minute = 0;
        Integer second = 0;
        switch (vessels.length) {
            case 4://
                 day = vessels[0];
                 hou = vessels[1];
                 minute = vessels[2];
                 second = vessels[3];
                break;
            case 3:// 时
                hou = vessels[0];
                minute = vessels[1];
                second = vessels[2];
                break;
            case 2:// 分
                minute = vessels[0];
                second = vessels[1];
                break;
            case 1:// 秒
                second = vessels[0];
                break;
        }

        // 锚定时间轮与格子
        switch (vessels.length) {
            case 4://
                if (day > 0) {
                    if (enumTimeWheel == EnumTimeWheel.day) {
                        taskList.add(new Task(work, vessels, day));
                    } else {
                        wheels[wheels.length - 4].addWork(work, day, vessels);
                    }
                    break;
                }
            case 3:// 时
                if (hou > 0) {
                    if (enumTimeWheel == EnumTimeWheel.hou) {
                        taskList.add(new Task(work, vessels, hou));
                    } else {
                        wheels[wheels.length - 3].addWork(work, hou, vessels);
                    }
                    break;
                }
            case 2:// 分
                if (minute > 0) {
                    if (enumTimeWheel == EnumTimeWheel.minute) {
                        taskList.add(new Task(work, vessels, minute));
                    } else {
                        wheels[wheels.length - 2].addWork(work, minute, vessels);
                    }
                    break;
                }
            case 1:// 秒
                // 秒轮直接追加任务
                wheels[wheels.length - 1].addWork(work, second, vessels);
                break;
            default:
                throw new MyExceptionIncomplete("暂不支持此类型时间轮");
        }
    }

    public static void main(String[] args) {
        TimeWheel timeWheel = new TimeWheel(EnumTimeWheel.hou);
        Thread thread = new Thread(timeWheel);
        thread.start();
//		timeWheel.addDelayedWork("测试","1:2:3");
//		timeWheel.addDelayedWork("测试233","0:0:1");
    }

}
