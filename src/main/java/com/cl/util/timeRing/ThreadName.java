package com.cl.util.timeRing;

public class ThreadName<T> {

	private static ThreadLocal threadLocal = new ThreadLocal();

	public void set(T value) {
		threadLocal.set(value);
	}

	public T get() {
		return (T) threadLocal.get();
	}

	public void remove() {
		threadLocal.remove();
	}

}
