package com.cl.util.timeRing;

//延时任务
public class Task {

	Task(String taskValue, Integer[] vessels) {
		this.taskValue = taskValue;
		this.vessels = vessels;
	}
	
	Task(String taskValue, Integer[] vessels,Integer cylinderNumber) {
		this.taskValue = taskValue;
		this.vessels = vessels;
		this.cylinderNumber = cylinderNumber;
	}

	// 任务内容
	public String taskValue;

	// 预计执行时间
	public Integer[] vessels;

	//圈数
	public Integer cylinderNumber;
}
