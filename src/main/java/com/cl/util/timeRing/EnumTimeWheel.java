package com.cl.util.timeRing;

public enum EnumTimeWheel {
	minute(60,"分轮"), hou(60,"时轮"), day(24,"天轮"),week(7,"周轮"), month(31,"月轮"),year(12,"年轮");

	private Integer cylinderNumber;
	
	private String wheelNmae;

	EnumTimeWheel(Integer cylinderNumber,String wheelNmae) {
		this.cylinderNumber = cylinderNumber;
		this.wheelNmae = wheelNmae;
	}

	public String getWheelNmae() {
		return wheelNmae;
	}

	public Integer getCylinderNumber() {
		return cylinderNumber;
	}

}
