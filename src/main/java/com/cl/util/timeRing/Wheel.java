package com.cl.util.timeRing;

import com.alibaba.fastjson.JSONObject;
import com.cl.util.EnumTime;
import com.cl.util.MyExceptionIncomplete;

public class Wheel {
    // 时间轮类型
    public EnumTimeWheel enumTimeWheel;
    // 格子
    public Tick[] ticks;
    // 指针
    public int pointer;
    // 时间轮延迟误差
    public Long difference = 0L;
    // 绝对时间
    public String absoluteTime;
    // 校准
    public Boolean flag = false;

    // 初始化时间轮
    public Wheel(EnumTimeWheel enumTimeWheel, String absoluteTime) {
        this.enumTimeWheel = enumTimeWheel;
        this.absoluteTime = absoluteTime;
        ticks = new Tick[enumTimeWheel.getCylinderNumber()];
        for (int i = 0; i < ticks.length; i++) {
            ticks[i] = new Tick();
        }
        // 时间锚定
        anchoring();
    }

    // 格子跳动
    public Boolean bounce() {
        pointer += 1;
        if (enumTimeWheel.getCylinderNumber() > pointer) {
            System.out.println("相对时间 : " + absoluteTime + "--" + "当前时间 : " + EnumTime.millisecond.getTime() + "--"
            		+ enumTimeWheel.getWheelNmae() + "跳动格子到:" + pointer);
            return true;
        } else {
            pointer = -1;
            return false;
        }

    }

    // 格子跳动
    public void absoluteTimeBounce() {
        switch (enumTimeWheel) {
            case day:// 天轮
                absoluteTime = EnumTime.hou.dateSubMillisecond(EnumTime.millisecond.getLocalDateTime(absoluteTime), 1);
                break;
            case hou:// 时轮
                absoluteTime = EnumTime.minute.dateSubMillisecond(EnumTime.millisecond.getLocalDateTime(absoluteTime), 1);
                break;
            case minute:// 分轮
                absoluteTime = EnumTime.second.dateSubMillisecond(EnumTime.millisecond.getLocalDateTime(absoluteTime), 1);
                break;
            default:
                throw new MyExceptionIncomplete("未配置此类型的时间轮锚定逻辑");
        }
    }

    // 初始格子锚定
    public void anchoring() {
        switch (enumTimeWheel) {
            case day:// 天轮
                absoluteTime = EnumTime.hou.getSupplementTime(absoluteTime);
                pointer = EnumTime.hou.getInTime(absoluteTime) - 1;
                break;
            case hou:// 时轮
                absoluteTime = EnumTime.minute.getSupplementTime(absoluteTime);
                pointer = EnumTime.minute.getInTime(absoluteTime) - 1;
                break;
            case minute:// 分轮
                absoluteTime = EnumTime.second.getSupplementTime(absoluteTime);
                pointer = EnumTime.second.getInTime(absoluteTime) - 1;
                break;
            default:
                throw new MyExceptionIncomplete("未配置此类型的时间轮锚定逻辑");
        }
    }

    //添加任务
    public void addWork(String work, Integer location, Integer[] vessels) {
        if (location > ticks.length) {
            throw new MyExceptionIncomplete("此任务超出当前时间最大延迟时间");
        }
        Task task = new Task(work, vessels);

        location = pointer + location;
        if (location >= enumTimeWheel.getCylinderNumber()) {
            location = location % enumTimeWheel.getCylinderNumber();
        }
        ticks[location].getTaskList().add(task);
        System.out.println("定时任务接收 -> " + JSONObject.toJSONString(task) + enumTimeWheel.getWheelNmae()+"轮接收格子" + location);

    }

    //降级任务接收
    public void addWork(Task task) {

        Integer day = 0;
        Integer hou = 0;
        Integer minute = 0;
        Integer second = 0;
        switch (task.vessels.length) {
            case 4://
                day = task.vessels[0];
                hou = task.vessels[1];
                minute = task.vessels[2];
                second = task.vessels[3];
                break;
            case 3:// 时
                hou = task.vessels[0];
                minute = task.vessels[1];
                second = task.vessels[2];
                break;
            case 2:// 分
                minute = task.vessels[0];
                second = task.vessels[1];
                break;
            case 1:// 秒
                second = task.vessels[0];
                break;
        }

        System.out.println("任务降级处理 : " + task.taskValue + " 预计执行时间 : " + task.toString() + " 时间轮相对时间 : "
                + absoluteTime + " 当前实际时间 : " + EnumTime.millisecond.getTime());
        Integer location;
        switch (enumTimeWheel) {
            case day:// 天轮
                location = hou;
                break;
            case hou:// 时轮
                location = minute;
                break;
            case minute:// 分轮
                location = second;
                break;
            default:
                throw new MyExceptionIncomplete("暂不支持此类型时间轮");
        }
        if (location > ticks.length) {
            throw new MyExceptionIncomplete("此任务超出当前时间最大延迟时间");
        }
        location = pointer + location;
        if (location >= enumTimeWheel.getCylinderNumber()) {
            location = location % enumTimeWheel.getCylinderNumber();
        }
        ticks[location].getTaskList().add(task);
        System.out.println("定时任务降级处理接收 -> " + JSONObject.toJSONString(task) + enumTimeWheel.getWheelNmae()+"轮接收格子" + location);
    }

    // 格子校准
    public Long calibration() {
        // 相对时间 : 绝对时间 :当前时间
        Long differ = 0L;
        switch (enumTimeWheel) {
            case hou:// 时轮
                if (flag) {
                    differ = Long.parseLong(EnumTime.millisecond.dateGap(absoluteTime));
                    System.out.println("分轮校准误差 :"+ differ);
                    if (differ > 60000L) {
                        differ = 60000L;
                    }
                } else {
                    flag = true;
                }
        }
        return differ;
    }

}
