package com.cl.been;


/**
* @ClassName: LinkFlag
* @Description: TODO(用于客户端断开链接时，抛出信息)
* @author 沉思有顷:564715118
* @date 2020年10月27日
*/
public class LinkFlag {
	public LinkFlag(Boolean flag) {
		this.flag = flag;
	}

	public Boolean flag = true;
	public String msg = "";// 错误原因

}
