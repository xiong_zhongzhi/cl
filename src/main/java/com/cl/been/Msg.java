package com.cl.been;

import com.cl.util.localCache.Cache;

public class Msg<T> {

    public Msg() {

    }

    public Msg(String instruct, T data) {
        this.instruct = instruct;
        this.data = data;
        token = Cache.container.getString("token");
    }

    public Msg(String instruct) {
        token = Cache.container.getString("token");
        this.instruct = instruct;
    }

    private String instruct;// 指令

    private String token;// 令牌

    private T data;// 操作

    public String getInstruct() {
        return instruct;
    }

    public void setInstruct(String instruct) {
        this.instruct = instruct;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


}
