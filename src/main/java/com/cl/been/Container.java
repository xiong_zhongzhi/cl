package com.cl.been;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.cl.util.DataProcessingUtil;
import com.cl.util.MyExceptionIncomplete;

/**
 * @author 564715118
 * @ClassName: Container
 * @Description: TODO(map的封装, 简洁代码, 提高工作效率用)
 * @date 2019年12月20日
 */
public class Container extends HashMap implements Map {
	private static final long serialVersionUID = 1L;
	private Container core;
	private Map map = null;

	public Container(Object key1, Object obj1, Object key2, Object obj2) {
		Map<Object, Object> map = new HashMap<>();
		map.put(key1, obj1);
		map.put(key2, obj2);
		this.map = map;
	}

	public Container() {
		map = new HashMap();
	}

	public Container(Map map) {
		this.map = map;
	}

	public Container(String key, Object obj) {
		Map<String, Object> map = new HashMap<>();
		map.put(key, obj);
		this.map = map;
	}

	public Container(String key1, Object obj1, String key2, Object obj2) {
		Map<String, Object> map = new HashMap<>();
		map.put(key1, obj1);
		map.put(key2, obj2);
		this.map = map;
	}

	public Container(Map<String, Object> map, String key, Object obj) {
		map.put(key, obj);
		this.map = map;
	}

	public Container upkey(String key, String newKey) {
		put(newKey, get(key));
		return this;
	}

	public Container upremovekey(String key, String newKey) {
		put(newKey, get(key));
		remove(key);
		return this;
	}

	// 获取map
	public Map getMap() {
		return this.map;
	}

	// 获取map<String,String> 形式的map 先产生一下容器污染可不可以骗过去
	public Map<String, String> getMapStr() {
		return returnStrMap();
	}

	public Map<String, Object> getMapStrList() {
		return returnStrMapList();
	}

	public <A> A get(Object key, Class<A> a) {
		Object obj = get(key);
		if (obj == null) {
			return null;
		} else {
			return (A) obj;
		}
	}

	public Container getPageData(String Sstr) {
		return (Container) map.get(Sstr);
	}

	public List<Container> getListContainer(String Sstr) {
		return (List<Container>) map.get(Sstr);
	}
	
	public List<Container> getToListContainer(String Sstr) {
		return toListContainer(map.get(Sstr));
	}

	@Override
	public Object get(Object key) {
		return map.get(key);
	}

	public Integer getStringInteger(Object key) {
		Object obj = get(key);
		return obj == null ? null : Integer.parseInt(String.valueOf(obj));
	}

	public Integer getInteger(Object key) {
		return (Integer) get(key);
	}

	public Method getMethod(Object key) {
		return (Method) get(key);
	}

	public Container getContainer(Object key) {
		return (Container) get(key);
	}
	
	public Container getToContainer(Object key) {
		return toContainer(get(key));
	}

	public Double getStrDouble(Object key) {
		Object obj = get(key);
		return obj == null ? null : Double.parseDouble(String.valueOf(obj));
	}

	public Double getDouble(Object key) {
		return (Double) get(key);
	}

	public Long getLong(Object key) {
		return (Long) get(key);
	}

	public Long getStringLong(Object key) {
		Object obj = get(key);
		return obj == null ? null : Long.parseLong(String.valueOf(obj));
	}

	public Boolean getBoolean(Object key) {
		return (Boolean) get(key);
	}

	public String getString(Object key) {
		Object obj = get(key);
		return obj == null ? null : String.valueOf(obj);
	}

	public String getStringOrStr(Object key, String str) {
		return get(key) == null ? str : (String) get(key);
	}

	@Override
	public Object put(Object key, Object value) {
		return map.put(key, value);
	}

	public Container newCore() {
		core = new Container();
		return this;
	}

	public Container count(Object key1, Object key2, Container container) {
		if (get(key1) == null) {
			put(key1, "0");
		}
		put(key1,
				new BigDecimal(getString(key1))
						.add(new BigDecimal(container == null ? getString(key2) == null ? "0" : getString(key2)
								: container.getString(key2) == null ? "0" : container.getString(key2)))
						.toString());
		return this;
	}

	public Container set(Object key, Object value) {
		map.put(key, value);
		return this;
	}

	public Container switchSet(Object key, Container container) {
		map.put(key, container.get(map.get(key)));
		return this;
	}

	@Override
	public Object remove(Object key) {
		return map.remove(key);
	}

	public void clear() {
		map.clear();
	}

	public void newclear() {
		this.map = new HashMap<>();
	}

	public boolean containsKey(Object key) {
		return map.containsKey(key);// 判断是否有指定键名
	}

	public boolean containsValue(Object value) {
		return map.containsValue(value);// 判断是否存在指定value
	}

	// 效率高
	public Set entrySet() {
		return map.entrySet();
	}

	// 判断map是否存在内容
	public boolean isEmpty() {
		return map.isEmpty();
	}

	// 获取key
	public Set keySet() {
		return map.keySet();
	}

	public void putAll(Map t) {
		map.putAll(t);// map合并
	}

	public int size() {
		return map.size();
	}

	public List<Object> getkeys() {
		List<Object> list = new ArrayList<>();
		for (Iterator ite = this.map.entrySet().iterator(); ite.hasNext();) {
			Map.Entry entry = (Map.Entry) ite.next();
			list.add(entry.getKey());
		}
		return list;
	}

	// 拿到map的key值集合
	public static List<String> getkeys(Map map) {
		List<String> list = new ArrayList<>();
		for (Iterator ite = map.entrySet().iterator(); ite.hasNext();) {
			Map.Entry entry = (Map.Entry) ite.next();
			list.add(entry.getKey() + "");
		}
		return list;
	}
	
	public static void main(String[] args) {
		
		
		
		
	}

	// map转对象
	public <T> T returnClass(T b) throws Exception {
		Map<String, Object> JSON = this.map;
		Class a = b.getClass();
		Field[] fields = a.getDeclaredFields();
		Integer length = fields.length;
		List<String> list = new ArrayList<>();
		while (length-- > 0) {
			list.add(fields[length].getName());
		}
		for (String string : list) {
			if (JSON.get(string) != null) {
				Field field = a.getDeclaredField(string);
				field.setAccessible(true);
				if (field.getGenericType() == String.class) {
					field.set(b, JSON.get(string) + "");
				} else if (field.getGenericType() == Integer.class) {
					field.set(b, Integer.parseInt(JSON.get(string) + ""));
				} else if (field.getGenericType() == Double.class) {
					field.set(b, Double.parseDouble(JSON.get(string) + ""));
				}
			}
		}
		return b;
	}

	// map转对象 <String, String>
	public Map<String, String> returnStrMap() {
		Map<String, String> returnMap = new HashMap<String, String>();
		for (Object obj : getkeys()) {
			returnMap.put(obj.toString(), getString(obj));
		}
		return returnMap;
	}

	// map转对象 <String, String>
	public Map<String, Object> returnStrMapList() {
		Map<String, Object> returnMap = new HashMap();
		for (Object obj : getkeys()) {
			if ( obj instanceof  List){
				returnMap.put(obj.toString(), get(obj));
			}else{
				returnMap.put(obj.toString(), getString(obj));
			}
		}
		return returnMap;
	}

	/**
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @Title:extract TODO(数据提纯)
	 */
	public void extract(String... str) throws ClassNotFoundException, IOException {
		newCore();
		Container container = this.core.getContainer("extract");
		if (container == null) {
			container = new Container((Map) DataProcessingUtil.sdkl(this.map));
			this.core.set("extract", container);
		}
		this.map.clear();
		for (String string : str) {
			put(string, container.get(string));
		}
	}

	/**
	 * @param str
	 * @return
	 * @throws Exception List<Container> 前后为 1个value 对应一个 name value,name,value,name
	 * @MdoeName flagNulCoordination TODO(与工具方法flagNul 配合使用)
	 */
	public static List<Container> flagNulCoordination(String... str) {
		List<Container> list = new ArrayList<>();
		if (str.length % 2 > 0) {
			throw new MyExceptionIncomplete("请被2整除");
		}
		Boolean flag = true;// true表示value false 表示name
		Container add = new Container();
		for (String string : str) {
			if (flag) {
				flag = false;
				add.put("value", string);
			} else {
				flag = true;
				add.put("name", string);
				list.add(add);
				add = new Container();
			}
		}
		return list;
	}

	/*
	 * @see java.util.HashMap#clone() TODO(克隆)
	 *
	 * @return
	 */
	@Override
	public Container clone() {
		Map ma = null;
		try {
			ma = (Map) DataProcessingUtil.sdkl(map);
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new Container(ma);
	}

	public boolean NotBlank(Object object) {
		return get(object) != null && !getString(object).equals("");
	}
	
	public Object getKey(Object value) {
		for (Object  object: getkeys()) {
			if (get(object) == value) {
				return object;
			}
		}
		return null;
	}

	public boolean NotBlankS(Object... objects) {
		for (Object object : objects) {
			if (!NotBlank(object)) {
				return false;
			}
		}
		return true;
	}

	public void NotBlankS(Container container, Object... objects) {
		for (Object object : objects) {
			if (!NotBlank(object)) {
				throw new MyExceptionIncomplete(container.get(object));
			}
		}
	}

	/*
	 * @see java.util.HashMap#clone() TODO(toJson)
	 *
	 * @return
	 */
	public String toJson() {
		return JSONObject.toJSONString(map, SerializerFeature.WriteMapNullValue);
	}
	
	public static Container toContainer(Object object) {
		return jsonToContainer(JSONObject.toJSONString(object, SerializerFeature.WriteMapNullValue));
	}
	
	public static Container jsonToContainer(String json) {
		return DataProcessingUtil.reflectContainer(JSONObject.parseObject(json));
	}

	public static List<Container> toListContainer(Object object) {
		return jsonToListContainer(JSONArray.toJSONString(object, SerializerFeature.WriteMapNullValue));
	}
	
	public static List<Container> jsonToListContainer(String json) {
		return JSONArray.parseArray(json, Container.class);
	}
	
	@Override
	public String toString() {
		return toJson();
	}

}
