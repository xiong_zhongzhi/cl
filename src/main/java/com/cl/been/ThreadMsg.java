package com.cl.been;

import java.nio.channels.SocketChannel;
/**
* @ClassName: ThreadMsg
* @Description: TODO(消息对象)
* @author 沉思有顷:564715118
* @date 2021年9月14日
*/
public class ThreadMsg {
	//消息对象
	private Msg msg;
	//通道
	private SocketChannel socketChannel;

	public Msg getMsg() {
		return msg;
	}
	public void setMsg(Msg msg) {
		this.msg = msg;
	}
	public SocketChannel getSocketChannel() {
		return socketChannel;
	}
	public void setSocketChannel(SocketChannel socketChannel) {
		this.socketChannel = socketChannel;
	}
	public ThreadMsg(Msg msg, SocketChannel socketChannel) {
		super();
		this.msg = msg;
		this.socketChannel = socketChannel;
	}
	
}
