package com.cl.api.skill.dto;

import java.time.LocalDateTime;

/**
 * 描述：技能DTO
 *
 * @author csyq
 * @date 2023-06-10
 */
public class SkillDTO {

    /**
     *
     */
    private Integer id;

    /**
     * 技能名称
     */
    private String name;

    /**
     * 技能描述
     */
    private String describe;

    /**
     * 实际技能效果
     */
    private String practicalDescribe;

    /**
     * 图片路径
     */
    private String imgUrl;

    /**
     * 目标类型0自身 1友方 2敌方3弹道4模型
     */
    private String pointToType;

    /**
     * 最小范围
     */
    private String scopeStart;

    /**
     * 最大范围
     */
    private String scopeEnd;

    /**
     * 技能模型
     */
    private String model;

    /**
     * 模型核心坐标
     */
    private String showModel;

    /**
     * 模型核心坐标
     */
    private String centralPoint;

    /**
     * 冷却(s)
     */
    private Integer refrigeration;

    /**
     * 限制0可用 1限制
     */
    private Integer astrict;

    /**
     * 是否删除 0否 1是
     */
    private Integer status;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getPracticalDescribe() {
        return practicalDescribe;
    }

    public void setPracticalDescribe(String practicalDescribe) {
        this.practicalDescribe = practicalDescribe;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getPointToType() {
        return pointToType;
    }

    public void setPointToType(String pointToType) {
        this.pointToType = pointToType;
    }

    public String getScopeStart() {
        return scopeStart;
    }

    public void setScopeStart(String scopeStart) {
        this.scopeStart = scopeStart;
    }

    public String getScopeEnd() {
        return scopeEnd;
    }

    public void setScopeEnd(String scopeEnd) {
        this.scopeEnd = scopeEnd;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getShowModel() {
        return showModel;
    }

    public void setShowModel(String showModel) {
        this.showModel = showModel;
    }

    public String getCentralPoint() {
        return centralPoint;
    }

    public void setCentralPoint(String centralPoint) {
        this.centralPoint = centralPoint;
    }

    public Integer getRefrigeration() {
        return refrigeration;
    }

    public void setRefrigeration(Integer refrigeration) {
        this.refrigeration = refrigeration;
    }

    public Integer getAstrict() {
        return astrict;
    }

    public void setAstrict(Integer astrict) {
        this.astrict = astrict;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}