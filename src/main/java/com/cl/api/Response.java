package com.cl.api;

import com.alibaba.fastjson.JSONObject;

public class Response<T> {

	// 状态信息
	private String result;

	// 请求结果
	private String msg;

	// 状态码
	private Integer state;

	// 数据
	private T data;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public void setClazz(Class<?> clazz) {
		this.data = (T) JSONObject.parseObject(JSONObject.toJSONString(data), clazz);
	}

}
