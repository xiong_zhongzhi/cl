package com.cl.api;

import com.cl.handler.Nature;

/**
 * @ClassName: Page
 * @Description: TODO(分页类)
 * @author 564715118
 * @date 2019年12月20日
 */
public class Page {
	@Nature("当前页")
	private Integer currentPage;
	@Nature("每页条数")
	private Integer pageSize;
	@Nature("起始行")
	private Integer startLine;
	@Nature("总条数")
	private Integer totalNumber;
	@Nature("总页数")
	private Integer pageCount;

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getStartLine() {
		return startLine;
	}

	public void setStartLine(Integer startLine) {
		this.startLine = startLine;
	}

	public Integer getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(Integer totalNumber) {
		this.totalNumber = totalNumber;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}
}
