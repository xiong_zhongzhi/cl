package com.cl.api.userInTime.response;

import com.cl.api.userInTime.dto.UserInTimeDTO;
import com.cl.handler.Nature;

/**
* 描述：及时备战数据列表请求Response类
* @author csyq
* @date 2023-05-19
*/
public class FindUserInTimeResponse {

    @Nature("用户即时备战信息")
    private UserInTimeDTO userInTimeDTO;

    public UserInTimeDTO getUserInTimeDTO() {
        return userInTimeDTO;
    }

    public void setUserInTimeDTO(UserInTimeDTO userInTimeDTO) {
        this.userInTimeDTO = userInTimeDTO;
    }
}
