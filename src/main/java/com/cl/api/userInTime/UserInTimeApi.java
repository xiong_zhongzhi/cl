package com.cl.api.userInTime;

import com.cl.api.ApiURL;
import com.cl.api.Response;
import com.cl.api.userInTime.request.UpdateUserInTimeRequest;
import com.cl.api.userInTime.response.FindUserInTimeResponse;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.api.Api;
import org.springframework.http.HttpMethod;

@RulesOfCompetency
@Api(ApiURL.SERVER_URL)
public interface UserInTimeApi {
	
	//查询及时备战数据列表
	@Api(value = "/userInTime/findUserInTime",httpMethod = HttpMethod.GET)
	public Response<FindUserInTimeResponse> findUserInTime();

	//查询及时备战数据列表
	@Api(value = "/userInTime/updateUserInTime",httpMethod = HttpMethod.PUT)
	public Response updateUserInTime(UpdateUserInTimeRequest updateUserInTime);
}