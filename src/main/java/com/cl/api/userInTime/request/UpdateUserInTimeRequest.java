package com.cl.api.userInTime.request;

import com.cl.handler.Nature;

/**
 * 描述：更新请求参数类UpdateUserInTimeRequest
 *
 * @author csyq
 * @date 2023-05-19
 */
public class UpdateUserInTimeRequest {

    /**
     * 当前出战的角色id
     */
    @Nature("当前出战的角色id")
    private Integer gameRoleId;

    public Integer getGameRoleId() {
        return gameRoleId;
    }

    public void setGameRoleId(Integer gameRoleId) {
        this.gameRoleId = gameRoleId;
    }
}
