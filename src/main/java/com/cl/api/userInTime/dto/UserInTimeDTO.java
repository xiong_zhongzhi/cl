package com.cl.api.userInTime.dto;

import com.cl.handler.Nature;


/**
 * 描述：及时备战数据DTO
 *
 * @author csyq
 * @date 2023-05-19
 */
public class UserInTimeDTO {

    /**
     *
     */
    @Nature("用户id")
    private String id;

    /**
     * 当前出战的角色id
     */
    @Nature("当前出战的角色id")
    private Integer gameRoleId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getGameRoleId() {
        return gameRoleId;
    }

    public void setGameRoleId(Integer gameRoleId) {
        this.gameRoleId = gameRoleId;
    }
}