package com.cl.api.account;

import com.cl.api.ApiURL;
import com.cl.api.account.response.FindAccountCLResponse;
import com.cl.api.Response;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.api.Api;
import org.springframework.http.HttpMethod;

@RulesOfCompetency
@Api(ApiURL.SERVER_URL)
public interface Account {
	
	//查询账号剩余积分
	@Api(value = "/account/findAccount",httpMethod = HttpMethod.GET)
	public Response<FindAccountCLResponse> findAccount();
	
}