package com.cl.api.account.response;

import com.cl.handler.Nature;
/**
* 描述：用户账户列表请求Response类
* @author csyq
* @date 2023-03-04
*/
public class FindAccountCLResponse {

    /**
     *账户积分
     */
    @Nature("账户积分")
    private Integer integral;

    public Integer getIntegral() {
        return integral;
    }

    public void setIntegral(Integer integral) {
        this.integral = integral;
    }
}
