package com.cl.api.mall;

import com.cl.api.ApiURL;
import com.cl.api.mall.request.FindGoodsPageRequest;
import com.cl.api.mall.response.FindGoodsPageResponse;
import com.cl.api.Response;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.api.Api;
import org.springframework.http.HttpMethod;

@RulesOfCompetency
@Api(ApiURL.SERVER_URL)
public interface GoodsApi {

	// 查询商品列表
	@Api(value = "/goods/mallFindGoodsPage", httpMethod = HttpMethod.GET)
	public Response<FindGoodsPageResponse> mallFindGoodsPage(FindGoodsPageRequest findGoodsPageRequest);

}