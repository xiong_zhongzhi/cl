package com.cl.api.mall;

import com.cl.api.ApiURL;
import com.cl.api.Response;
import com.cl.api.mall.request.FindGoodsCateListRequest;
import com.cl.api.mall.response.FindGoodsCateListResponse;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.api.Api;
import org.springframework.http.HttpMethod;

@RulesOfCompetency
@Api(ApiURL.SERVER_URL)
public interface GoodsCateApi {

    // 查询商品品类
    @Api(value = "/goodsCate/clientFindGoodsCateList", httpMethod = HttpMethod.GET)
    public Response<FindGoodsCateListResponse> clientFindGoodsCateList(FindGoodsCateListRequest findGoodsCateListRequest);



}
