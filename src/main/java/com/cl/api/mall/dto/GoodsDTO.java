package com.cl.api.mall.dto;

import com.cl.api.mall.pojo.Goods;

import java.util.List;

public class GoodsDTO extends Goods {

    List<GoodsSkuDTO> goodsSkuDTOList;

    public List<GoodsSkuDTO> getGoodsSkuDTOList() {
        return goodsSkuDTOList;
    }

    public void setGoodsSkuDTOList(List<GoodsSkuDTO> goodsSkuDTOList) {
        this.goodsSkuDTOList = goodsSkuDTOList;
    }
}