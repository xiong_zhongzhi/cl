package com.cl.api.mall.dto;

import com.cl.handler.Nature;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
* 描述：商品分类DTO
* @author csyq
* @date 2023-05-03
*/
public class GoodsCateDTO {

    /**
     *
     */
    @Nature("")
    private Integer cateId;

    /**
     *父节点id
     */
    @Nature("父节点id")
    private Integer parent;

    /**
     *0:1级类别 2:2级类别
     */
    @Nature("0:1级类别 1:2级类别")
    private Integer grade;

    /**
     *分类名称
     */
    @Nature("分类名称")
    private String cateName;

    /**
     *是否删除 0否 1是
     */
    @Nature("是否删除 0否 1是")
    private Integer status;

    /**
     *修改时间
     */
    @Nature("修改时间")
    private LocalDateTime updateTime;

    /**
     *创建时间
     */
    @Nature("创建时间")
    private LocalDateTime createTime;

    /**
     *商品分类子集
     */
    @Nature("商品分类子集")
    private List<GoodsCateDTO> children=new ArrayList<>();

    /**
     *指定节点选择框是否禁用为节点对象的某个属性值 true 开启
     */
    @Nature("选择框 true 禁用  ")
    private Boolean disabled;

    public Integer getCateId() {
        return cateId;
    }

    public void setCateId(Integer cateId) {
        this.cateId = cateId;
    }

    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public List<GoodsCateDTO> getChildren() {
        return children;
    }

    public void setChildren(List<GoodsCateDTO> children) {
        this.children = children;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }
}