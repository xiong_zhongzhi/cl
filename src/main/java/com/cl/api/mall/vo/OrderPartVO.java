package com.cl.api.mall.vo;

import com.cl.api.mall.dto.GoodsSkuDTO;
import com.cl.handler.Nature;
import java.time.LocalDateTime;

/**
* 描述：子订单VO类
* @author csyq
* @date 2023-05-14
*/
public class OrderPartVO {

        /**
        *
        */
        @Nature("")
        private Integer orderPartId;

        /**
        *单个商品的详细形象
        */
        @Nature("单个商品的详细形象")
        private String skuJsonData;

        /**
         *goodsSku商品信息
         */
        @Nature("goodsSku商品信息")
        private GoodsSkuDTO goodsSkuDTO;

        /**
        *购买数量
        */
        @Nature("购买数量")
        private Integer sum;

        /**
        *子订单总价格
        */
        @Nature("子订单总价格")
        private Integer sumPrice;

        /**
        *修改时间
        */
        @Nature("修改时间")
        private LocalDateTime updateTime;

        /**
        *创建时间
        */
        @Nature("创建时间")
        private LocalDateTime createTime;

        /**
        *是否删除 0否 1是
        */
        @Nature("是否删除 0否 1是")
        private Integer status;

        public Integer getOrderPartId() {
                return orderPartId;
        }

        public void setOrderPartId(Integer orderPartId) {
                this.orderPartId = orderPartId;
        }

        public String getSkuJsonData() {
                return skuJsonData;
        }

        public void setSkuJsonData(String skuJsonData) {
                this.skuJsonData = skuJsonData;
        }

        public GoodsSkuDTO getGoodsSkuDTO() {
                return goodsSkuDTO;
        }

        public void setGoodsSkuDTO(GoodsSkuDTO goodsSkuDTO) {
                this.goodsSkuDTO = goodsSkuDTO;
        }

        public Integer getSum() {
                return sum;
        }

        public void setSum(Integer sum) {
                this.sum = sum;
        }

        public Integer getSumPrice() {
                return sumPrice;
        }

        public void setSumPrice(Integer sumPrice) {
                this.sumPrice = sumPrice;
        }

        public LocalDateTime getUpdateTime() {
                return updateTime;
        }

        public void setUpdateTime(LocalDateTime updateTime) {
                this.updateTime = updateTime;
        }

        public LocalDateTime getCreateTime() {
                return createTime;
        }

        public void setCreateTime(LocalDateTime createTime) {
                this.createTime = createTime;
        }

        public Integer getStatus() {
                return status;
        }

        public void setStatus(Integer status) {
                this.status = status;
        }
}
