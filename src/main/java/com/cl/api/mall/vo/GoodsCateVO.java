package com.cl.api.mall.vo;

import com.cl.handler.Nature;

import java.time.LocalDateTime;

/**
* 描述：商品分类VO类
* @author csyq
* @date 2023-05-03
*/
public class GoodsCateVO {

        /**
        *
        */
        @Nature("")
        private Integer cateId;

        /**
        *父节点id
        */
        @Nature("父节点id")
        private Integer parent;

        /**
        *0:1级类别 2:2级类别
        */
        @Nature("0:1级类别 1:2级类别")
        private Integer grade;

        /**
        *分类名称
        */
        @Nature("分类名称")
        private String cateName;

        /**
        *是否删除 0否 1是
        */
        @Nature("是否删除 0否 1是")
        private Integer status;

        /**
        *修改时间
        */
        @Nature("修改时间")
        private LocalDateTime updateTime;

        /**
        *创建时间
        */
        @Nature("创建时间")
        private LocalDateTime createTime;

        public Integer getCateId() {
                return cateId;
        }

        public void setCateId(Integer cateId) {
                this.cateId = cateId;
        }

        public Integer getParent() {
                return parent;
        }

        public void setParent(Integer parent) {
                this.parent = parent;
        }

        public Integer getGrade() {
                return grade;
        }

        public void setGrade(Integer grade) {
                this.grade = grade;
        }

        public String getCateName() {
                return cateName;
        }

        public void setCateName(String cateName) {
                this.cateName = cateName;
        }

        public Integer getStatus() {
                return status;
        }

        public void setStatus(Integer status) {
                this.status = status;
        }

        public LocalDateTime getUpdateTime() {
                return updateTime;
        }

        public void setUpdateTime(LocalDateTime updateTime) {
                this.updateTime = updateTime;
        }

        public LocalDateTime getCreateTime() {
                return createTime;
        }

        public void setCreateTime(LocalDateTime createTime) {
                this.createTime = createTime;
        }
}
