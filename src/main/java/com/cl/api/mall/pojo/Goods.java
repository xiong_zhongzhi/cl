package com.cl.api.mall.pojo;

import com.cl.handler.Nature;

import java.time.LocalDateTime;

/**
 * 描述：商品实体类
 * @author csyq
 * @date 2023-05-05
 */
public class Goods {
        public Integer getGoodsId() {
                return goodsId;
        }

        public void setGoodsId(Integer goodsId) {
                this.goodsId = goodsId;
        }

        public String getSpu() {
                return spu;
        }

        public void setSpu(String spu) {
                this.spu = spu;
        }

        public String getGoodsImg() {
                return goodsImg;
        }

        public void setGoodsImg(String goodsImg) {
                this.goodsImg = goodsImg;
        }

        public String getGoodsName() {
                return goodsName;
        }

        public void setGoodsName(String goodsName) {
                this.goodsName = goodsName;
        }

        public String getSynopsis() {
                return synopsis;
        }

        public void setSynopsis(String synopsis) {
                this.synopsis = synopsis;
        }

        public Integer getCateId() {
                return cateId;
        }

        public void setCateId(Integer cateId) {
                this.cateId = cateId;
        }

        public Integer getLabel() {
                return label;
        }

        public void setLabel(Integer label) {
                this.label = label;
        }

        public Integer getPrice() {
                return price;
        }

        public void setPrice(Integer price) {
                this.price = price;
        }

        public Integer getShelf() {
                return shelf;
        }

        public void setShelf(Integer shelf) {
                this.shelf = shelf;
        }

        public Integer getStatus() {
                return status;
        }

        public void setStatus(Integer status) {
                this.status = status;
        }

        public LocalDateTime getUpdateTime() {
                return updateTime;
        }

        public void setUpdateTime(LocalDateTime updateTime) {
                this.updateTime = updateTime;
        }

        public LocalDateTime getCreateTime() {
                return createTime;
        }

        public void setCreateTime(LocalDateTime createTime) {
                this.createTime = createTime;
        }

        /**
         *
         */
        @Nature("商品id")
        private Integer goodsId;

        /**
         *spu
         */
        @Nature("spu")
        private String spu;

        /**
         *商品图片
         */
        @Nature("商品图片")
        private String goodsImg;

        /**
         *商品名称
         */
        @Nature("商品名称")
        private String goodsName;

        /**
         *商品简介
         */
        @Nature("商品简介")
        private String synopsis;

        /**
         *品类
         */
        @Nature("品类")
        private Integer cateId;

        /**
         *标签
         */
        @Nature("标签")
        private Integer label;

        /**
         *展示价格
         */
        @Nature("展示价格")
        private Integer price;

        /**
         *是否删除 0未上架 1已经上架
         */
        @Nature("是否删除 0未上架 1已经上架")
        private Integer shelf;

        /**
         *是否删除 0否 1是
         */
        @Nature("是否删除 0否 1是")
        private Integer status;

        /**
         *修改时间
         */
        @Nature("修改时间")
        private LocalDateTime updateTime;

        /**
         *创建时间
         */
        @Nature("创建时间")
        private LocalDateTime createTime;

}