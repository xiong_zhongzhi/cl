package com.cl.api.mall.pojo;

import com.cl.handler.Nature;
import java.time.LocalDateTime;

/**
 * 描述：商品Sku实体类
 * @author csyq
 * @date 2023-05-05
 */
public class GoodsSku {

        /**
         *
         */
        @Nature("商品skuId")
        private Integer goodsId;

        /**
         *spu
         */
        @Nature("spu")
        private String spu;

        /**
         *sku
         */
        @Nature("sku")
        private String sku;

        /**
         *商品类型:0:角色商品 1:道具
         */
        @Nature("商品类型:0:角色商品 1:道具")
        private Integer goodsType;

        /**
         *关联id
         */
        @Nature("关联id")
        private String correlationId;

        /**
         *商品图片
         */
        @Nature("商品图片")
        private String goodsImg;

        /**
         *商品名称
         */
        @Nature("商品名称")
        private String goodsName;

        /**
         *商品简介
         */
        @Nature("商品简介")
        private String synopsis;

        /**
         *价格
         */
        @Nature("价格")
        private Integer price;

        /**
         *是否删除 0未上架 1已经上架
         */
        @Nature("是否删除 0未上架 1已经上架")
        private Integer shelf;

        /**
         *是否删除 0否 1是
         */
        @Nature("是否删除 0否 1是")
        private Integer status;

        /**
         *修改时间
         */
        @Nature("修改时间")
        private LocalDateTime updateTime;

        /**
         *创建时间
         */
        @Nature("创建时间")
        private LocalDateTime createTime;

        public Integer getGoodsId() {
                return goodsId;
        }

        public void setGoodsId(Integer goodsId) {
                this.goodsId = goodsId;
        }

        public String getSpu() {
                return spu;
        }

        public void setSpu(String spu) {
                this.spu = spu;
        }

        public String getSku() {
                return sku;
        }

        public void setSku(String sku) {
                this.sku = sku;
        }

        public Integer getGoodsType() {
                return goodsType;
        }

        public void setGoodsType(Integer goodsType) {
                this.goodsType = goodsType;
        }

        public String getCorrelationId() {
                return correlationId;
        }

        public void setCorrelationId(String correlationId) {
                this.correlationId = correlationId;
        }

        public String getGoodsImg() {
                return goodsImg;
        }

        public void setGoodsImg(String goodsImg) {
                this.goodsImg = goodsImg;
        }

        public String getGoodsName() {
                return goodsName;
        }

        public void setGoodsName(String goodsName) {
                this.goodsName = goodsName;
        }

        public String getSynopsis() {
                return synopsis;
        }

        public void setSynopsis(String synopsis) {
                this.synopsis = synopsis;
        }

        public Integer getPrice() {
                return price;
        }

        public void setPrice(Integer price) {
                this.price = price;
        }

        public Integer getShelf() {
                return shelf;
        }

        public void setShelf(Integer shelf) {
                this.shelf = shelf;
        }

        public Integer getStatus() {
                return status;
        }

        public void setStatus(Integer status) {
                this.status = status;
        }

        public LocalDateTime getUpdateTime() {
                return updateTime;
        }

        public void setUpdateTime(LocalDateTime updateTime) {
                this.updateTime = updateTime;
        }

        public LocalDateTime getCreateTime() {
                return createTime;
        }

        public void setCreateTime(LocalDateTime createTime) {
                this.createTime = createTime;
        }
}