package com.cl.api.mall.request;

import com.cl.handler.Nature;

/**
 * 描述：新增请求参数类主订单
 *
 * @author csyq
 * @date 2023-05-14
 */
public class PayOrderRequest {

    /**
     *
     */
    @Nature("订单id")
    private Integer orderId;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
}
