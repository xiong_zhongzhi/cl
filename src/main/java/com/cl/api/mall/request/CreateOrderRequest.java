package com.cl.api.mall.request;

import com.cl.api.mall.vo.OrderPartVO;
import com.cl.handler.Nature;

import java.util.List;

/**
 * 描述：新增请求参数类主订单
 *
 * @author csyq
 * @date 2023-05-14
 */
public class CreateOrderRequest {

    /**
     *
     */
    @Nature("")
    private Integer orderId;

    /**
     * 购买人id
     */
    @Nature("购买人id")
    private String userId;

    /**
     * 订单状态 0待确认 1未完成 2已完成
     */
    @Nature("订单状态 0待确认 1未完成 2已完成")
    private Integer orderStatus;

    /**
     * 订单支付状态 0未支付  1已支付
     */
    @Nature("订单支付状态 0未支付  1已支付")
    private Integer payStatus;

    /**
     * 订单总积分
     */
    @Nature("订单总积分")
    private Integer sumPrice;

    /**
     * 是否删除 0否 1是
     */
    @Nature("是否删除 0否 1是")
    private Integer status;


    @Nature("子订单详细信息")
    List<OrderPartVO> orderPartVOList;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Integer getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(Integer sumPrice) {
        this.sumPrice = sumPrice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<OrderPartVO> getOrderPartVOList() {
        return orderPartVOList;
    }

    public void setOrderPartVOList(List<OrderPartVO> orderPartVOList) {
        this.orderPartVOList = orderPartVOList;
    }
}
