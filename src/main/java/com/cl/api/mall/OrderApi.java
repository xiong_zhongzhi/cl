package com.cl.api.mall;

import com.cl.api.ApiURL;
import com.cl.api.Response;
import com.cl.api.mall.request.CreateOrderRequest;
import com.cl.api.mall.request.PayOrderRequest;
import com.cl.api.mall.response.CreateOrderResponse;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.api.Api;
import org.springframework.http.HttpMethod;

@RulesOfCompetency
@Api(ApiURL.SERVER_URL)
public interface OrderApi {

	// 创建订单
	@Api(value = "/order/createOrder", httpMethod = HttpMethod.POST)
	public Response<CreateOrderResponse> createOrder(CreateOrderRequest createOrderRequest);

	// 订单支付
	@Api(value = "/order/payOrder", httpMethod = HttpMethod.POST)
	public Response payOrder(PayOrderRequest payOrderRequest);

}