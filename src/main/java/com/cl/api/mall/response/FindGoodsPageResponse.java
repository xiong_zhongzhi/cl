package com.cl.api.mall.response;

import com.cl.api.Page;
import com.cl.api.mall.dto.GoodsDTO;
import com.cl.handler.Nature;

import java.util.List;


/**
* 描述：商品分页请求Response类
* @author csyq
* @date 2023-05-05
*/
public class FindGoodsPageResponse {

    @Nature("分页信息")
    private Page page;

    @Nature("结果集")
    private List<GoodsDTO> goodsDTOList;

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public List<GoodsDTO> getGoodsDTOList() {
        return goodsDTOList;
    }

    public void setGoodsDTOList(List<GoodsDTO> goodsDTOList) {
        this.goodsDTOList = goodsDTOList;
    }
}
