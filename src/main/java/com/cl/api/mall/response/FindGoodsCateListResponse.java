package com.cl.api.mall.response;

import com.cl.api.mall.dto.GoodsCateDTO;
import com.cl.handler.Nature;

import java.util.List;

/**
* 描述：商品分类列表请求Response类
* @author csyq
* @date 2023-05-03
*/
public class FindGoodsCateListResponse  {

    @Nature("结果集")
    private List<GoodsCateDTO> goodsCateDTOList;

    public List<GoodsCateDTO> getGoodsCateDTOList() {
        return goodsCateDTOList;
    }

    public void setGoodsCateDTOList(List<GoodsCateDTO> goodsCateDTOList) {
        this.goodsCateDTOList = goodsCateDTOList;
    }
}
