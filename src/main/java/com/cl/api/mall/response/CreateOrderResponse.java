package com.cl.api.mall.response;

import com.cl.handler.Nature;

/**
* 描述：主订单列表请求Response类
* @author csyq
* @date 2023-05-14
*/
public class CreateOrderResponse {

    @Nature("订单id")
    private Integer orderId;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
}
