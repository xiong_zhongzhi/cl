package com.cl.api.user;

import com.cl.api.ApiURL;
import com.cl.api.user.pojo.User;
import com.cl.api.user.request.LoginRequest;
import com.cl.api.user.request.RegisterRequest;
import com.cl.api.user.response.LoginResponse;
import org.springframework.http.HttpMethod;

import com.cl.api.Response;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.api.Api;

@RulesOfCompetency
@Api(ApiURL.SERVER_URL)
public interface UserApi {
	
	//登录
	@Api(value = "/user/login",httpMethod = HttpMethod.POST)
	public Response<LoginResponse> login(LoginRequest loginRequest);

	//注册
	@Api(value = "/user/register",httpMethod = HttpMethod.POST)
	public Response register(RegisterRequest registerRequest);

}