package com.cl.api.user.request;

/**
 * 描述：注册
 * @author csyq
 * @date 2023-04-06
 */
public class RegisterRequest {

    /**
     *登录账号
     */
    private String account;

    /**
     *登录密码
     */
    private String password;

    /**
     *用户昵称
     */
    private String userName;


    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
