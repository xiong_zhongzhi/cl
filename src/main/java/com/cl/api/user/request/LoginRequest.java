package com.cl.api.user.request;


/**
 * 描述：新增请求参数类用户账户
 * @author csyq
 * @date 2023-03-04
 */
public class LoginRequest {

    /**
     *账号
     */
   private String account;

    /**
     *账号
     */
    private String password;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
