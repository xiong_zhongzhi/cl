package com.cl.api.user.response;

import com.cl.handler.Nature;

public class LoginResponse {

	@Nature("用户id")
	private String id;

	@Nature("账号头像")
	private String photo;

	@Nature("账号名称")
	private String userName;

	@Nature("token")
	private String token;

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getUserName() {
		return userName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
