package com.cl.api.user.pojo;

import com.cl.handler.Nature;

import java.time.LocalDateTime;

public class User {

    /**
     * 主键UUID
     */
    @Nature("主键UUID")
    private String id;

    /**
     * 用户昵称
     */
    @Nature("用户昵称")
    private String userName;

    /**
     * 用户头像
     */
    @Nature("用户头像")
    private String photo;

    /**
     * 登录账号
     */
    @Nature("登录账号")
    private String account;

    /**
     * 登录密码
     */
    @Nature("登录密码")
    private String password;

    /**
     * 盐
     */
    @Nature("盐")
    private String yan;

    /**
     * 用户实名
     */
    @Nature("用户实名")
    private String name;

    /**
     * 用户电话
     */
    @Nature("用户电话")
    private String utel;

    /**
     * 微信号
     */
    @Nature("微信号")
    private String weixin;

    /**
     * 出生日期
     */
    @Nature("出生日期")
    private String birthday;

    /**
     * 身份证号
     */
    @Nature("身份证号")
    private String idcard;

    /**
     * 性别 0:男 1:女 2:保密
     */
    @Nature("性别 0:男 1:女 2:保密")
    private Integer usex;

    /**
     * 年龄
     */
    @Nature("年龄")
    private Integer age;

    /**
     * 邮箱
     */
    @Nature("邮箱")
    private String email;

    /**
     * 地区
     */
    @Nature("地区")
    private String uaddress;

    /**
     * 个人简介
     */
    @Nature("个人简介")
    private String introduction;

    /**
     * 最近登录日期
     */
    @Nature("最近登录日期")
    private String logindate;

    /**
     * 最近登录ip
     */
    @Nature("最近登录ip")
    private String uip;

    /**
     * 创建人
     */
    @Nature("创建人")
    private String creator;

    /**
     * 创建时间
     */
    @Nature("创建时间")
    private LocalDateTime createTime;

    /**
     *
     */
    @Nature("修改时间")
    private LocalDateTime updateTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getYan() {
        return yan;
    }

    public void setYan(String yan) {
        this.yan = yan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUtel() {
        return utel;
    }

    public void setUtel(String utel) {
        this.utel = utel;
    }

    public String getWeixin() {
        return weixin;
    }

    public void setWeixin(String weixin) {
        this.weixin = weixin;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public Integer getUsex() {
        return usex;
    }

    public void setUsex(Integer usex) {
        this.usex = usex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUaddress() {
        return uaddress;
    }

    public void setUaddress(String uaddress) {
        this.uaddress = uaddress;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getLogindate() {
        return logindate;
    }

    public void setLogindate(String logindate) {
        this.logindate = logindate;
    }

    public String getUip() {
        return uip;
    }

    public void setUip(String uip) {
        this.uip = uip;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}
