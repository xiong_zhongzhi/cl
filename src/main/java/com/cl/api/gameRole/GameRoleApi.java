package com.cl.api.gameRole;

import com.cl.api.ApiURL;
import com.cl.api.Response;
import com.cl.api.gameRole.request.FindGameRolePageRequest;
import com.cl.api.gameRole.response.FindGameRolePageResponse;
import com.cl.api.mall.request.FindGoodsPageRequest;
import com.cl.api.mall.response.FindGoodsPageResponse;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.api.Api;
import org.springframework.http.HttpMethod;

@RulesOfCompetency
@Api(ApiURL.SERVER_URL)
public interface GameRoleApi {

	// 分页查询角色
	@Api(value = "/gameRole/findGameRolePage", httpMethod = HttpMethod.GET)
	public Response<FindGameRolePageResponse> findGameRolePage(FindGameRolePageRequest findGameRolePageRequest);

}