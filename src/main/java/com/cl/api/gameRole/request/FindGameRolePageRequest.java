package com.cl.api.gameRole.request;

import com.cl.api.Page;
import com.cl.handler.Nature;

import java.time.LocalDateTime;

/**
* 描述：游戏角色分页请求Reques类
* @author csyq
* @date 2023-04-24
*/
public class FindGameRolePageRequest extends Page {

    /**
     *
     */
    @Nature("角色id")
    private Integer gameRoleId;

    public Integer getGameRoleId() {
        return gameRoleId;
    }

    public void setGameRoleId(Integer gameRoleId) {
        this.gameRoleId = gameRoleId;
    }

    public String getRoleImg() {
        return roleImg;
    }

    public void setRoleImg(String roleImg) {
        this.roleImg = roleImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Integer getDef() {
        return def;
    }

    public void setDef(Integer def) {
        this.def = def;
    }

    public Integer getAtk() {
        return atk;
    }

    public void setAtk(Integer atk) {
        this.atk = atk;
    }

    public Integer getSd() {
        return sd;
    }

    public void setSd(Integer sd) {
        this.sd = sd;
    }

    public Integer getHp() {
        return hp;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Integer getSp() {
        return sp;
    }

    public void setSp(Integer sp) {
        this.sp = sp;
    }

    public Integer getDd() {
        return dd;
    }

    public void setDd(Integer dd) {
        this.dd = dd;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     *角色图片
     */
    @Nature("角色图片")
    private String roleImg;

    /**
     *角色名称
     */
    @Nature("角色名称")
    private String name;

    /**
     *描述
     */
    @Nature("描述")
    private String describe;

    /**
     *防御力
     */
    @Nature("防御力")
    private Integer def;

    /**
     *攻击力
     */
    @Nature("攻击力")
    private Integer atk;

    /**
     *攻击距离
     */
    @Nature("攻击距离")
    private Integer sd;

    /**
     *生命值
     */
    @Nature("生命值")
    private Integer hp;

    /**
     *法力值
     */
    @Nature("法力值")
    private Integer sp;

    /**
     *移动距离
     */
    @Nature("移动距离")
    private Integer dd;

    /**
     *是否删除 0否 1是
     */
    @Nature("是否删除 0否 1是")
    private Integer status;

    /**
     *创建时间
     */
    @Nature("创建时间")
    private LocalDateTime createTime;

    /**
     *修改时间
     */
    @Nature("修改时间")
    private LocalDateTime updateTime;
}
