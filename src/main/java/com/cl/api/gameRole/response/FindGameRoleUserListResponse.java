package com.cl.api.gameRole.response;

import com.cl.api.gameRole.dto.GameRoleUserDTO;
import com.cl.handler.Nature;

import java.util.List;

/**
* 描述：玩家的角色列表请求Response类
* @author csyq
* @date 2023-05-17
*/
public class FindGameRoleUserListResponse {

    @Nature("结果集")
    private List<GameRoleUserDTO> gameRoleUserDTOList;

    public List<GameRoleUserDTO> getGameRoleUserDTOList() {
        return gameRoleUserDTOList;
    }

    public void setGameRoleUserDTOList(List<GameRoleUserDTO> gameRoleUserDTOList) {
        this.gameRoleUserDTOList = gameRoleUserDTOList;
    }
}
