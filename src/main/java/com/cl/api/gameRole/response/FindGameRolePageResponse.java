package com.cl.api.gameRole.response;

import com.cl.api.Page;
import com.cl.api.gameRole.dto.GameRoleDTO;
import com.cl.handler.Nature;

import java.util.List;


/**
* 描述：游戏角色分页请求Response类
* @author csyq
* @date 2023-04-24
*/
public class FindGameRolePageResponse {

    @Nature("分页信息")
    private Page page;

    @Nature("结果集")
    private List<GameRoleDTO> gameRoleDTOList;

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public List<GameRoleDTO> getGameRoleDTOList() {
        return gameRoleDTOList;
    }

    public void setGameRoleDTOList(List<GameRoleDTO> gameRoleDTOList) {
        this.gameRoleDTOList = gameRoleDTOList;
    }
}
