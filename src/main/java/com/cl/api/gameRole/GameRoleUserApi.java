package com.cl.api.gameRole;

import com.cl.api.ApiURL;
import com.cl.api.Response;
import com.cl.api.gameRole.response.FindGameRoleUserListResponse;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.api.Api;
import org.springframework.http.HttpMethod;

@RulesOfCompetency
@Api(ApiURL.SERVER_URL)
public interface GameRoleUserApi {

    // 玩家查询自己拥有的角色
    @Api(value = "/gameRoleUser/findGameRoleUserList", httpMethod = HttpMethod.GET)
    public Response<FindGameRoleUserListResponse> findGameRoleUserList();

}
