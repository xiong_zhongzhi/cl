package com.cl.api.gameRole.dto;

import com.cl.handler.Nature;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 描述：玩家的角色DTO
 *
 * @author csyq
 * @date 2023-05-17
 */
public class GameRoleUserDTO {

    /**
     *
     */
    @Nature("")
    private Integer myGameRoleId;

    /**
     *
     */
    @Nature("")
    private Integer gameRoleId;

    /**
     *
     */
    @Nature("")
    private String userId;

    /**
     *
     */
    @Nature("角色图片")
    private String roleImg;

    /**
     *角色长度
     */
    @Nature("角色长度")
    private Integer height;

    /**
     *角色宽度
     */
    @Nature("角色宽度")
    private Integer width;


    /**
     * 角色名称
     */
    @Nature("角色名称")
    private String name;

    /**
     * 描述
     */
    @Nature("描述")
    private String describe;

    /**
     * 防御力
     */
    @Nature("防御力")
    private Integer def;

    /**
     * 攻击力
     */
    @Nature("攻击力")
    private Integer atk;

    /**
     * 攻击距离
     */
    @Nature("攻击距离")
    private Integer sd;

    /**
     * 生命值
     */
    @Nature("生命值")
    private Integer hp;

    /**
     * 法力值
     */
    @Nature("法力值")
    private Integer sp;

    /**
     * 实际生命值
     */
    @Nature("实际生命值")
    private Integer realityHp;

    /**
     * 实际法力值
     */
    @Nature("实际法力值")
    private Integer realitySp;

    /**
     * 移动距离
     */
    @Nature("移动距离")
    private Integer dd;

    /**
     * 是否删除 0否 1是
     */
    @Nature("是否删除 0否 1是")
    private Integer status;

    /**
     * 原角色信息
     */
    private GameRoleDTO gameRoleDTO;
    /**
     * 原角色绑定的技能信息
     */
    private List<GameRoleSkillDTO> gameRoleSkillDTOList;

    /**
     * 修改时间
     */
    @Nature("修改时间")
    private LocalDateTime updateTime;

    /**
     * 创建时间
     */
    @Nature("创建时间")
    private LocalDateTime createTime;

    /**
     * 状态 0:存活 1:死亡
     */
    @Nature("状态")
    private Integer flag = 0;

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getRealityHp() {
        return realityHp;
    }

    public void setRealityHp(Integer realityHp) {
        this.realityHp = realityHp;
    }

    public Integer getRealitySp() {
        return realitySp;
    }

    public void setRealitySp(Integer realitySp) {
        this.realitySp = realitySp;
    }

    public GameRoleDTO getGameRoleDTO() {
        return gameRoleDTO;
    }

    public void setGameRoleDTO(GameRoleDTO gameRoleDTO) {
        this.gameRoleDTO = gameRoleDTO;
    }

    public List<GameRoleSkillDTO> getGameRoleSkillDTOList() {
        return gameRoleSkillDTOList;
    }

    public void setGameRoleSkillDTOList(List<GameRoleSkillDTO> gameRoleSkillDTOList) {
        this.gameRoleSkillDTOList = gameRoleSkillDTOList;
    }

    public Integer getMyGameRoleId() {
        return myGameRoleId;
    }

    public void setMyGameRoleId(Integer myGameRoleId) {
        this.myGameRoleId = myGameRoleId;
    }

    public Integer getGameRoleId() {
        return gameRoleId;
    }

    public void setGameRoleId(Integer gameRoleId) {
        this.gameRoleId = gameRoleId;
    }

    public String getRoleImg() {
        return roleImg;
    }

    public void setRoleImg(String roleImg) {
        this.roleImg = roleImg;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Integer getDef() {
        return def;
    }

    public void setDef(Integer def) {
        this.def = def;
    }

    public Integer getAtk() {
        return atk;
    }

    public void setAtk(Integer atk) {
        this.atk = atk;
    }

    public Integer getSd() {
        return sd;
    }

    public void setSd(Integer sd) {
        this.sd = sd;
    }

    public Integer getHp() {
        return hp;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Integer getSp() {
        return sp;
    }

    public void setSp(Integer sp) {
        this.sp = sp;
    }

    public Integer getDd() {
        return dd;
    }

    public void setDd(Integer dd) {
        this.dd = dd;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
}