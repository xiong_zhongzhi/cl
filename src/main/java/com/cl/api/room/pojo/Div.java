package com.cl.api.room.pojo;

import com.cl.handler.Nature;

@Nature("-")
public class Div {
	
	@Nature("id块元素")
	private Integer divId;
	
	@Nature("块元素名称")
	private String divName;
	
	@Nature("正常展示色")
	private String naturalRGBA;
	
	@Nature("元素块图片")
	private String divImg;
	
	@Nature("创建时间")
	private String createTime;
	
	@Nature("修改时间")
	private String updateTime;
	
	@Nature("是否删除0未删除 1已删除")
	private String delFlag;
	
	@Nature("审核状态0待审核 1审核通过 2审核驳回")
	private String checkState;

	public Integer getDivId() {
		return divId;
	}

	public void setDivId(Integer divId) {
		this.divId = divId;
	}

	public String getDivName() {
		return divName;
	}

	public void setDivName(String divName) {
		this.divName = divName;
	}

	public String getNaturalRGBA() {
		return naturalRGBA;
	}

	public void setNaturalRGBA(String naturalRGBA) {
		this.naturalRGBA = naturalRGBA;
	}

	public String getDivImg() {
		return divImg;
	}

	public void setDivImg(String divImg) {
		this.divImg = divImg;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public String getCheckState() {
		return checkState;
	}

	public void setCheckState(String checkState) {
		this.checkState = checkState;
	}
	
	
}
