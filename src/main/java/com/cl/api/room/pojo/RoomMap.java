package com.cl.api.room.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.parser.JSONLexer;
import com.cl.handler.Nature;
import com.cl.util.DataJsonDeserialization;
import com.cl.util.EnumTime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class RoomMap {
	@Nature("房间id")
	private String roomMapId;
	
	@Nature("地图类型")
	private Integer mapType;
	
	@Nature("x轴坐标")
	private Integer x;
	
	@Nature("y轴坐标")
	private Integer y;
	
	@Nature("地图图片")
	private String url;

	@Nature("地图描述")
	private String name;

	@Nature("创建时间")
	@JSONField(deserializeUsing = DataJsonDeserialization.class)
	private String createTime;

	@Nature("修改时间")
	@JSONField(deserializeUsing = DataJsonDeserialization.class)
	private String updateTime;

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getRoomMapId() {
		return roomMapId;
	}

	public void setRoomMapId(String roomMapId) {
		this.roomMapId = roomMapId;
	}

	public Integer getMapType() {
		return mapType;
	}

	public void setMapType(Integer mapType) {
		this.mapType = mapType;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public boolean equals(Object o) {
		System.out.println("1");
		return true;
	}
}
