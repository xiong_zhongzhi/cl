package com.cl.api.room.pojo;

import com.cl.handler.Nature;

@Nature("坐标类")
public class Coordinates {

    public Coordinates(Integer pointX, Integer pointY) {
        this.pointX = pointX;
        this.pointY = pointY;
    }

    @Nature("点对象缓存的x")
    private Integer pointX;

    @Nature("点对象缓存的y")
    private Integer pointY;

    public Integer getPointX() {
        return pointX;
    }

    public void setPointX(Integer pointX) {
        this.pointX = pointX;
    }

    public Integer getPointY() {
        return pointY;
    }

    public void setPointY(Integer pointY) {
        this.pointY = pointY;
    }
}
