package com.cl.api.room.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.cl.handler.Nature;
import com.cl.util.DataJsonDeserialization;
import com.cl.util.DataJsonSerialize;

public class Room {
	
	@Nature("房间密码")
	private String roomPassword;
	
	@Nature("房间编号")
	private String roomNumber;
	
	@Nature("创建时间")
	@JSONField(serializeUsing = DataJsonSerialize.class,deserializeUsing = DataJsonDeserialization.class)
	private String createTime;
	
	@Nature("是否公开")
	private String roomPublic;
	
	@Nature("修改时间")
	@JSONField(serializeUsing = DataJsonSerialize.class,deserializeUsing = DataJsonDeserialization.class)
	private String updateTime;
	
	@Nature("房间最大参与人数")
	private String personSum;
	
	@Nature("房主id")
	private String userId;
	
	@Nature("房间id")
	private String roomId;
	
	@Nature("房间名称")
	private String roomName;
	
	@Nature("房间类型")
	private String roomType;

	@Nature("房间高度")
	private Integer high;

	@Nature("房间宽度")
	private Integer width;


	public String getRoomPassword() {
		return roomPassword;
	}

	public void setRoomPassword(String roomPassword) {
		this.roomPassword = roomPassword;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getRoomPublic() {
		return roomPublic;
	}

	public void setRoomPublic(String roomPublic) {
		this.roomPublic = roomPublic;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getPersonSum() {
		return personSum;
	}

	public void setPersonSum(String personSum) {
		this.personSum = personSum;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public Integer getHigh() {
		return high;
	}

	public void setHigh(Integer high) {
		this.high = high;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}
}
