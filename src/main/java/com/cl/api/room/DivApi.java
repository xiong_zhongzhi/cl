package com.cl.api.room;


import com.cl.api.ApiURL;
import com.cl.api.room.response.FindDivListResponse;
import org.springframework.http.HttpMethod;

import com.cl.api.Response;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.api.Api;

@RulesOfCompetency
@Api(ApiURL.SERVER_URL)
public interface DivApi {
	
	//查询元素块列表
	@Api(value = "/div/findDivList",httpMethod = HttpMethod.GET)
	public Response<FindDivListResponse> queryDiv();
	
}