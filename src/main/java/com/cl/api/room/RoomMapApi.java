package com.cl.api.room;


import com.cl.api.ApiURL;
import com.cl.api.room.request.FindRoomMapListRequest;
import com.cl.api.room.request.UpDateRoomMapRequest;
import com.cl.api.room.response.FindRoomMapListResponse;
import org.springframework.http.HttpMethod;

import com.cl.api.Response;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.api.Api;

@RulesOfCompetency
@Api(ApiURL.SERVER_URL)
public interface RoomMapApi {
	
	//查询房间列表
	@Api(value = "/roomMap/findRoomMapList",httpMethod = HttpMethod.GET)
	public Response<FindRoomMapListResponse> findRoomMapList(FindRoomMapListRequest findRoomMapListRequest);

	//保存房间列表
	@Api(value = "/roomMap/upDateRoomMap",httpMethod = HttpMethod.POST)
	public Response upDateRoomMap(UpDateRoomMapRequest upDateRoomMapRequest);
	
}