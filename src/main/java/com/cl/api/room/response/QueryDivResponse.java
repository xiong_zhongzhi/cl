package com.cl.api.room.response;

import java.util.List;

import com.cl.api.room.pojo.Div;

public class QueryDivResponse {

	private List<Div> divList;

	public List<Div> getDivList() {
		return divList;
	}

	public void setDivList(List<Div> divList) {
		this.divList = divList;
	}

	

}
