package com.cl.api.room.response;

import com.cl.handler.Nature;

import java.util.List;

import com.cl.api.room.dto.RoomDTO;

/**
* 描述：房间信息列表请求Response类
* @author csyq
* @date 2023-05-24
*/
public class FindRoomListResponse {

    @Nature("结果集")
    private List<RoomDTO> roomDTOList;

    public List<RoomDTO> getRoomDTOList() {
        return roomDTOList;
    }

    public void setRoomDTOList(List<RoomDTO> roomDTOList) {
        this.roomDTOList = roomDTOList;
    }
}
