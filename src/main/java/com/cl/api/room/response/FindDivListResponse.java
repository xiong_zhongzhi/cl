package com.cl.api.room.response;

import com.cl.api.room.dto.DivDTO;
import java.util.List;


/**
* 描述：地块列表请求Response类
* @author csyq
* @date 2023-06-15
*/
public class FindDivListResponse {

    private List<DivDTO> divDTOList;

    public List<DivDTO> getDivDTOList() {
        return divDTOList;
    }

    public void setDivDTOList(List<DivDTO> divDTOList) {
        this.divDTOList = divDTOList;
    }
}
