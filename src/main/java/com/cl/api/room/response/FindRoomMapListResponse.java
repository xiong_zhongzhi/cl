package com.cl.api.room.response;

import com.cl.api.room.dto.RoomMapDTO;
import com.cl.handler.Nature;

import java.util.List;

/**
* 描述：房间地图信息列表请求Response类
* @author csyq
* @date 2023-05-24
*/
public class FindRoomMapListResponse {

    @Nature("结果集")
    private List<RoomMapDTO> roomMapDTOList;

    public List<RoomMapDTO> getRoomMapDTOList() {
        return roomMapDTOList;
    }

    public void setRoomMapDTOList(List<RoomMapDTO> roomMapDTOList) {
        this.roomMapDTOList = roomMapDTOList;
    }
}
