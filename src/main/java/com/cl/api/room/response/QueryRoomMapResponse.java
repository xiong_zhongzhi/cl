package com.cl.api.room.response;

import java.util.List;

import com.cl.api.room.pojo.RoomMap;

public class QueryRoomMapResponse {

	private List<RoomMap> roomMapList;

	public List<RoomMap> getRoomMapList() {
		return roomMapList;
	}

	public void setRoomMapList(List<RoomMap> roomMapList) {
		this.roomMapList = roomMapList;
	}

}
