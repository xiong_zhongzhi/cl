package com.cl.api.room.dto;

import java.time.LocalDateTime;

/**
 * 描述：地块DTO
 *
 * @author csyq
 * @date 2023-06-15
 */
public class DivDTO {

    /**
     * id块元素
     */
    private Integer divId;

    /**
     * 块元素名称
     */
    private String divName;

    /**
     * 正常展示色
     */
    private String naturalRGBA;

    /**
     * 元素块图片
     */
    private String divImg;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 是否删除0未删除 1已删除
     */
    private Integer delFlag;

    /**
     * 审核状态0待审核 1审核通过 2审核驳回
     */
    private Integer checkState;

    public Integer getDivId() {
        return divId;
    }

    public void setDivId(Integer divId) {
        this.divId = divId;
    }

    public String getDivName() {
        return divName;
    }

    public void setDivName(String divName) {
        this.divName = divName;
    }

    public String getNaturalRGBA() {
        return naturalRGBA;
    }

    public void setNaturalRGBA(String naturalRGBA) {
        this.naturalRGBA = naturalRGBA;
    }

    public String getDivImg() {
        return divImg;
    }

    public void setDivImg(String divImg) {
        this.divImg = divImg;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Integer getCheckState() {
        return checkState;
    }

    public void setCheckState(Integer checkState) {
        this.checkState = checkState;
    }
}