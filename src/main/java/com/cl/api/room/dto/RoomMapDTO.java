package com.cl.api.room.dto;

import com.cl.handler.Nature;

import java.time.LocalDateTime;


/**
 * 描述：房间地图信息DTO
 *
 * @author csyq
 * @date 2023-05-24
 */
public class RoomMapDTO {

    /**
     * 房间id
     */
    @Nature("房间id")
    private String roomMapId;

    /**
     * 地图类型
     */
    @Nature("地图类型")
    private Integer mapType;

    /**
     * x轴
     */
    @Nature("x轴")
    private Integer x;

    /**
     * y轴
     */
    @Nature("y轴")
    private Integer y;

    /**
     * 图片
     */
    @Nature("图片")
    private String url;

    /**
     * 描述
     */
    @Nature("描述")
    private String name;

    /**
     * 创建时间
     */
    @Nature("创建时间")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @Nature("修改时间")
    private LocalDateTime updateTime;


    public String getRoomMapId() {
        return roomMapId;
    }

    public void setRoomMapId(String roomMapId) {
        this.roomMapId = roomMapId;
    }

    public Integer getMapType() {
        return mapType;
    }

    public void setMapType(Integer mapType) {
        this.mapType = mapType;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}