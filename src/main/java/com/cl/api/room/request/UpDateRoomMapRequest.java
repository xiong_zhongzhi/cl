package com.cl.api.room.request;


import com.cl.api.room.vo.RoomMapVO;
import com.cl.handler.Nature;

import java.util.List;

/**
 * 描述：更新请求参数类UpDateRoomMapRequest
 * @author csyq
 * @date 2023-05-24
 */
public class UpDateRoomMapRequest {

    /**
     *房间id
     */
    @Nature("房间id")
    private Long roomId;

    /**
     *地图列表
     */
    @Nature("地图列表")
    private List<RoomMapVO> roomMapVOList;

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public List<RoomMapVO> getRoomMapVOList() {
        return roomMapVOList;
    }

    public void setRoomMapVOList(List<RoomMapVO> roomMapVOList) {
        this.roomMapVOList = roomMapVOList;
    }
}
