package com.cl.api.room.request;

import com.cl.handler.Nature;

/**
* 描述：房间地图信息分页请求Reques类
* @author csyq
* @date 2023-05-24
*/
public class FindRoomMapListRequest {

    /**
     *房间id
     */
    @Nature("房间id")
    private Long roomMapId;

    public FindRoomMapListRequest(Long roomMapId) {
        this.roomMapId = roomMapId;
    }

    public Long getRoomMapId() {
        return roomMapId;
    }

    public void setRoomMapId(Long roomMapId) {
        this.roomMapId = roomMapId;
    }
}
