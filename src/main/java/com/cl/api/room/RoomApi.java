package com.cl.api.room;

import com.cl.api.ApiURL;
import com.cl.api.room.pojo.Room;
import com.cl.api.room.request.CreateRoomRequest;
import com.cl.api.room.request.FindRoomListRequest;
import com.cl.api.room.response.FindRoomListResponse;
import org.springframework.http.HttpMethod;

import com.cl.api.Response;
import com.cl.handler.Jurisdiction.RulesOfCompetency;
import com.cl.handler.api.Api;

@RulesOfCompetency
@Api(ApiURL.SERVER_URL)
public interface RoomApi {

	// 查询房间列表
	@Api(value = "/room/findRoomList", httpMethod = HttpMethod.GET)
	public Response<FindRoomListResponse> findRoomList(FindRoomListRequest findRoomListRequest);

	// 新增房间
	@Api(value = "/room/createRoom", httpMethod = HttpMethod.POST)
	public Response createRoom(CreateRoomRequest createRoomRequest);

}